
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class NoCacheFilter implements Filter {
    private static final boolean debug = true;
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    private FilterConfig filterConfig = null;
    public NoCacheFilter() {
    }
    public void destroy() {
    }
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
        System.out.println(request.getServletContext().getContextPath());
        log("Checking for url:" + request.getServletContext().getContextPath());
        String url = request.getServletContext().getContextPath();
        if (url.equals("/AccountingSystem/index.jsp")
                || url.equals("/AccountingSystem/LoginServlet")
                || url.equals("/AccountingSystem")) {
            log("OK filter");
            chain.doFilter(request, response);
        } else {
            log("Debug: " + session.getAttribute("userid"));
            if (session.getAttribute("userid") == null) {
                log("Redirecting to index.");
                ((HttpServletResponse) response).setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                ((HttpServletResponse) response).setHeader("Pragma", "no-cache"); // HTTP 1.0.
                ((HttpServletResponse) response).setDateHeader("Expires", 0); // Proxies.
                ((HttpServletResponse) response).sendRedirect("index.jsp");
            } else {
                log("We are not redirecting.");
            }
        }
    }
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("NoCacheFilter:Initializing filter");
            }
        }
    }
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("NoCacheFilter()");
        }
        StringBuffer sb = new StringBuffer("NoCacheFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("NoCacheFilter:DoAfterProcessing");
        }
    }
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("NoCacheFilter:DoBeforeProcessing");
        }
    }
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);
        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N
                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
}
