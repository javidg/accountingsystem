/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;
import Beans.AccountBean;
import Beans.BankBean;
import Beans.ChartOfAccountsBean;
import Beans.CompanyBean;
import Beans.PictureBean;
import Beans.ReportBean;
import Beans.TransactionBean;
import Beans.TransactionDetailBean;
import Beans.TypeOfPaymentBean;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.ResourceBundle;
import javax.xml.bind.DatatypeConverter;
/**
 *
 * @author user
 */
public class AccountingIO {
    Connection conn = null;
    String driver;
    String pass;
    Statement stmt = null;
    String url;
    String user;
    public AccountingIO(String driver, String url, String user, String pass) {
        this.driver = driver;
        this.url = url;
        this.user = user;
        this.pass = pass;
    }
    public AccountingIO() {
        ResourceBundle sql = ResourceBundle.getBundle("IO.SQL");
        this.driver = sql.getString("driver");
        this.url = sql.getString("url");
        this.user = sql.getString("user");
        this.pass = sql.getString("password");
    }
    public void addBalanceSheet(ReportBean report) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "insert into ReportTable(reportname, reporttype, startdate, enddate, datecreated, makerid, status, verifierid) values(?,?,?,?,CURDATE(),?,?,?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, report.getReportname());
            ps.setString(2, report.getReporttype());
            ps.setDate(3, report.getStartdate());
            ps.setDate(4, report.getEnddate());
            ps.setInt(5, report.getMakerid());
            ps.setString(6, "pending");
            ps.setInt(7, 0);
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addBank(BankBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "insert into BankTable(bankname) values(?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getBankname());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addCOA(ChartOfAccountsBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "insert into ChartOfAccountsTable(coaname,debitcredit,fs,itrfs,incomeexpense,classification,definition) values(?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getCoaname());
            ps.setString(2, data.getDebitcredit());
            ps.setString(3, data.getFs());
            ps.setString(4, data.getItrfs());
            ps.setString(5, data.getIncomeexpense());
            ps.setString(6, data.getClassification());
            ps.setString(7, data.getDefinition());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addIncomeStatement(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            String preparedSQL = "insert into ReportTable(reportname, reporttype, startdate, enddate, datecreated, makerid, status, verifierid) values(?,?,?,?,CURDATE(),?,?,?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getReportname());
            ps.setString(2, data.getReporttype());
            ps.setDate(3, data.getStartdate());
            ps.setDate(4, data.getEnddate());
            ps.setInt(5, data.getMakerid());
            ps.setString(6, "pending");
            ps.setInt(7, 0);
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addTOP(TypeOfPaymentBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            String topname = data.getTopname();
            
            String preparedSQL = "insert into TypeOfPaymentTable(topname) values(?)";
            
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, topname);
            ps.executeUpdate();
            
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addTransaction(TransactionBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "insert into TransactionTable(accountid,date,debitcoa,debitbank,debittop,debitamount,"
                    + "creditcoa,creditamount,"
                    + "transactiondetail,status,verifierid, debittopdetail)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setInt(1, data.getAccountid());
            ps.setDate(2, data.getDate());
            ps.setInt(3, data.getDebitcoa());
            ps.setInt(4, data.getDebitbank());
            ps.setInt(5, data.getDebittop());
            ps.setDouble(6, data.getDebitamount());
            ps.setInt(7, data.getCreditcoa());
            ps.setDouble(8, data.getCreditamount());
            ps.setString(9, data.getTransactiondetail());
            ps.setString(10, data.getStatus());
            ps.setInt(11, 1);
            ps.setString(12, data.getDebittopdetail());
            ps.executeUpdate();
            
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addTransactionDetails(TransactionDetailBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            String preparedSQL = "insert into TransactionDetailTable(transactionid, coaid, amount) values(?,?,?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setInt(1, data.getTransactionid());
            ps.setInt(2, data.getCoaid());
            ps.setDouble(3, data.getAmount());
            
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void addUser(AccountBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "insert into AccountTable(employeeid,firstname,lastname,password,permission, status) values(?,?,?,?,?,?);";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getEmployeeid());
            ps.setString(2, data.getFirstname());
            ps.setString(3, data.getLastname());
            ps.setString(4, data.getPassword());
            ps.setString(5, data.getPermission());
            ps.setString(6, data.getStatus());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void cancelReport(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ReportTable SET status = ?, verifierid = ? WHERE reportid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getStatus());
            ps.setInt(2, data.getVerifierid());
            ps.setInt(3, data.getReportid());
            
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void cancelTransaction(TransactionBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE TransactionTable SET status = ? WHERE transactionid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getStatus());
            ps.setInt(2, data.getTransactionid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public String checkAccount(String id, String password) throws SQLException {
        String uri = "index.jsp";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return uri;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String slc = "SELECT * FROM AccountTable WHERE employeeid = ? AND password = ?";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setString(1, id);
            ps.setString(2, password);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                String permission = rset.getString("permission");
                String status = rset.getString("status");
                //for easier filtering
                if (status.equals("active")) {
                    uri = "AdminServlet?xxx=x";
                } else if (status.equals("inactive")) {
                    uri = "index.jsp,inactive";
                }
            } else {
                uri = "index.jsp";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return uri;
    }
    
    //generic checker
    public boolean checkIfExists(String tablename, String columnname, String columnvalue) throws SQLException {
        boolean exists = false;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String sql = "select * from " + tablename + " where " + columnname + " = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, columnvalue);
            
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                exists = true;
            }
            
            return exists;
        } catch (SQLException e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public boolean checkIfExistsForUpdate(String tablename, String columnname, String columnvalue, String idname, int id) throws SQLException {
        boolean exists = false;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String sql = "select * from " + tablename + " where " + columnname + " = ? and " + idname + " = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, columnvalue);
            ps.setInt(2, id);
            
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                exists = true;
            }
            
            return exists;
        } catch (SQLException e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public boolean checkPasswordStrength(String password) {
        boolean hasLetter = false;
        boolean hasDigit = false;
        boolean length = false;
        
        try {
            
            if (password.length() > 7) {
                length = true;
            }
            
            for (int i = 0; i < password.length(); i++) {
                char x = password.charAt(i);
                
                if (Character.isLetter(x)) {
                    hasLetter = true;
                }
                
                if (Character.isDigit(x)) {
                    hasDigit = true;
                }
            }
            
            if (hasLetter == true && hasDigit == true && length == true) {
                return true;
            } else {
                return false;
            }
            
        } catch (StringIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        
        return false;
    }
    public int countAll(String tablename) throws SQLException {
        int ctr = 0;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return ctr;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "select count(*) from " + tablename;
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ResultSet rset = ps.executeQuery();
            rset.next();
            ctr = rset.getInt(1);
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ctr;
        
    }
    public void deleteBSReport(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ReportTable SET status = ? WHERE status = ? and reporttype = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, "archive");
            ps.setString(2, "canceled");
            ps.setString(3, "balancesheet");
            
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void deleteBank(BankBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "delete from BankTable where bankname = ?";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getBankname());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void deleteISReport(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ReportTable SET status = ? WHERE status = ? and reporttype = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, "archive");
            ps.setString(2, "canceled");
            ps.setString(3, "incomestatement");
            
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void deleteTReport() throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE TransactionTable SET status = ? WHERE status = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, "archive");
            ps.setString(2, "canceled");
            
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public ArrayList<AccountBean> getAccount() throws SQLException {
        ArrayList<AccountBean> account = new ArrayList<AccountBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from AccountTable order by accountid";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountBean data = new AccountBean();
                data.setAccountid(rs.getInt("accountid"));
                data.setEmployeeid(rs.getString("employeeid"));
                data.setFirstname(rs.getString("firstname"));
                data.setLastname(rs.getString("lastname"));
                data.setPassword(rs.getString("password"));
                data.setPermission(rs.getString("permission"));
                data.setStatus(rs.getString("status"));
                account.add(data);
            }
            
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return account;
    }
    public AccountBean getAccountById(int id) throws SQLException {
        AccountBean account = new AccountBean();
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return account;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String slc = "SELECT * FROM AccountTable WHERE accountid = ?";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                account.setAccountid(id);
                account.setEmployeeid(rset.getString("employeeid"));
                account.setPassword(rset.getString("password"));
                account.setFirstname(rset.getString("firstname"));
                account.setLastname(rset.getString("lastname"));
                account.setPermission(rset.getString("permission"));
                account.setStatus(rset.getString("status"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return account;
    }
    public AccountBean getAccountInfo(String id, String password) throws SQLException {
        AccountBean account = new AccountBean();
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return account;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "SELECT * FROM AccountTable WHERE employeeid = ? AND password = ?";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setString(1, id);
            ps.setString(2, password);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                
                account.setAccountid(rset.getInt("accountid"));
                account.setEmployeeid(id);
                account.setPassword(password);
                account.setFirstname(rset.getString("firstname"));
                account.setLastname(rset.getString("lastname"));
                account.setPermission(rset.getString("permission"));
                account.setStatus(rset.getString("status"));
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return account;
    }
    public ArrayList<ReportBean> getAllReports() throws SQLException {
        ArrayList<ReportBean> report = new ArrayList<ReportBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from ReportTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                ReportBean data = new ReportBean();
                data.setReportid(rs.getInt("reportid"));
                data.setReportname(rs.getString("reportname"));
                data.setReporttype(rs.getString("reporttype"));
                data.setMakerid(rs.getInt("makerid"));
                data.setVerifierid(rs.getInt("verifierid"));
                data.setStatus(rs.getString("status"));
                data.setStartdate(rs.getDate("startdate"));
                data.setEnddate(rs.getDate("enddate"));
                data.setDatecreated(rs.getDate("datecreated"));
                
                report.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return report;
    }
    public ReportBean getBalanceSheetById(int id) throws SQLException {
        ReportBean report = new ReportBean();
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return report;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "select i.reportid, i.makerid, i.verifierid, make.firstname as makerfirstname, make.lastname as makerlastname, verify.firstname as verifierfirstname, verify.lastname as verifierlastname, startdate, enddate from ReportTable i inner join AccountTable make on i.makerid=make.accountid inner join AccountTable verify on i.verifierid=verify.accountid where reportid=?";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                report.setReportid(rset.getInt("isid"));
                report.setMakerid(rset.getInt("makerid"));
                report.setVerifierid(rset.getInt("verifierid"));
                report.setStartdate(rset.getDate("startdate"));
                report.setEnddate(rset.getDate("enddate"));
                report.setDatecreated(rset.getDate("datecreated"));
                report.setMakerfirstname(rset.getString("makerfirstname"));
                report.setMakerlastname(rset.getString("makerlastname"));
                report.setVerifierfirstname(rset.getString("verifierfirstname"));
                report.setVerifierlastname(rset.getString("verifierlastname"));
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return report;
    }
    public ArrayList<ReportBean> getBalanceSheetReport() throws SQLException {
        ArrayList<ReportBean> report = new ArrayList<ReportBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select r.reportid, r.datecreated, r.reportname, r.startdate, r.enddate, r.verifierid, r.makerid, a.firstname, a.lastname, b.firstname, b.lastname from ReportTable r inner join AccountTable a on a.accountid = r.makerid inner join AccountTable b on b.accountid = r.verifierid  where r.reporttype = 'balancesheet' and r.status = 'pending'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                ReportBean data = new ReportBean();
                data.setReportname(rs.getString("r.reportname"));
                data.setReportid(rs.getInt("r.reportid"));
                data.setMakerid(rs.getInt("r.makerid"));
                data.setVerifierid(rs.getInt("r.verifierid"));
                data.setStartdate(rs.getDate("r.startdate"));
                data.setEnddate(rs.getDate("r.enddate"));
                data.setDatecreated(rs.getDate("r.datecreated"));
                data.setMakerfirstname(rs.getString("a.firstname"));
                data.setMakerlastname(rs.getString("a.lastname"));
                data.setVerifierfirstname(rs.getString("b.firstname"));
                data.setVerifierlastname(rs.getString("b.lastname"));
                report.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return report;
    }
    public ArrayList<BankBean> getBank() throws SQLException {
        ArrayList<BankBean> bank = new ArrayList<BankBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from BankTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                BankBean data = new BankBean();
                data.setBankid(rs.getInt("bankid"));
                data.setBankname(rs.getString("bankname"));
                
                bank.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return bank;
    }
    public BankBean getBankById(int id) throws SQLException {
        BankBean bank = new BankBean();
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return bank;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String slc = "SELECT * FROM BankTable WHERE bankid = ?";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                bank.setBankid(rset.getInt("bankid"));
                bank.setBankname(rset.getString("bankname"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return bank;
    }
    public ArrayList<ChartOfAccountsBean> getCOA() throws SQLException {
        ArrayList<ChartOfAccountsBean> coa = new ArrayList<ChartOfAccountsBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from ChartOfAccountsTable order by coaname";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ChartOfAccountsBean data = new ChartOfAccountsBean();
                data.setCoaid(rs.getInt("coaid"));
                data.setCoaname(rs.getString("coaname"));
                data.setDebitcredit(rs.getString("debitcredit"));
                data.setIncomeexpense(rs.getString("incomeexpense"));
                data.setFs(rs.getString("fs"));
                data.setItrfs(rs.getString("itrfs"));
                data.setClassification(rs.getString("classification"));
                data.setDefinition(rs.getString("definition"));
                
                coa.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return coa;
    }
    public ChartOfAccountsBean getCOAById(int id) throws SQLException {
        ChartOfAccountsBean coa = new ChartOfAccountsBean();
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return coa;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String slc = "SELECT * FROM ChartOfAccountsTable WHERE coaid = ?";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                coa.setCoaid(rset.getInt("coaid"));
                coa.setCoaname(rset.getString("coaname"));
                coa.setDebitcredit(rset.getString("debitcredit"));
                coa.setIncomeexpense(rset.getString("incomeexpense"));
                coa.setFs(rset.getString("fs"));
                coa.setItrfs(rset.getString("itrfs"));
                coa.setClassification(rset.getString("classification"));
                coa.setDefinition(rset.getString("definition"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return coa;
    }
    public ArrayList<ReportBean> getCanceledBalanceSheetReport() throws SQLException {
        ArrayList<ReportBean> report = new ArrayList<ReportBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select r.reportid, r.datecreated, r.reportname, r.startdate, r.enddate, r.verifierid, r.makerid, a.firstname, a.lastname, b.firstname, b.lastname, r.status from ReportTable r inner join AccountTable a on a.accountid = r.makerid inner join AccountTable b on b.accountid = r.verifierid  where r.reporttype = 'balancesheet' and r.status = 'canceled'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                ReportBean data = new ReportBean();
                data.setReportname(rs.getString("r.reportname"));
                data.setReportid(rs.getInt("r.reportid"));
                data.setMakerid(rs.getInt("r.makerid"));
                data.setVerifierid(rs.getInt("r.verifierid"));
                data.setStartdate(rs.getDate("r.startdate"));
                data.setEnddate(rs.getDate("r.enddate"));
                data.setDatecreated(rs.getDate("r.datecreated"));
                data.setMakerfirstname(rs.getString("a.firstname"));
                data.setMakerlastname(rs.getString("a.lastname"));
                data.setVerifierfirstname(rs.getString("b.firstname"));
                data.setVerifierlastname(rs.getString("b.lastname"));
                data.setStatus(rs.getString("r.status"));
                report.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return report;
    }
    public ArrayList<TransactionBean> getCanceledTransactionDetails() throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select t.transactionid, t.date, t.transactiondetail, t.status, t.verifierid, t.debitcoa, coa.coaname, t.creditcoa, ccoa.coaname from TransactionTable t inner join ChartOfAccountsTable coa on coa.coaid = t.debitcoa inner join ChartOfAccountsTable ccoa on ccoa.coaid = t.creditcoa where status = 'canceled'";
             
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                data.setTransactionid(rs.getInt("t.transactionid"));
                data.setDate(rs.getDate("t.date"));
                data.setTransactiondetail(rs.getString("t.transactiondetail"));
                data.setStatus(rs.getString("t.status"));
                data.setVerifierid(rs.getInt("t.verifierid"));
                data.setDebitcoa(rs.getInt("t.debitcoa"));
                data.setDebitcoaname(rs.getString("coa.coaname"));
                data.setCreditcoa(rs.getInt("t.creditcoa"));
                data.setCreditcoaname(rs.getString("ccoa.coaname"));
                
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
     public ArrayList<CompanyBean> getCompany() throws SQLException {
        ArrayList<CompanyBean> company = new ArrayList<CompanyBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from CompanyTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                CompanyBean data = new CompanyBean();
                data.setId(rs.getInt("id"));
                data.setCompanyname(rs.getString("companyname"));
                data.setCompanyaddress(rs.getString("companyaddress"));
                data.setCompanytelno(rs.getInt("companytelno"));
                company.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return company;
    }
     
    public CompanyBean getCompanyDetails() throws SQLException {
        CompanyBean data = new CompanyBean();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from CompanyTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
               
                data.setId(rs.getInt("id"));
                data.setCompanyname(rs.getString("companyname"));
                data.setCompanyaddress(rs.getString("companyaddress"));
                data.setCompanytelno(rs.getInt("companytelno"));
                
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return data;
    }
    
    public ArrayList<TransactionDetailBean> getDebitCreditDetailsById(int id) throws SQLException {
        ArrayList<TransactionDetailBean> detail = new ArrayList<TransactionDetailBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String slc = "select td.id, td.transactionid, c.coaid, c.coaname, c.debitcredit, td.amount from TransactionDetailTable td inner join ChartOfAccountsTable c on c.coaid = td.coaid where transactionid=?;";
            PreparedStatement ps;
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                TransactionDetailBean data = new TransactionDetailBean();
                data.setId(rset.getInt("td.id"));
                data.setTransactionid(id);
                data.setCoaid(rset.getInt("c.coaid"));
                data.setCoaname(rset.getString("c.coaname"));
                data.setDebitcredit(rset.getString("c.debitcredit"));
                data.setAmount(rset.getDouble("td.amount"));
                
                detail.add(data);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return detail;
    }
    public ReportBean getISById(int id) throws SQLException {
        ReportBean report = new ReportBean();
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return report;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "select * from ReportTable i inner join AccountTable make on i.makerid=make.accountid where reportid=?";
            PreparedStatement ps;
            
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            
            if (rset.next()) {
                report.setReportid(rset.getInt("reportid"));
                report.setReportname(rset.getString("reportname"));
                report.setMakerid(rset.getInt("makerid"));
                report.setVerifierid(rset.getInt("verifierid"));
                report.setStartdate(rset.getDate("startdate"));
                report.setEnddate(rset.getDate("enddate"));
                report.setMakerfirstname(rset.getString("make.firstname"));
                report.setMakerlastname(rset.getString("make.lastname"));
                report.setDatecreated(rset.getDate("datecreated"));
                report.setStatus(rset.getString("status"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return report;
    }
    public ArrayList<ReportBean> getIncomeStatement() throws SQLException {
        ArrayList<ReportBean> report = new ArrayList<ReportBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from ReportTable r inner join AccountTable m on r.makerid=m.accountid where reporttype='incomestatement';";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                ReportBean data = new ReportBean();
                data.setReportid(rs.getInt("reportid"));
                data.setReportname(rs.getString("reportname"));
                data.setMakerid(rs.getInt("makerid"));
                data.setMakerfirstname(rs.getString("m.firstname"));
                data.setMakerlastname(rs.getString("m.lastname"));
                data.setVerifierid(rs.getInt("verifierid"));
                data.setStartdate(rs.getDate("startdate"));
                data.setEnddate(rs.getDate("enddate"));
                data.setDatecreated(rs.getDate("datecreated"));
                data.setStatus(rs.getString("status"));
                
                report.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return report;
    }
    
    public AccountBean getLastAccount() throws SQLException {
        AccountBean data = null;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String selectSQL = "select max(accountid) as last from AccountTable";
            PreparedStatement ps = conn.prepareStatement(selectSQL);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                data = new AccountBean();
                data.setAccountid(rset.getInt("last"));
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return data;
    }
    public PictureBean getLastPicture() throws SQLException {
        PictureBean data = null;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String selectSQL = "select max(pictureid) as last from PictureTable";
            PreparedStatement ps = conn.prepareStatement(selectSQL);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                data = new PictureBean();
                data.setPictureid(rset.getInt("last"));
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return data;
    }
    
    public TransactionBean getLastTransaction() throws SQLException {
        TransactionBean data = null;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String selectSQL = "select max(transactionid) as last from TransactionTable";
            PreparedStatement ps = conn.prepareStatement(selectSQL);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                data = new TransactionBean();
                data.setTransactionid(rset.getInt("last"));
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return data;
    }
    public ArrayList<TransactionBean> getPendingTransactionDetails() throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select t.transactionid, t.accountid, t.date, t.debitcoa, coa.coaname, t.debitbank, b.bankname, t.debittop, top.topname, t.debitamount, t.creditcoa, ccoa.coaname, t.creditamount, t.transactiondetail, t.debittopdetail, t.status, t.verifierid from TransactionTable t inner join ChartOfAccountsTable coa on coa.coaid = t.debitcoa inner join ChartOfAccountsTable ccoa on ccoa.coaid = t.creditcoa inner join BankTable b on b.bankid = t.debitbank inner join TypeOfPaymentTable top on top.topid = t.debittop where status = 'pending'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                data.setTransactionid(rs.getInt("t.transactionid"));
                data.setAccountid(rs.getInt("t.accountid"));
                data.setDate(rs.getDate("t.date"));
                data.setDebitcoa(rs.getInt("t.debitcoa"));
                data.setDebitcoaname(rs.getString("coa.coaname"));
                data.setDebitbank(rs.getInt("t.debitbank"));
                data.setDebitbankname(rs.getString("b.bankname"));
                data.setDebittop(rs.getInt("t.debittop"));
                data.setDebittopname(rs.getString("top.topname"));
                data.setDebitamount(rs.getDouble("t.debitamount"));
                data.setCreditcoa(rs.getInt("t.creditcoa"));
                data.setCreditcoaname(rs.getString("ccoa.coaname"));
                data.setCreditamount(rs.getDouble("t.creditamount"));
                data.setTransactiondetail(rs.getString("t.transactiondetail"));
                data.setStatus(rs.getString("t.status"));
                data.setVerifierid(rs.getInt("t.verifierid"));
                data.setDebittopdetail(rs.getString("t.debittopdetail"));
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
    public PictureBean getPicture(int accountid) throws SQLException {
        PictureBean picture = new PictureBean();
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return picture;
        }
        
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "select picturename from PictureTable where pictureid=(select max(pictureid) from PictureTable where accountid=?);";
            PreparedStatement ps;
            
            ps = conn.prepareStatement(slc);
            ps.setInt(1, accountid);
            ResultSet rset = ps.executeQuery();
            
            if (rset.next()) {
                picture.setPicturename(rset.getString("picturename"));
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return picture;
    }
    public ArrayList<TypeOfPaymentBean> getTOP() throws SQLException {
        ArrayList<TypeOfPaymentBean> top = new ArrayList<TypeOfPaymentBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from TypeOfPaymentTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                TypeOfPaymentBean data = new TypeOfPaymentBean();
                data.setTopid(rs.getInt("topid"));
                data.setTopname(rs.getString("topname"));
                
                top.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return top;
    }
    public TypeOfPaymentBean getTOPById(int id) throws SQLException {
        TypeOfPaymentBean top = new TypeOfPaymentBean();
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return top;
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "SELECT * FROM TypeOfPaymentTable WHERE topid = ?";
            PreparedStatement ps;
            
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                top.setTopid(rset.getInt("topid"));
                top.setTopname(rset.getString("topname"));
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return top;
    }
    public ArrayList<TransactionBean> getTransaction() throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from TransactionTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                data.setTransactionid(rs.getInt("transactionid"));
                data.setAccountid(rs.getInt("accountid"));
                data.setDate(rs.getDate("date"));
                data.setDebitcoa(rs.getInt("debitcoa"));
                data.setDebitbank(rs.getInt("debitbank"));
                data.setDebittop(rs.getInt("debittop"));
                data.setDebitamount(rs.getDouble("debitamount"));
                data.setCreditcoa(rs.getInt("creditcoa"));
                data.setCreditamount(rs.getDouble("creditamount"));
                data.setTransactiondetail(rs.getString("transactiondetail"));
                data.setStatus(rs.getString("status"));
                data.setVerifierid(rs.getInt("verifierid"));
                data.setDebittopdetail(rs.getString("debittopdetail"));
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
    public ArrayList<TransactionBean> getTransactionByDateRangeBS(TransactionBean row, String aloe) throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select sum(amount) as totalamount, t.transactionid, c.coaid, c.coaname, c.incomeexpense, c.classification from TransactionDetailTable td inner join ChartOfAccountsTable c on c.coaid = td.coaid inner join TransactionTable t on t.transactionid = td.transactionid where t.status='verified' and t.date between ? and ? and c.classification = ? group by c.coaid;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setDate(1, row.getStartdate());
            ps.setDate(2, row.getEnddate());
            ps.setString(3, aloe);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                data.setTransactionid(rs.getInt("t.transactionid"));
                data.setDebitcoa(rs.getInt("c.coaid"));
                data.setDebitamount(rs.getDouble("totalamount"));
                data.setDebitdetail(rs.getString("c.coaname"));
                data.setTransactiondetail(rs.getString("c.incomeexpense"));
                data.setClassification(rs.getString("c.classification"));
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
    public ArrayList<TransactionBean> getTransactionByDateRangeIS(TransactionBean row) throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select sum(amount) as totalamount, t.transactionid, c.coaid, c.coaname, c.incomeexpense, c.classification from TransactionDetailTable td inner join ChartOfAccountsTable c on c.coaid = td.coaid inner join TransactionTable t on t.transactionid = td.transactionid where t.status='verified' and t.date between ? and ? group by c.coaid;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setDate(1, row.getStartdate());
            ps.setDate(2, row.getEnddate());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                
                data.setTransactionid(rs.getInt("t.transactionid"));
                data.setDebitcoa(rs.getInt("c.coaid"));
                data.setDebitamount(rs.getDouble("totalamount"));
                data.setDebitdetail(rs.getString("c.coaname"));
                data.setTransactiondetail(rs.getString("c.incomeexpense"));
                data.setClassification(rs.getString("c.classification"));
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
    public TransactionBean getTransactionById(int id) throws SQLException {
        TransactionBean transaction = new TransactionBean();
        ChartOfAccountsBean coa = new ChartOfAccountsBean();
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return transaction;
        }
        
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            
            String slc = "SELECT * FROM TransactionTable WHERE transactionid = ?";
            PreparedStatement ps;
            
            ps = conn.prepareStatement(slc);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();
            
            if (rset.next()) {
                transaction.setTransactionid(rset.getInt("transactionid"));
                transaction.setAccountid(rset.getInt("accountid"));
                transaction.setDate(rset.getDate("date"));
                transaction.setDebitcoa(rset.getInt("debitcoa"));
                transaction.setDebitbank(rset.getInt("debitbank"));
                transaction.setDebittop(rset.getInt("debittop"));
                transaction.setDebitamount(rset.getDouble("debitamount"));
                transaction.setCreditcoa(rset.getInt("creditcoa"));
                transaction.setCreditamount(rset.getDouble("creditamount"));
                transaction.setTransactiondetail(rset.getString("transactiondetail"));
                transaction.setStatus(rset.getString("status"));
                transaction.setVerifierid(rset.getInt("verifierid"));
                transaction.setDebittopdetail(rset.getString("debittopdetail"));
                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return transaction;
    }
    public ArrayList<TransactionBean> getTransactionDetailsById(int id) throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select t.transactionid, t.date, t.debitcoa, coa.coaname, t.debitbank, b.bankname, t.debittop, top.topname, t.debitamount, t.creditcoa, ccoa.coaname, t.creditamount, t.transactiondetail, t.debittopdetail from TransactionTable t inner join ChartOfAccountsTable coa on coa.coaid = t.debitcoa inner join ChartOfAccountsTable ccoa on ccoa.coaid = t.creditcoa inner join BankTable b on b.bankid = t.debitbank  inner join TypeOfPaymentTable top on top.topid = t.debittop WHERE transactionid = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                data.setTransactionid(rs.getInt("t.transactionid"));
                data.setDate(rs.getDate("t.date"));
                data.setDebitcoa(rs.getInt("t.debitcoa"));
                data.setDebitcoaname(rs.getString("coa.coaname"));
                data.setDebitbank(rs.getInt("t.debitbank"));
                data.setDebitbankname(rs.getString("b.bankname"));
                data.setDebittop(rs.getInt("t.debittop"));
                data.setDebittopname(rs.getString("top.topname"));
                data.setDebitamount(rs.getDouble("t.debitamount"));
                data.setCreditcoa(rs.getInt("t.creditcoa"));
                data.setCreditcoaname(rs.getString("ccoa.coaname"));
                data.setCreditamount(rs.getDouble("t.creditamount"));
                data.setTransactiondetail(rs.getString("t.transactiondetail"));
                data.setDebittopdetail(rs.getString("t.debittopdetail"));
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
    public ArrayList<ReportBean> getVerifiedBalanceSheetReport() throws SQLException {
        ArrayList<ReportBean> report = new ArrayList<ReportBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select r.reportid, r.datecreated, r.reportname, r.startdate, r.enddate, r.verifierid, r.makerid, a.firstname, a.lastname, b.firstname, b.lastname, r.status from ReportTable r inner join AccountTable a on a.accountid = r.makerid inner join AccountTable b on b.accountid = r.verifierid  where r.reporttype = 'balancesheet' and r.status = 'verified'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                ReportBean data = new ReportBean();
                data.setReportname(rs.getString("r.reportname"));
                data.setReportid(rs.getInt("r.reportid"));
                data.setMakerid(rs.getInt("r.makerid"));
                data.setVerifierid(rs.getInt("r.verifierid"));
                data.setStartdate(rs.getDate("r.startdate"));
                data.setEnddate(rs.getDate("r.enddate"));
                data.setDatecreated(rs.getDate("r.datecreated"));
                data.setMakerfirstname(rs.getString("a.firstname"));
                data.setMakerlastname(rs.getString("a.lastname"));
                data.setVerifierfirstname(rs.getString("b.firstname"));
                data.setVerifierlastname(rs.getString("b.lastname"));
                data.setStatus(rs.getString("r.status"));
                report.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return report;
    }
    public ArrayList<TransactionBean> getVerifiedTransactionDetails() throws SQLException {
        ArrayList<TransactionBean> transaction = new ArrayList<TransactionBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select t.transactionid, t.accountid, t.date, t.debitcoa, coa.coaname, t.debitbank, b.bankname, t.debittop, top.topname, t.debitamount, t.creditcoa, ccoa.coaname, t.creditamount, t.transactiondetail, t.debittopdetail, t.status, t.verifierid, a.firstname, a.lastname from TransactionTable t inner join ChartOfAccountsTable coa on coa.coaid = t.debitcoa inner join ChartOfAccountsTable ccoa on ccoa.coaid = t.creditcoa inner join BankTable b on b.bankid = t.debitbank  inner join TypeOfPaymentTable top on top.topid = t.debittop inner join AccountTable a on a.accountid = t.verifierid where t.status = 'verified'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TransactionBean data = new TransactionBean();
                data.setTransactionid(rs.getInt("t.transactionid"));
                data.setAccountid(rs.getInt("t.accountid"));
                data.setDate(rs.getDate("t.date"));
                data.setDebitcoa(rs.getInt("t.debitcoa"));
                data.setDebitcoaname(rs.getString("coa.coaname"));
                data.setDebitbank(rs.getInt("t.debitbank"));
                data.setDebitbankname(rs.getString("b.bankname"));
                data.setDebittop(rs.getInt("t.debittop"));
                data.setDebittopname(rs.getString("top.topname"));
                data.setDebitamount(rs.getDouble("t.debitamount"));
                data.setCreditcoa(rs.getInt("t.creditcoa"));
                data.setCreditcoaname(rs.getString("ccoa.coaname"));
                data.setCreditamount(rs.getDouble("t.creditamount"));
                data.setTransactiondetail(rs.getString("t.transactiondetail"));
                data.setStatus(rs.getString("t.status"));
                data.setVerifierid(rs.getInt("t.verifierid"));
                data.setVerifierfirstname(rs.getString("a.firstname"));
                data.setVerifierlastname(rs.getString("a.lastname"));
                data.setDebittopdetail(rs.getString("t.debittopdetail"));
                transaction.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return transaction;
    }
    public String hashPassword(String pin) throws SQLException, UnsupportedEncodingException {
        byte[] hash = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            hash = md.digest(pin.getBytes("UTF-8"));
            pin = DatatypeConverter.printHexBinary(hash);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        
        String encodedPin = Base64.getEncoder().encodeToString(hash);
        return encodedPin;
    }
    public void updateBank(BankBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            Statement upd = conn.createStatement();
            String sqlUpdate = "UPDATE BankTable SET bankname = ? WHERE bankid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getBankname());
            ps.setInt(2, data.getBankid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void updateCoa(ChartOfAccountsBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ChartOfAccountsTable SET coaname = ?,debitcredit = ?, fs = ?, itrfs = ?, incomeexpense = ?, classification = ?, definition = ? WHERE coaid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getCoaname());
            ps.setString(2, data.getDebitcredit());
            ps.setString(3, data.getFs());
            ps.setString(4, data.getItrfs());
            ps.setString(5, data.getIncomeexpense());
            ps.setString(6, data.getClassification());
            ps.setString(7, data.getDefinition());
            ps.setInt(8, data.getCoaid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void updateCompany(CompanyBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            Statement upd = conn.createStatement();
            String sqlUpdate = "UPDATE CompanyTable SET companyname = ?, companyaddress = ?, companytelno = ? WHERE id = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getCompanyname());
            ps.setString(2, data.getCompanyaddress());
            ps.setInt(3, data.getCompanytelno());
            ps.setInt(4, data.getId());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void updateReportName(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ReportTable SET reportname = ? WHERE reportid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getReportname());
            ps.setInt(2, data.getReportid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void updateTop(TypeOfPaymentBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE TypeOfPaymentTable SET topname = ? WHERE topid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getTopname());
            ps.setInt(2, data.getTopid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    

    public void updateTransaction(TransactionBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE TransactionTable SET accountid = ?, date = ?, debitcoa = ?, debitbank = ?, "
                    + "debittop = ?, debittopdetail= ?, debitamount= ?, creditcoa = ?,  "
                    + "creditamount = ?, transactiondetail = ?, status = ? WHERE transactionid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setInt(1, data.getAccountid());
            ps.setDate(2, data.getDate());
            ps.setInt(3, data.getDebitcoa());
            ps.setInt(4, data.getDebitbank());
            ps.setInt(5, data.getDebittop());
            ps.setString(6, data.getDebittopdetail());
            ps.setDouble(7, data.getDebitamount());
            ps.setInt(8, data.getCreditcoa());
            ps.setDouble(9, data.getCreditamount());
            ps.setString(10, data.getTransactiondetail());
            ps.setString(11, data.getStatus());
            ps.setInt(12, data.getTransactionid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void updateTransactionDetail(TransactionDetailBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE TransactionDetailTable SET coaid = ?,amount = ? WHERE id = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setInt(1, data.getCoaid());
            ps.setDouble(2, data.getAmount());
            ps.setInt(3, data.getId());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void updateUser(AccountBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            if (data.getPassword().equals("") == true && data.getPermission().equals("") == false && data.getStatus().equals("") == false) {
                String sqlUpdate = "UPDATE AccountTable SET employeeid = ?, firstname = ?, lastname = ?, permission = ?, status = ? WHERE accountid = ?";
                PreparedStatement ps = conn.prepareStatement(sqlUpdate);
                ps.setString(1, data.getEmployeeid());
                ps.setString(2, data.getFirstname());
                ps.setString(3, data.getLastname());
                ps.setString(4, data.getPermission());
                ps.setString(5, data.getStatus());
                ps.setInt(6, data.getAccountid());
                ps.executeUpdate();
                System.out.println(sqlUpdate);
            } else if (data.getPassword().equals("") == true && data.getPermission().equals("") == true && data.getStatus().equals("") == true) {
                String sqlUpdate = "UPDATE AccountTable SET employeeid = ?, firstname = ?, lastname = ? WHERE accountid = ?";
                PreparedStatement ps = conn.prepareStatement(sqlUpdate);
                ps.setString(1, data.getEmployeeid());
                ps.setString(2, data.getFirstname());
                ps.setString(3, data.getLastname());
                ps.setInt(4, data.getAccountid());
                ps.executeUpdate();
                System.out.println(sqlUpdate);
            } else if (data.getPassword().equals("") == false && data.getPermission().equals("") == true && data.getStatus().equals("") == true) {
                String sqlUpdate = "UPDATE AccountTable SET employeeid = ?, firstname = ?, lastname = ?, password = ? WHERE accountid = ?";
                PreparedStatement ps = conn.prepareStatement(sqlUpdate);
                ps.setString(1, data.getEmployeeid());
                ps.setString(2, data.getFirstname());
                ps.setString(3, data.getLastname());
                ps.setString(4, data.getPassword());
                ps.setInt(5, data.getAccountid());
                ps.executeUpdate();
                System.out.println(sqlUpdate);
            } else {
                String sqlUpdate = "UPDATE AccountTable SET employeeid = ?, firstname = ?, lastname = ?, password = ?, permission = ?, status = ? WHERE accountid = ?";
                PreparedStatement ps = conn.prepareStatement(sqlUpdate);
                ps.setString(1, data.getEmployeeid());
                ps.setString(2, data.getFirstname());
                ps.setString(3, data.getLastname());
                ps.setString(4, data.getPassword());
                ps.setString(5, data.getPermission());
                ps.setString(6, data.getStatus());
                ps.setInt(7, data.getAccountid());
                ps.executeUpdate();
                System.out.println(sqlUpdate);
            }
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void uploadPicture(PictureBean data) throws SQLException {
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;
            
            String preparedSQL = "insert into PictureTable(accountid, picturename) values (?,?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setInt(1, data.getAccountid());
            ps.setString(2, data.getPicturename());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void verifyBalanceSheet(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ReportTable SET verifierid = ?, status = 'verified' WHERE reportid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setInt(1, data.getVerifierid());
            ps.setInt(2, data.getReportid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void verifyISReport(ReportBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE ReportTable SET verifierid = ?, status = ? WHERE reportid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setInt(1, data.getVerifierid());
            ps.setString(2, data.getStatus());
            ps.setInt(3, data.getReportid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public void verifyTransaction(TransactionBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sqlUpdate = "UPDATE TransactionTable SET  status = ?, verifierid = ? WHERE transactionid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getStatus());
            ps.setInt(2, data.getVerifierid());
            ps.setInt(3, data.getTransactionid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
