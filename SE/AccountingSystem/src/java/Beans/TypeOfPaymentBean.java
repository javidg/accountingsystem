package Beans;
import java.io.Serializable;
public class TypeOfPaymentBean implements Serializable{
    int topid; 
    String topname;
        
    public TypeOfPaymentBean (){
        topid = 0;
        topname = "";
    }
    public int getTopid() {
        return topid;
    }
    public void setTopid(int topid) {
        this.topid = topid;
    }
    public String getTopname() {
        return topname;
    }
    public void setTopname(String topname) {
        this.topname = topname;
    }
}
