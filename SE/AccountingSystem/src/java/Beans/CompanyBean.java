package Beans;
import java.io.Serializable;
public class CompanyBean implements Serializable {
    String companyaddress;
    String companyname;
    int companytelno;
    int id;
    
    public CompanyBean (){
        id = 0;
        companyname = "";
        companyaddress = "";
        companytelno = 0;
    }
    public String getCompanyaddress() {
        return companyaddress;
    }
    public void setCompanyaddress(String companyaddress) {
        this.companyaddress = companyaddress;
    }
    public String getCompanyname() {
        return companyname;
    }
    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }
    public int getCompanytelno() {
        return companytelno;
    }
    public void setCompanytelno(int companytelno) {
        this.companytelno = companytelno;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    
}
