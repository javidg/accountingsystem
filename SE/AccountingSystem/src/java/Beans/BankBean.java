/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;
import java.io.Serializable;
/**
 *
 * @author user
 */
public class BankBean implements Serializable {
    int bankid;
    String bankname;
   
    public BankBean (){
        bankid = 0;
        bankname = "";
    }
    public int getBankid() {
        return bankid;
    }
    public void setBankid(int bankid) {
        this.bankid = bankid;
    }
    public String getBankname() {
        return bankname;
    }
    public void setBankname(String bankname) {
        this.bankname = bankname;
    }
  
}
