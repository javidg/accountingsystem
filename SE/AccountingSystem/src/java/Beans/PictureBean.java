package Beans;
import java.io.Serializable;
public class PictureBean implements Serializable {
    int accountid;
    int pictureid;
    String picturename;
    
    public PictureBean(){
        pictureid = 0;
        accountid = 0;
        picturename = "";
    }    
    public int getAccountid() {
        return accountid;
    }
    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }
    public int getPictureid() {
        return pictureid;
    }
    public void setPictureid(int pictureid) {
        this.pictureid = pictureid;
    }
    public String getPicturename() {
        return picturename;
    }
    public void setPicturename(String picturename) {
        this.picturename = picturename;
    }
    
    
}
