/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;
import java.io.Serializable;
/**
 *
 * @author user
 */
public class ChartOfAccountsBean implements Serializable {
    String classification;
    int coaid;
    String coaname;
    String debitcredit;
    String definition;
    String fs;
    String incomeexpense;
    String itrfs;
    
    public ChartOfAccountsBean (){
        coaid = 0;
        coaname = "";
        debitcredit = "";
        incomeexpense = "";
        classification = "";
        definition = "";
        fs = "";
        itrfs = "";
        
    }
    public String getClassification() {
        return classification;
    }
    public void setClassification(String classification) {
        this.classification = classification;
    }
    public int getCoaid() {
        return coaid;
    }
    public void setCoaid(int coaid) {
        this.coaid = coaid;
    }
    public String getCoaname() {
        return coaname;
    }
    public void setCoaname(String coaname) {
        this.coaname = coaname;
    }
    public String getDebitcredit() {
        return debitcredit;
    }
    public void setDebitcredit(String debitcredit) {
        this.debitcredit = debitcredit;
    }
    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    public String getFs() {
        return fs;
    }
    public void setFs(String fs) {
        this.fs = fs;
    }
    public String getIncomeexpense() {
        return incomeexpense;
    }
    public void setIncomeexpense(String incomeexpense) {
        this.incomeexpense = incomeexpense;
    }
    public String getItrfs() {
        return itrfs;
    }
    public void setItrfs(String itrfs) {
        this.itrfs = itrfs;
    }
    
}
