/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;
import java.io.Serializable;
import java.sql.Date;
/**
 *
 * @author user
 */
public class ReportBean implements Serializable {
    Date datecreated = this.datecreated;
    Date enddate = this.enddate;
    String makerfirstname;
    int makerid;
    String makerlastname;
    
    int reportid;
    String reportname;
    String reporttype;
    Date startdate = this.startdate;
    String status;
    
    String verifierfirstname;
    int verifierid;
    String verifierlastname;
    
    
    public ReportBean(){
        reportid = 0;
        reportname = "";
        reporttype = "";
        makerid = 0;
        verifierid = 0;
        status = "";
    }    
    public Date getDatecreated() {
        return datecreated;
    }
    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }
    public Date getEnddate() {
        return enddate;
    }
    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
    public String getMakerfirstname() {
        return makerfirstname;
    }
    public void setMakerfirstname(String makerfirstname) {
        this.makerfirstname = makerfirstname;
    }
    public int getMakerid() {
        return makerid;
    }
    public void setMakerid(int makerid) {
        this.makerid = makerid;
    }
    public String getMakerlastname() {
        return makerlastname;
    }
    public void setMakerlastname(String makerlastname) {
        this.makerlastname = makerlastname;
    }
    public int getReportid() {
        return reportid;
    }
    public void setReportid(int reportid) {
        this.reportid = reportid;
    }
    public String getReportname() {
        return reportname;
    }
    public void setReportname(String reportname) {
        this.reportname = reportname;
    }
    public String getReporttype() {
        return reporttype;
    }
    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }
    public Date getStartdate() {
        return startdate;
    }
    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getVerifierfirstname() {
        return verifierfirstname;
    }
    public void setVerifierfirstname(String verifierfirstname) {
        this.verifierfirstname = verifierfirstname;
    }
    public int getVerifierid() {
        return verifierid;
    }
    public void setVerifierid(int verifierid) {
        this.verifierid = verifierid;
    }
    public String getVerifierlastname() {
        return verifierlastname;
    }
    public void setVerifierlastname(String verifierlastname) {
        this.verifierlastname = verifierlastname;
    }
    
}
