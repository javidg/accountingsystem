package Beans;
import java.io.Serializable;
import java.sql.Date;
public class TransactionBean implements Serializable {
    int accountid; //maker
    String classification;
    Double creditamount;
    int creditbank;
    String creditbankname;
    int creditcoa;
    String creditcoaaloe;
    String creditcoaname;
    String creditdetail;
    int credittop;
    String credittopdetail;
    String credittopname;
    Date date = this.date;
    Double debitamount;
    int debitbank;
    String debitbankname;
    int debitcoa;
    String debitcoaaloe;
    String debitcoaname;
    String debitdetail;
    int debittop;
    String debittopdetail;
    String debittopname;
    Date enddate = this.enddate;
    Date startdate = this.startdate;
    String status;
    String transactiondetail;
    int transactionid;
    String verifierfirstname;
    int verifierid;    
    String verifierlastname;    
    public TransactionBean() {
        transactionid = 0;
        accountid = 0;
        verifierid = 0;
        verifierfirstname = "";
        verifierlastname = "";
        status = "";
        debitcoa = 0;
        debitdetail = "";
        debitbank = 0;
        debittop = 0;
        debittopdetail = "";
        debitamount = 0.0;
        creditcoa = 0;
        creditdetail = "";
        creditbank = 0;
        credittop = 0;
        credittopdetail = "";
        creditamount = 0.0;
        transactiondetail = "";
        debitbankname = "";
        debittopname = "";
        debitcoaname = "";
        debitcoaaloe= "";
        creditbankname = "";
        credittopname = "";
        creditcoaname = "";
        creditcoaaloe = "";
    }
    public int getAccountid() {
        return accountid;
    }
    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }
    public String getClassification() {
        return classification;
    }
    public void setClassification(String classification) {
        this.classification = classification;
    }
    public Double getCreditamount() {
        return creditamount;
    }
    public void setCreditamount(Double creditamount) {
        this.creditamount = creditamount;
    }
    public int getCreditbank() {
        return creditbank;
    }
    public void setCreditbank(int creditbank) {
        this.creditbank = creditbank;
    }
    public String getCreditbankname() {
        return creditbankname;
    }
    public void setCreditbankname(String creditbankname) {
        this.creditbankname = creditbankname;
    }
    public int getCreditcoa() {
        return creditcoa;
    }
    public void setCreditcoa(int creditcoa) {
        this.creditcoa = creditcoa;
    }
    public String getCreditcoaaloe() {
        return creditcoaaloe;
    }
    public void setCreditcoaaloe(String creditcoaaloe) {
        this.creditcoaaloe = creditcoaaloe;
    }
    public String getCreditcoaname() {
        return creditcoaname;
    }
    public void setCreditcoaname(String creditcoaname) {
        this.creditcoaname = creditcoaname;
    }
    public String getCreditdetail() {
        return creditdetail;
    }
    public void setCreditdetail(String creditdetail) {
        this.creditdetail = creditdetail;
    }
    public int getCredittop() {
        return credittop;
    }
    public void setCredittop(int credittop) {
        this.credittop = credittop;
    }
    public String getCredittopdetail() {
        return credittopdetail;
    }
    public void setCredittopdetail(String credittopdetail) {
        this.credittopdetail = credittopdetail;
    }
    public String getCredittopname() {
        return credittopname;
    }
    public void setCredittopname(String credittopname) {
        this.credittopname = credittopname;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public Double getDebitamount() {
        return debitamount;
    }
    public void setDebitamount(Double debitamount) {
        this.debitamount = debitamount;
    }
    public int getDebitbank() {
        return debitbank;
    }
    public void setDebitbank(int debitbank) {
        this.debitbank = debitbank;
    }
    public String getDebitbankname() {
        return debitbankname;
    }
    public void setDebitbankname(String debitbankname) {
        this.debitbankname = debitbankname;
    }
    public int getDebitcoa() {
        return debitcoa;
    }
    public void setDebitcoa(int debitcoa) {
        this.debitcoa = debitcoa;
    }
    public String getDebitcoaaloe() {
        return debitcoaaloe;
    }
    public void setDebitcoaaloe(String debitcoaaloe) {
        this.debitcoaaloe = debitcoaaloe;
    }
    public String getDebitcoaname() {
        return debitcoaname;
    }
    public void setDebitcoaname(String debitcoaname) {
        this.debitcoaname = debitcoaname;
    }
    public String getDebitdetail() {
        return debitdetail;
    }
    public void setDebitdetail(String debitdetail) {
        this.debitdetail = debitdetail;
    }
    public int getDebittop() {
        return debittop;
    }
    public void setDebittop(int debittop) {
        this.debittop = debittop;
    }
    public String getDebittopdetail() {
        return debittopdetail;
    }
    public void setDebittopdetail(String debittopdetail) {
        this.debittopdetail = debittopdetail;
    }
    public String getDebittopname() {
        return debittopname;
    }
    public void setDebittopname(String debittopname) {
        this.debittopname = debittopname;
    }
    public Date getEnddate() {
        return enddate;
    }
    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
    public Date getStartdate() {
        return startdate;
    }
    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getTransactiondetail() {
        return transactiondetail;
    }
    public void setTransactiondetail(String transactiondetail) {
        this.transactiondetail = transactiondetail;
    }
    public int getTransactionid() {
        return transactionid;
    }
    public void setTransactionid(int transactionid) {
        this.transactionid = transactionid;
    }
    public String getVerifierfirstname() {
        return verifierfirstname;
    }
    public void setVerifierfirstname(String verifierfirstname) {
        this.verifierfirstname = verifierfirstname;
    }
    public int getVerifierid() {
        return verifierid;
    }
    public void setVerifierid(int verifierid) {
        this.verifierid = verifierid;
    }
    public String getVerifierlastname() {
        return verifierlastname;
    }
    public void setVerifierlastname(String verifierlastname) {
        this.verifierlastname = verifierlastname;
    }
    
    
}
