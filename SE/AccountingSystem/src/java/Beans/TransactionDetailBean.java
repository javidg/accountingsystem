package Beans;
import java.io.Serializable;
/**
 *
 * @author user
 */
public class TransactionDetailBean implements Serializable {
    double amount;
    int coaid;
    String coaname;
    String debitcredit;
    int id;
    int transactionid;
    
    public TransactionDetailBean(){
        id = 0;
        transactionid = 0;
        coaid = 0;
        amount = 0.0;
        debitcredit = "";
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public int getCoaid() {
        return coaid;
    }
    public void setCoaid(int coaid) {
        this.coaid = coaid;
    }
    public String getCoaname() {
        return coaname;
    }
    public void setCoaname(String coaname) {
        this.coaname = coaname;
    }
    public String getDebitcredit() {
        return debitcredit;
    }
    public void setDebitcredit(String debitcredit) {
        this.debitcredit = debitcredit;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getTransactionid() {
        return transactionid;
    }
    public void setTransactionid(int transactionid) {
        this.transactionid = transactionid;
    }
     
    
}
