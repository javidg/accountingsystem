package Servlets;
import Beans.AccountBean;
import Beans.CompanyBean;
import Beans.PictureBean;
import Beans.ReportBean;
import Beans.TransactionBean;
import IO.AccountingIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class LoginServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            AccountingIO pass = new AccountingIO();
            if (request.getParameter("Login").equals("Login")) {
                String uri = "";
                String idnum = request.getParameter("idnum");
                String pin = request.getParameter("password");
                String password = "";
                try {
                    password = pass.hashPassword(pin);
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                //check if the servlet catches it
                log(idnum);
                log(password);
                AccountBean account = null;
                PictureBean picture = null;
                CompanyBean company = null;
                ArrayList<TransactionBean> transaction = null;
                ArrayList<ReportBean> report = null;
                try {
                    uri = pass.checkAccount(idnum, password); //To check if the user exists
                    log("uri : " + uri);
                    if (uri.equals("index.jsp")) {
                        request.setAttribute("error", "Invalid username and/or password.");
                    } else if (uri.equals("index.jsp,inactive")) {
                        uri = "index.jsp";
                        session.setAttribute("error", "Inactive user");
                    } else {
                        account = pass.getAccountInfo(idnum, password); //To store user's information
                        company = pass.getCompanyDetails();
                        picture = pass.getPicture(account.getAccountid());
                        transaction = pass.getTransaction();
                        report = pass.getAllReports();
                        session.setAttribute("userid", account.getEmployeeid());
                        session.setAttribute("accountid", account.getAccountid());
                        //Change of Company Name will take place after logging out since this is not always changed
                        session.setAttribute("companyname", company.getCompanyname());
                        session.setAttribute("companyaddress", company.getCompanyaddress());
                        session.setAttribute("companytelno", company.getCompanytelno());
                        session.setAttribute("userfname", account.getFirstname());
                        session.setAttribute("userlname", account.getLastname());
                        session.setAttribute("permission", account.getPermission());
                        session.setAttribute("picturename", picture.getPicturename());
                        session.setAttribute("account", account);
                        session.setAttribute("transaction", transaction);
                        session.setAttribute("report", report);
                        if (session.getAttribute("permission").equals("admin")) {
                            try {
                                session.setAttribute("accountctr", pass.countAll("AccountTable"));
                                session.setAttribute("bankctr", pass.countAll("BankTable"));
                                session.setAttribute("coactr", pass.countAll("ChartOfAccountsTable"));
                                session.setAttribute("reportctr", pass.countAll("ReportTable"));
                                session.setAttribute("transactionctr", pass.countAll("TransactionTable"));
                                session.setAttribute("topctr", pass.countAll("TypeOfPaymentTable"));
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                RequestDispatcher rd = request.getRequestDispatcher(uri);
                rd.forward(request, response);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
