package Servlets;
import Beans.AccountBean;
import Beans.BankBean;
import Beans.ChartOfAccountsBean;
import Beans.CompanyBean;
import Beans.PictureBean;
import Beans.TransactionBean;
import Beans.TypeOfPaymentBean;
import IO.AccountingIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet(name = "AdminServlet_1", urlPatterns = {"/AdminServlet_1"})
public class AdminServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            AccountingIO pass = new AccountingIO();
            PictureBean picture = null;
            AccountBean accountinfo = null;
            String uri = "";
            String accountid = request.getParameter("accountid");
            int id = (Integer) session.getAttribute("accountid");
            String userpermission = (String) session.getAttribute("permission");
            if (session.getAttribute("userid") != null) {
                if (request.getParameter("error") != null) {
                    uri = "/WEB-INF/jsp/error.jsp";
                }
                if (request.getParameter("xxx") != null) {
                    ArrayList<TransactionBean> transaction = null;
                    if (session.getAttribute("permission").equals("admin")) {
                        try {
                            picture = pass.getPicture(id);
                            accountinfo = pass.getAccountById(id);
                            session.setAttribute("userid", accountinfo.getEmployeeid());
                            session.setAttribute("userfname", accountinfo.getFirstname());
                            session.setAttribute("userlname", accountinfo.getLastname());
                            session.setAttribute("picturename", picture.getPicturename());
                            transaction = pass.getTransaction();
                            session.setAttribute("transaction", transaction);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/home.jsp";
                    } else if (session.getAttribute("permission").equals("accountant")) {
                        try {
                            picture = pass.getPicture(id);
                            accountinfo = pass.getAccountById(id);
                            session.setAttribute("userid", accountinfo.getEmployeeid());
                            session.setAttribute("userfname", accountinfo.getFirstname());
                            session.setAttribute("userlname", accountinfo.getLastname());
                            session.setAttribute("picturename", picture.getPicturename());
                            transaction = pass.getTransaction();
                            session.setAttribute("transaction", transaction);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/home.jsp";
                    } else if (session.getAttribute("permission").equals("employee")) {
                        try {
                            picture = pass.getPicture(id);
                            accountinfo = pass.getAccountById(id);
                            session.setAttribute("userid", accountinfo.getEmployeeid());
                            session.setAttribute("userfname", accountinfo.getFirstname());
                            session.setAttribute("userlname", accountinfo.getLastname());
                            session.setAttribute("picturename", picture.getPicturename());
                            transaction = pass.getTransaction();
                            session.setAttribute("transaction", transaction);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/home.jsp";
                    }
                }
                log("DEBUG: " + userpermission);
                if (userpermission.equals("admin")) {
                    if (request.getParameter("adduser") != null) {
                        uri = "/WEB-INF/jsp/adduser.jsp";
                    } else if (request.getParameter("addbank") != null) {
                        uri = "/WEB-INF/jsp/addbank.jsp";
                    } else if (request.getParameter("addchartofaccounts") != null) {
                        uri = "/WEB-INF/jsp/addchartofaccounts.jsp";
                    } else if (request.getParameter("addtypeofpayment") != null) {
                        uri = "/WEB-INF/jsp/addtypeofpayment.jsp";
                    } else if (request.getParameter("updateuser") != null) {
                        try {
                            ArrayList<AccountBean> account = null;
                            account = pass.getAccount();
                            request.setAttribute("account", account);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updateuser.jsp";
                    } else if (request.getParameter("updatebank") != null) {
                        try {
                            ArrayList<BankBean> bank = null;
                            bank = pass.getBank();
                            request.setAttribute("bank", bank);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatebank.jsp";
                    } else if (request.getParameter("updatechartofaccounts") != null) {
                        try {
                            ArrayList<ChartOfAccountsBean> coa = null;
                            coa = pass.getCOA();
                            request.setAttribute("coa", coa);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatechartofaccounts.jsp";
                    } else if (request.getParameter("updatetypeofpayment") != null) {
                        try {
                            ArrayList<TypeOfPaymentBean> top = null;
                            top = pass.getTOP();
                            request.setAttribute("top", top);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatetypeofpayment.jsp";
                    } else if (request.getParameter("verifytransaction") != null) {
                        try {
                            ArrayList<TransactionBean> transaction = null;
                            ArrayList<TransactionBean> Ctransaction = null;
                            transaction = pass.getPendingTransactionDetails();
                            Ctransaction = pass.getCanceledTransactionDetails();
                            request.setAttribute("transaction", transaction);
                            request.setAttribute("ctransaction", Ctransaction);
                            request.setAttribute("accountid", accountid);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/verifytransaction.jsp";
                    } else if (request.getParameter("updatecompany") != null) {
                        try {
                            ArrayList<CompanyBean> company = null;
                            company = pass.getCompany();
                            request.setAttribute("companylist", company);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatecompany.jsp";
                    } else if (request.getParameter("addreport") != null) {
                        uri = "/WEB-INF/jsp/addreport.jsp";
                    } else if (request.getParameter("updatereport") != null) {
                        uri = "/WEB-INF/jsp/updatereport.jsp";
                    } else if (request.getParameter("addtransaction") != null) {
                        log("Adding transaction");
                        try {
                            ArrayList<ChartOfAccountsBean> coa = null;
                            ArrayList<BankBean> bank = null;
                            ArrayList<TypeOfPaymentBean> top = null;
                            top = pass.getTOP();
                            coa = pass.getCOA();
                            bank = pass.getBank();
                            request.setAttribute("coa", coa);
                            request.setAttribute("bank", bank);
                            request.setAttribute("top", top);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/addtransactions.jsp";
                    } else if (request.getParameter("updatetransaction") != null) {
                        try {
                            ArrayList<ChartOfAccountsBean> coa = null;
                            ArrayList<BankBean> bank = null;
                            ArrayList<TypeOfPaymentBean> top = null;
                            ArrayList<TransactionBean> transaction = null;
                            ArrayList<TransactionBean> Ctransaction = null;
                            top = pass.getTOP();
                            coa = pass.getCOA();
                            bank = pass.getBank();
                            transaction = pass.getVerifiedTransactionDetails();
                            Ctransaction = pass.getCanceledTransactionDetails();
                            request.setAttribute("coa", coa);
                            request.setAttribute("bank", bank);
                            request.setAttribute("top", top);
                            request.setAttribute("transaction", transaction);
                            request.setAttribute("ctransaction", Ctransaction);
                            request.setAttribute("accountid", accountid);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatetransaction.jsp";
                    }
                } else if (userpermission.equals("accountant")) {
                    if (request.getParameter("addreport") != null) {
                        uri = "/WEB-INF/jsp/addreport.jsp";
                    } else if (request.getParameter("updatereport") != null) {
                        uri = "/WEB-INF/jsp/updatereport.jsp";
                    } else if (request.getParameter("addtransaction") != null) {
                        log("Adding transaction");
                        try {
                            ArrayList<ChartOfAccountsBean> coa = null;
                            ArrayList<BankBean> bank = null;
                            ArrayList<TypeOfPaymentBean> top = null;
                            top = pass.getTOP();
                            coa = pass.getCOA();
                            bank = pass.getBank();
                            request.setAttribute("coa", coa);
                            request.setAttribute("bank", bank);
                            request.setAttribute("top", top);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/addtransactions.jsp";
                    } else if (request.getParameter("verifytransaction") != null) {
                        try {
                            ArrayList<ChartOfAccountsBean> coa = null;
                            ArrayList<BankBean> bank = null;
                            ArrayList<TypeOfPaymentBean> top = null;
                            ArrayList<TransactionBean> transaction = null;
                            ArrayList<TransactionBean> Ctransaction = null;
                            top = pass.getTOP();
                            coa = pass.getCOA();
                            bank = pass.getBank();
                            transaction = pass.getVerifiedTransactionDetails();
                            Ctransaction = pass.getCanceledTransactionDetails();
                            request.setAttribute("coa", coa);
                            request.setAttribute("bank", bank);
                            request.setAttribute("top", top);
                            request.setAttribute("transaction", transaction);
                            request.setAttribute("ctransaction", Ctransaction);
                            request.setAttribute("accountid", accountid);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatetransaction.jsp";   
                    } else if (request.getParameter("updatetransaction") != null) {
                        try {
                            ArrayList<ChartOfAccountsBean> coa = null;
                            ArrayList<BankBean> bank = null;
                            ArrayList<TypeOfPaymentBean> top = null;
                            ArrayList<TransactionBean> transaction = null;
                            ArrayList<TransactionBean> Ctransaction = null;
                            top = pass.getTOP();
                            coa = pass.getCOA();
                            bank = pass.getBank();
                            transaction = pass.getVerifiedTransactionDetails();
                            Ctransaction = pass.getCanceledTransactionDetails();
                            request.setAttribute("coa", coa);
                            request.setAttribute("bank", bank);
                            request.setAttribute("top", top);
                            request.setAttribute("transaction", transaction);
                            request.setAttribute("ctransaction", Ctransaction);
                            request.setAttribute("accountid", accountid);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        uri = "/WEB-INF/jsp/updatetransaction.jsp";
                    }
                } else if (userpermission.equals("employee")) {
                } else {
                    uri = "index.jsp";
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(uri);
            rd.forward(request, response);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
