package Servlets;
import Beans.AccountBean;
import Beans.ReportBean;
import Beans.TransactionBean;
import Beans.TransactionDetailBean;
import IO.AccountingIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class UpdateServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        HttpSession session = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            AccountingIO pass = new AccountingIO();
            session = request.getSession();
            String userpermission = (String) session.getAttribute("permission");
            int accountid = (Integer) session.getAttribute("accountid");
            String uri = "";
            if (session.getAttribute("userid") != null) {
                String value = request.getParameter("updateinfo");
                if (value != null) {
                    if (userpermission.equals("admin")) {
                        if (value.equals("Delete BS Report")) {
                            ReportBean report = new ReportBean();
                            report.setStatus("balancesheet");
                            try {
                                pass.deleteBSReport(report);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println(e);
                                request.setAttribute("Emessage", "Error In Cancelling Report");
                            }
                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Deleted all Successfully");
                        } else if (value.equals("Delete IS Report")) {
                            ReportBean report = new ReportBean();
                            report.setStatus("incometstatement");
                            try {
                                pass.deleteISReport(report);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println(e);
                                request.setAttribute("Emessage", "Error In Cancelling Report");
                            }
                            uri = "ReportsServlet?addincomestatement=x";
                            request.setAttribute("Smessage", "Deleted all Successfully");
                        } else if (value.equals("Delete T Report")) {
                            try {
                                pass.deleteTReport();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println(e);
                                request.setAttribute("Emessage", "Error In Cancelling Report");
                            }
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Deleted all Successfully");
                        } else if (value.equals("Verify Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setVerifierid(accountid);
                                transaction.setStatus("verified");
                                try {
                                    pass.verifyTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Verifying Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Verified Transaction Successfully");
                        } else if (value.equals("Cancel Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("canceled");
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Canceling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("pending");
                                
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    log("error in cancalling transaction" + e.getMessage());
                                    request.setAttribute("Emessage", "Error In Canceling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Retrieved Successfully");
                        } else if (value.equals("Cancel Verified Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("canceled");
                                
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Canceling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }

                            uri = "AdminServlet?updatetransaction=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Verified Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("pending");
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?updatetransaction=x";
                            request.setAttribute("Smessage", "Retrieved Successfully, Please check pending Transactions");
                        } else if (value.equals("Update Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            ArrayList<Integer> coa = new ArrayList<Integer>();
                            ArrayList<Double> amount = new ArrayList<Double>();
                            ArrayList<Integer> detailid = new ArrayList<Integer>();
                            try {
                                if (request.getParameter("coa1") != null) {
                                    int coa1 = Integer.parseInt(request.getParameter("coa1"));
                                    coa.add(coa1);
                                }
                                if (request.getParameter("coa2") != null) {
                                    int coa2 = Integer.parseInt(request.getParameter("coa2"));
                                    coa.add(coa2);
                                }
                                if (request.getParameter("coa3") != null) {
                                    int coa3 = Integer.parseInt(request.getParameter("coa3"));
                                    coa.add(coa3);
                                }
                                if (request.getParameter("coa4") != null) {
                                    int coa4 = Integer.parseInt(request.getParameter("coa4"));
                                    coa.add(coa4);
                                }
                                if (request.getParameter("coa5") != null) {
                                    int coa5 = Integer.parseInt(request.getParameter("coa5"));
                                    coa.add(coa5);
                                }
                                if (request.getParameter("coa6") != null) {
                                    int coa6 = Integer.parseInt(request.getParameter("coa6"));
                                    coa.add(coa6);
                                }
                                if (request.getParameter("coa7") != null) {
                                    int coa7 = Integer.parseInt(request.getParameter("coa7"));
                                    coa.add(coa7);
                                }
                                if (request.getParameter("coa8") != null) {
                                    int coa8 = Integer.parseInt(request.getParameter("coa8"));
                                    coa.add(coa8);
                                }
                                if (request.getParameter("coa9") != null) {
                                    int coa9 = Integer.parseInt(request.getParameter("coa9"));
                                    coa.add(coa9);
                                }
                                log("coa : " + coa);
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                            }
                            try {
                                if (request.getParameter("amount1") != null) {
                                    double amount1 = Double.parseDouble(request.getParameter("amount1"));
                                    amount.add(amount1);
                                }
                                if (request.getParameter("amount2") != null) {
                                    double amount2 = Double.parseDouble(request.getParameter("amount2"));
                                    amount.add(amount2);
                                }
                                if (request.getParameter("amount3") != null) {
                                    double amount3 = Double.parseDouble(request.getParameter("amount3"));
                                    amount.add(amount3);
                                }
                                if (request.getParameter("amount4") != null) {
                                    double amount4 = Double.parseDouble(request.getParameter("amount4"));
                                    amount.add(amount4);
                                }
                                if (request.getParameter("amount5") != null) {
                                    double amount5 = Double.parseDouble(request.getParameter("amount5"));
                                    amount.add(amount5);
                                }
                                if (request.getParameter("amount6") != null) {
                                    double amount6 = Double.parseDouble(request.getParameter("amount6"));
                                    amount.add(amount6);
                                }
                                if (request.getParameter("amount7") != null) {
                                    double amount7 = Double.parseDouble(request.getParameter("amount7"));
                                    amount.add(amount7);
                                }
                                if (request.getParameter("amount8") != null) {
                                    double amount8 = Double.parseDouble(request.getParameter("amount8"));
                                    amount.add(amount8);
                                }
                                if (request.getParameter("amount9") != null) {
                                    double amount9 = Double.parseDouble(request.getParameter("amount9"));
                                    amount.add(amount9);
                                }
                                log("amount : " + amount);
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                            }
                            try {
                                if (request.getParameter("detailid1") != null) {
                                    int detailid1 = Integer.parseInt(request.getParameter("detailid1"));
                                    detailid.add(detailid1);
                                }
                                if (request.getParameter("detailid2") != null) {
                                    int detailid2 = Integer.parseInt(request.getParameter("detailid2"));
                                    detailid.add(detailid2);
                                }
                                if (request.getParameter("detailid3") != null) {
                                    int detailid3 = Integer.parseInt(request.getParameter("detailid3"));
                                    detailid.add(detailid3);
                                }
                                if (request.getParameter("detailid4") != null) {
                                    int detailid4 = Integer.parseInt(request.getParameter("detailid4"));
                                    detailid.add(detailid4);
                                }
                                if (request.getParameter("detailid5") != null) {
                                    int detailid5 = Integer.parseInt(request.getParameter("detailid5"));
                                    detailid.add(detailid5);
                                }
                                if (request.getParameter("detailid6") != null) {
                                    int detailid6 = Integer.parseInt(request.getParameter("detailid6"));
                                    detailid.add(detailid6);
                                }
                                if (request.getParameter("detailid7") != null) {
                                    int detailid7 = Integer.parseInt(request.getParameter("detailid7"));
                                    detailid.add(detailid7);
                                }
                                if (request.getParameter("detailid8") != null) {
                                    int detailid8 = Integer.parseInt(request.getParameter("detailid8"));
                                    detailid.add(detailid8);
                                }
                                if (request.getParameter("detailid9") != null) {
                                    int detailid9 = Integer.parseInt(request.getParameter("detailid9"));
                                    detailid.add(detailid9);
                                }
                                log("detailid : " + detailid);
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                            }
                            try {
                                java.sql.Date date = java.sql.Date.valueOf(request.getParameter("date"));
                                transaction.setDate(date);
                                log("date : " + date);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                log("Parsing Date Error : " + ex.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Date");
                            }
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setAccountid(Integer.parseInt(request.getParameter("accountid")));
                                transaction.setDebitcoa(Integer.parseInt(request.getParameter("coa1")));
                                transaction.setDebitbank(Integer.parseInt(request.getParameter("debitbank")));
                                transaction.setDebittop(Integer.parseInt(request.getParameter("debittypeofpayment")));
                                transaction.setCreditcoa(Integer.parseInt(request.getParameter("coa2")));
                                Double totaldebit = Double.parseDouble(request.getParameter("totaldebitamount"));
                                Double totalcredit = Double.parseDouble(request.getParameter("totalcreditamount"));
                                if (totaldebit != totalcredit) {
                                    request.setAttribute("Emessage", "Total Debit Amount and Total Credit Amount are not equal. Please Check.");
                                } else {
                                    transaction.setCreditamount(Double.parseDouble(request.getParameter("totalcreditamount")));
                                    transaction.setDebitamount(Double.parseDouble(request.getParameter("totaldebitamount")));
                                }
                                transaction.setStatus("pending");
                                transaction.setDebittopdetail(request.getParameter("debitdetailtop"));
                                transaction.setTransactiondetail(request.getParameter("detail"));
                                //transaction.setCreditamount(Double.parseDouble(request.getParameter("totalcreditamount")));
                                //transaction.setDebitamount(Double.parseDouble(request.getParameter("totaldebitamount")));
                                try {
                                    pass.updateTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Updating Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            TransactionDetailBean trandetail = new TransactionDetailBean();
                            for (int h = 0; h < detailid.size(); h++) {
                                for (int i = 0; i < coa.size(); i++) {
                                    for (int j = 0; j < amount.size(); j++) {
                                        trandetail.setId(detailid.get(h));
                                        trandetail.setCoaid(coa.get(h));
                                        trandetail.setAmount(amount.get(h));
                                    }
                                }
                                log("coa : " + coa.get(h));
                                log("coa : " + detailid.get(h));
                                log("coa : " + amount.get(h));
                                try {
                                    pass.updateTransactionDetail(trandetail);
                                    log("Updated transaction details : ");
                                } catch (SQLException ex) {
                                    log("Exception in updating transaction details : " + ex.getMessage());
                                }
                            }
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Transaction is Updated Successfully");
                        } else if (value.equals("Cancel BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("canceled");
                                report.setVerifierid(0);
                                
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Canceled BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("pending");
                                report.setVerifierid(0);
                                
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }

                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Retrieved Successfully");
                        } else if (value.equals("Cancel Verified BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setVerifierid(accountid);
                                report.setStatus("canceled");
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?updatebalancesheet=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Verified BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("pending");
                                report.setVerifierid(0);
                                
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Retrieved Successfully, Please check pending Balance Sheet reports");
                        } else if (value.equals("Update Report Name")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));

                                try {
                                    report.setReportname(request.getParameter("reportname"));
                                    pass.updateReportName(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?updatebalancesheet=x";
                            request.setAttribute("Smessage", "Changed Successfully");
                        } else if (value.equals("Cancel IS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setVerifierid(0);
                                report.setStatus("canceled");
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?addincomestatement=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Cancel Verified IS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setVerifierid(accountid);
                                report.setStatus("canceled");
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?updateincomestatement=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Canceled IS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("pending");
                                report.setVerifierid(0);
                                
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?addincomestatement=x";
                            request.setAttribute("Smessage", "Retrieved Successfully.");
                        } else if (value.equals("Update Profile")) {
                            AccountBean account = new AccountBean();
                            String password = request.getParameter("password");
                            String encodedpass = "";
                            account.setEmployeeid(request.getParameter("employeeid"));
                            account.setFirstname(request.getParameter("firstname"));
                            account.setLastname(request.getParameter("lastname"));
                            try {
                                account.setAccountid(Integer.parseInt(request.getParameter("accountid")));
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Account ID : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error in parsing account id");
                            }
                            if (password.equals("") || password == null) {
                                try {
                                    pass.updateUser(account);
                                    uri = "AdminServlet?xxx=x";
                                    request.setAttribute("Smessage", "Updated User Profile");
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    log("Exception : " + e);
                                    request.setAttribute("Emessage", "Error In Updating User Profile");
                                }
                            } else {
                                boolean strength = pass.checkPasswordStrength(password);
                                if (strength == false) {
                                    request.setAttribute("Emessage", "Password strength is weak");
                                } else if (strength == true) {
                                    try {
                                        encodedpass = pass.hashPassword(password);
                                        account.setPassword(encodedpass);
                                        pass.updateUser(account);
                                        request.setAttribute("Smessage", "Updated User Profile");
                                        uri = "AdminServlet?xxx=x";
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        log("Exception : " + e);
                                        session.setAttribute("Emessage", "Error In Updating User Profile");
                                    }
                                }
                            }
                        } else {
                            uri = "/WEB-INF/jsp/error.jsp";
                        }
                    } else if (userpermission.equals("employee")) {
                        if (value.equals("Update Profile")) {
                            AccountBean account = new AccountBean();
                            String password = request.getParameter("password");
                            String encodedpass = "";
                            account.setEmployeeid(request.getParameter("employeeid"));
                            account.setFirstname(request.getParameter("firstname"));
                            account.setLastname(request.getParameter("lastname"));
                            try {
                                account.setAccountid(Integer.parseInt(request.getParameter("accountid")));
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Account ID : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error in parsing account id");
                            }
                            if (password.equals("") || password == null) {
                                try {
                                    pass.updateUser(account);
                                    uri = "AdminServlet?xxx=x";
                                    request.setAttribute("Smessage", "Updated User Profile");
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    log("Exception : " + e);
                                    request.setAttribute("Emessage", "Error In Updating User Profile");
                                }
                            } else {
                                boolean strength = pass.checkPasswordStrength(password);
                                if (strength == false) {
                                    request.setAttribute("Emessage", "Password strength is weak");
                                } else if (strength == true) {
                                    try {
                                        encodedpass = pass.hashPassword(password);
                                        account.setPassword(encodedpass);
                                        pass.updateUser(account);
                                        request.setAttribute("Smessage", "Updated User Profile");
                                        uri = "AdminServlet?xxx=x";
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        log("Exception : " + e);
                                        session.setAttribute("Emessage", "Error In Updating User Profile");
                                    }
                                }
                            }
                        }
                    } else if (userpermission.equals("accountant")) {
                        if (value.equals("Cancel Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));

                                transaction.setStatus("canceled");
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Canceling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Update Profile")) {
                            AccountBean account = new AccountBean();
                            String password = request.getParameter("password");
                            String encodedpass = "";
                            account.setEmployeeid(request.getParameter("employeeid"));
                            account.setFirstname(request.getParameter("firstname"));
                            account.setLastname(request.getParameter("lastname"));
                            try {
                                account.setAccountid(Integer.parseInt(request.getParameter("accountid")));
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Account ID : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error in parsing account id");
                            }
                            if (password.equals("") || password == null) {
                                try {
                                    pass.updateUser(account);
                                    uri = "AdminServlet?xxx=x";
                                    request.setAttribute("Smessage", "Updated User Profile");
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    log("Exception : " + e);
                                    request.setAttribute("Emessage", "Error In Updating User Profile");
                                }
                            } else {
                                boolean strength = pass.checkPasswordStrength(password);
                                if (strength == false) {
                                    request.setAttribute("Emessage", "Password strength is weak");
                                } else if (strength == true) {
                                    try {
                                        encodedpass = pass.hashPassword(password);
                                        account.setPassword(encodedpass);
                                        pass.updateUser(account);
                                        request.setAttribute("Smessage", "Updated User Profile");
                                        uri = "AdminServlet?xxx=x";
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        log("Exception : " + e);
                                        session.setAttribute("Emessage", "Error In Updating User Profile");
                                    }
                                }
                            }
                        } else if (value.equals("Retrieve Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("pending");
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    log("error in cancalling transaction" + e.getMessage());
                                    request.setAttribute("Emessage", "Error In Canceling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Retrieved Successfully");
                        } else if (value.equals("Cancel Verified Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("canceled");
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Canceling Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?updatetransaction=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Verified Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setStatus("pending");
                                try {
                                    pass.cancelTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Transaction");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            
                            uri = "AdminServlet?updatetransaction=x";
                            request.setAttribute("Smessage", "Retrieved Successfully, Please check pending Transactions");
                        } else if (value.equals("Update Transaction")) {
                            TransactionBean transaction = new TransactionBean();
                            ArrayList<Integer> coa = new ArrayList<Integer>();
                            ArrayList<Double> amount = new ArrayList<Double>();
                            ArrayList<Integer> detailid = new ArrayList<Integer>();
                            try {
                                if (request.getParameter("coa1") != null) {
                                    int coa1 = Integer.parseInt(request.getParameter("coa1"));
                                    coa.add(coa1);
                                }
                                if (request.getParameter("coa2") != null) {
                                    int coa2 = Integer.parseInt(request.getParameter("coa2"));
                                    coa.add(coa2);
                                }
                                if (request.getParameter("coa3") != null) {
                                    int coa3 = Integer.parseInt(request.getParameter("coa3"));
                                    coa.add(coa3);
                                }
                                if (request.getParameter("coa4") != null) {
                                    int coa4 = Integer.parseInt(request.getParameter("coa4"));
                                    coa.add(coa4);
                                }
                                if (request.getParameter("coa5") != null) {
                                    int coa5 = Integer.parseInt(request.getParameter("coa5"));
                                    coa.add(coa5);
                                }
                                if (request.getParameter("coa6") != null) {
                                    int coa6 = Integer.parseInt(request.getParameter("coa6"));
                                    coa.add(coa6);
                                }
                                if (request.getParameter("coa7") != null) {
                                    int coa7 = Integer.parseInt(request.getParameter("coa7"));
                                    coa.add(coa7);
                                }
                                if (request.getParameter("coa8") != null) {
                                    int coa8 = Integer.parseInt(request.getParameter("coa8"));
                                    coa.add(coa8);
                                }
                                if (request.getParameter("coa9") != null) {
                                    int coa9 = Integer.parseInt(request.getParameter("coa9"));
                                    coa.add(coa9);
                                }
                                log("coa : " + coa);
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                            }
                            try {
                                if (request.getParameter("amount1") != null) {
                                    double amount1 = Double.parseDouble(request.getParameter("amount1"));
                                    amount.add(amount1);
                                }
                                if (request.getParameter("amount2") != null) {
                                    double amount2 = Double.parseDouble(request.getParameter("amount2"));
                                    amount.add(amount2);
                                }
                                if (request.getParameter("amount3") != null) {
                                    double amount3 = Double.parseDouble(request.getParameter("amount3"));
                                    amount.add(amount3);
                                }
                                if (request.getParameter("amount4") != null) {
                                    double amount4 = Double.parseDouble(request.getParameter("amount4"));
                                    amount.add(amount4);
                                }
                                if (request.getParameter("amount5") != null) {
                                    double amount5 = Double.parseDouble(request.getParameter("amount5"));
                                    amount.add(amount5);
                                }
                                if (request.getParameter("amount6") != null) {
                                    double amount6 = Double.parseDouble(request.getParameter("amount6"));
                                    amount.add(amount6);
                                }
                                if (request.getParameter("amount7") != null) {
                                    double amount7 = Double.parseDouble(request.getParameter("amount7"));
                                    amount.add(amount7);
                                }
                                if (request.getParameter("amount8") != null) {
                                    double amount8 = Double.parseDouble(request.getParameter("amount8"));
                                    amount.add(amount8);
                                }
                                if (request.getParameter("amount9") != null) {
                                    double amount9 = Double.parseDouble(request.getParameter("amount9"));
                                    amount.add(amount9);
                                }
                                log("amount : " + amount);
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                            }
                            try {
                                if (request.getParameter("detailid1") != null) {
                                    int detailid1 = Integer.parseInt(request.getParameter("detailid1"));
                                    detailid.add(detailid1);
                                }
                                if (request.getParameter("detailid2") != null) {
                                    int detailid2 = Integer.parseInt(request.getParameter("detailid2"));
                                    detailid.add(detailid2);
                                }
                                if (request.getParameter("detailid3") != null) {
                                    int detailid3 = Integer.parseInt(request.getParameter("detailid3"));
                                    detailid.add(detailid3);
                                }
                                if (request.getParameter("detailid4") != null) {
                                    int detailid4 = Integer.parseInt(request.getParameter("detailid4"));
                                    detailid.add(detailid4);
                                }
                                if (request.getParameter("detailid5") != null) {
                                    int detailid5 = Integer.parseInt(request.getParameter("detailid5"));
                                    detailid.add(detailid5);
                                }
                                if (request.getParameter("detailid6") != null) {
                                    int detailid6 = Integer.parseInt(request.getParameter("detailid6"));
                                    detailid.add(detailid6);
                                }
                                if (request.getParameter("detailid7") != null) {
                                    int detailid7 = Integer.parseInt(request.getParameter("detailid7"));
                                    detailid.add(detailid7);
                                }
                                if (request.getParameter("detailid8") != null) {
                                    int detailid8 = Integer.parseInt(request.getParameter("detailid8"));
                                    detailid.add(detailid8);
                                }
                                if (request.getParameter("detailid9") != null) {
                                    int detailid9 = Integer.parseInt(request.getParameter("detailid9"));
                                    detailid.add(detailid9);
                                }
                                log("detailid : " + detailid);
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                            }
                            try {
                                java.sql.Date date = java.sql.Date.valueOf(request.getParameter("date"));
                                transaction.setDate(date);
                                log("date : " + date);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                log("Parsing Date Error : " + ex.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Date");
                            }
                            try {
                                transaction.setTransactionid(Integer.parseInt(request.getParameter("transactionid")));
                                transaction.setAccountid(Integer.parseInt(request.getParameter("accountid")));
                                transaction.setDebitcoa(Integer.parseInt(request.getParameter("coa1")));
                                transaction.setDebitbank(Integer.parseInt(request.getParameter("debitbank")));
                                transaction.setDebittop(Integer.parseInt(request.getParameter("debittypeofpayment")));
                                transaction.setCreditcoa(Integer.parseInt(request.getParameter("coa2")));
                                Double totaldebit = Double.parseDouble(request.getParameter("totaldebitamount"));
                                Double totalcredit = Double.parseDouble(request.getParameter("totalcreditamount"));
                                if (totaldebit != totalcredit) {
                                    request.setAttribute("Emessage", "Total Debit Amount and Total Credit Amount are not equal. Please Check.");
                                } else {
                                    transaction.setCreditamount(Double.parseDouble(request.getParameter("totalcreditamount")));
                                    transaction.setDebitamount(Double.parseDouble(request.getParameter("totaldebitamount")));
                                }
                                transaction.setStatus("pending");
                                transaction.setDebittopdetail(request.getParameter("debitdetailtop"));
                                transaction.setTransactiondetail(request.getParameter("detail"));
                                //transaction.setCreditamount(Double.parseDouble(request.getParameter("totalcreditamount")));
                                //transaction.setDebitamount(Double.parseDouble(request.getParameter("totaldebitamount")));
                                try {
                                    pass.updateTransaction(transaction);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Updating Transaction");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Transaction ID");
                            }
                            TransactionDetailBean trandetail = new TransactionDetailBean();
                            for (int h = 0; h < detailid.size(); h++) {
                                for (int i = 0; i < coa.size(); i++) {
                                    for (int j = 0; j < amount.size(); j++) {
                                        trandetail.setId(detailid.get(h));
                                        trandetail.setCoaid(coa.get(h));
                                        trandetail.setAmount(amount.get(h));
                                    }
                                }
                                log("coa : " + coa.get(h));
                                log("coa : " + detailid.get(h));
                                log("coa : " + amount.get(h));
                                try {
                                    pass.updateTransactionDetail(trandetail);
                                    log("Updated transaction details : ");
                                } catch (SQLException ex) {
                                    log("Exception in updating transaction details : " + ex.getMessage());
                                }
                            }
                            uri = "AdminServlet?verifytransaction=x";
                            request.setAttribute("Smessage", "Transaction is Updated Successfully");
                        } else if (value.equals("Cancel BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("canceled");
                                report.setVerifierid(0);
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Canceled BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("pending");
                                report.setVerifierid(0);
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Retrieved Successfully");
                        } else if (value.equals("Cancel Verified BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setVerifierid(accountid);
                                report.setStatus("canceled");
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?updatebalancesheet=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Verified BS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));

                                report.setStatus("pending");
                                report.setVerifierid(0);
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?balancesheet=x";
                            request.setAttribute("Smessage", "Retrieved Successfully, Please check pending Balance Sheet reports");
                        } else if (value.equals("Update Report Name")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                try {
                                    report.setReportname(request.getParameter("reportname"));
                                    pass.updateReportName(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?updatebalancesheet=x";
                            request.setAttribute("Smessage", "Changed Successfully");
                        } else if (value.equals("Cancel IS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setVerifierid(0);
                                report.setStatus("canceled");
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?addincomestatement=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Cancel Verified IS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setVerifierid(accountid);
                                report.setStatus("canceled");
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }
                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?updateincomestatement=x";
                            request.setAttribute("Smessage", "Canceled Successfully");
                        } else if (value.equals("Retrieve Canceled IS Report")) {
                            ReportBean report = new ReportBean();
                            try {
                                report.setReportid(Integer.parseInt(request.getParameter("reportid")));
                                report.setStatus("pending");
                                report.setVerifierid(0);
                                try {
                                    pass.cancelReport(report);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println(e);
                                    request.setAttribute("Emessage", "Error In Cancelling Report");
                                }

                            } catch (NumberFormatException nfe) {
                                nfe.printStackTrace();
                                log("Parsing Transaction ID Error : " + nfe.getMessage());
                                request.setAttribute("Emessage", "Error In Parsing Report ID");
                            }
                            
                            uri = "ReportsServlet?addincomestatement=x";
                            request.setAttribute("Smessage", "Retrieved Successfully.");
                        }
                    } else {
                        uri = "/WEB-INF/jsp/error.jsp";
                    }
                } else {
                    uri = "index.jsp";
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(uri);
            rd.forward(request, response);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
