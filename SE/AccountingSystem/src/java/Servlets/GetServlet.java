package Servlets;
import Beans.AccountBean;
import Beans.BankBean;
import Beans.ChartOfAccountsBean;
import Beans.CompanyBean;
import Beans.ReportBean;
import Beans.TransactionBean;
import Beans.TransactionDetailBean;
import Beans.TypeOfPaymentBean;
import IO.AccountingIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class GetServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            AccountingIO database = new AccountingIO();
            boolean exists = false;
            boolean existsforupdate = false;
            String uri = "";
            String userpermission = (String) session.getAttribute("permission");
            //Checking of permission wont let you insert data in the database
            if ((session.getAttribute("userid") != null) && userpermission.equals("admin")) {
                if (request.getParameter("page").equals("account")) {
                    AccountBean account = new AccountBean();
                    String uppassword = request.getParameter("password");
                    String encodedPassword = "";
                    try {
                        int accountid = Integer.parseInt(request.getParameter("accountid"));
                        account.setEmployeeid(request.getParameter("employeeid"));
                        account.setFirstname(request.getParameter("firstname"));
                        account.setLastname(request.getParameter("lastname"));
                        account.setPermission(request.getParameter("permission"));
                        account.setStatus(request.getParameter("status"));
                        account.setAccountid(accountid);
                        if (uppassword.equals("") || uppassword == null) {
                            try {
                                exists = database.checkIfExists("AccountTable", "employeeid", request.getParameter("employeeid"));
                                existsforupdate = database.checkIfExistsForUpdate("AccountTable", "employeeid", request.getParameter("employeeid"), "accountid", accountid);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating user data <br>" + e);
                            }
                            if ((exists && existsforupdate) || (exists == false)) {
                                try {
                                    database.updateUser(account);
                                    out.println("Updated " + request.getParameter("firstname") + " " + request.getParameter("lastname") + "'s profile");
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println("Failed in updating user data <br>" + e);
                                }
                            } else {
                                out.println("Error : " + request.getParameter("employeeid") + " already exists in the database.");
                            }
                        } else {
                            boolean strength = database.checkPasswordStrength(uppassword);
                            try {
                                exists = database.checkIfExists("AccountTable", "employeeid", request.getParameter("employeeid"));
                                existsforupdate = database.checkIfExistsForUpdate("AccountTable", "employeeid", request.getParameter("employeeid"), "accountid", accountid);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating user data <br>" + e);
                            }
                            try {
                                encodedPassword = database.hashPassword(uppassword);
                                account.setPassword(encodedPassword);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in checking password strength <br>" + e);
                            }
                            if (exists && existsforupdate && strength == true) {
                                try {
                                    database.updateUser(account);
                                    out.println("Updated " + request.getParameter("firstname") + " " + request.getParameter("lastname") + " profile");
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    out.println("Failed in updating user data <br>" + e);
                                }
                            } else if (strength == false) {
                                out.println("Error : Password strength is weak");
                            }
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("company")) {
                    CompanyBean data = new CompanyBean();
                    try {
                        int id = Integer.parseInt(request.getParameter("companyid"));
                        int companytelno = Integer.parseInt(request.getParameter("companytelno"));
                        try {
                            data.setId(id);
                            data.setCompanyname(request.getParameter("companyname"));
                            data.setCompanyaddress(request.getParameter("companyaddress"));
                            data.setCompanytelno(companytelno);
                            database.updateCompany(data);
                            out.println("Company Data Updated Successfully!");
                            database.updateCompany(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                            out.println("Error in updating company data");
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("bank")) {
                    BankBean data = new BankBean();
                    try {
                        int bankid = Integer.parseInt(request.getParameter("bankid"));
                        try {
                            exists = database.checkIfExists("BankTable", "bankname", request.getParameter("bankname"));
                            existsforupdate = database.checkIfExistsForUpdate("BankTable", "bankname", request.getParameter("bankname"), "bankid", bankid);
                        } catch (SQLException e) {
                            log("Exception : " + e);
                            e.printStackTrace();
                            out.println(e.getMessage());
                        }
                        if ((exists && existsforupdate) || (exists == false)) {
                            try {
                                data.setBankname(request.getParameter("bankname"));
                                data.setBankid(bankid);
                                database.updateBank(data);
                                out.println("Bank Data Updated Successfully!");
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating bank data" + e);
                            }
                        } else if (exists && existsforupdate == false) {
                            out.println("Error  : " + request.getParameter("bankname") + " existed in the database!");
                        } else {
                            out.println("something went wrong in updating data");
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        out.println("Failed in parsisng bank id" + nfe);
                    }
                } else if (request.getParameter("page").equals("coa")) {
                    ChartOfAccountsBean data = new ChartOfAccountsBean();
                    try {
                        int coaid = Integer.parseInt(request.getParameter("coaid"));
                        try {
                            exists = database.checkIfExists("ChartOfAccountsTable", "coaname", request.getParameter("coaname"));
                            existsforupdate = database.checkIfExistsForUpdate("ChartOfAccountsTable", "coaname", request.getParameter("coaname"), "coaid", coaid);
                        } catch (SQLException e) {
                            log("Exception : " + e);
                            e.printStackTrace();
                            out.println(e.getMessage());
                        }
                        if ((exists && existsforupdate) || (exists == false)) {
                            try {
                                data.setCoaid(coaid);
                                data.setCoaname(request.getParameter("coaname"));
                                data.setDebitcredit(request.getParameter("debitcredit"));
                                data.setIncomeexpense(request.getParameter("incomeexpense"));
                                data.setClassification(request.getParameter("classification"));
                                data.setFs(request.getParameter("fs"));
                                data.setItrfs(request.getParameter("itrfs"));
                                data.setDefinition(request.getParameter("definition"));
                                database.updateCoa(data);
                                out.println("Chart Of Accounts Data Updated Successfully!");
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating chart of accounts data" + e);
                            }
                        } else if (exists && existsforupdate == false) {
                            out.println("Error  : " + request.getParameter("coaname") + " existed in the database!");
                        } else {
                            out.println("something went wrong in updating data");
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("top")) {
                    TypeOfPaymentBean data = new TypeOfPaymentBean();
                    try {
                        int topid = Integer.parseInt(request.getParameter("topid"));
                        try {
                            exists = database.checkIfExists("TypeOfPaymentTable", "topname", request.getParameter("topname"));
                            existsforupdate = database.checkIfExistsForUpdate("TypeOfPaymentTable", "topname", request.getParameter("topname"), "topid", topid);
                        } catch (SQLException e) {
                            log("Exception : " + e);
                            e.printStackTrace();
                            out.println(e.getMessage());
                        }
                        if ((exists && existsforupdate) || (exists == false)) {
                            try {
                                data.setTopid(topid);
                                data.setTopname(request.getParameter("topname"));
                                database.updateTop(data);
                                out.println("Updated: Type of payment data Updated Successfully!");
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating type of payment data" + e);
                            }
                        } else if (exists) {
                            out.println("Error  : " + request.getParameter("topname") + " existed in the database!");
                        } else {
                            out.println("something went wrong in updating data");
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("report")) {
                    ReportBean data = new ReportBean();
                    try {
                        int reportid = Integer.parseInt(request.getParameter("reportid"));
                        try {
                            exists = database.checkIfExists("ReportTable", "reportname", request.getParameter("reportname"));
                            existsforupdate = database.checkIfExistsForUpdate("ReportTable", "reportname", request.getParameter("reportname"), "reportid", reportid);
                        } catch (SQLException e) {
                            log("Exception : " + e);
                            e.printStackTrace();
                            out.println(e.getMessage());
                        }
                        if ((exists && existsforupdate) || (exists == false)) {
                            try {
                                data.setReportid(reportid);
                                data.setReportname(request.getParameter("reportname"));
                                database.updateReportName(data);
                                out.println("Updated Report Name Successfully! ");
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating type of payment data" + e);
                            }
                        } else if (exists) {
                            out.println("Error  : " + request.getParameter("topname") + " existed in the database!");
                        } else {
                            out.println("something went wrong in updating data");
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("transactiondetail")) {
                    ArrayList<TransactionDetailBean> transactiondetail = null;
                    try {
                        int transactionid = Integer.parseInt(request.getParameter("transactionid"));
                        log("transaction id " + transactionid);
                        try {
                            transactiondetail = database.getDebitCreditDetailsById(transactionid);
                            out.println("<!DOCTYPE html><html><body><b>Transaction Details :<b><br><br>");
                            out.println("<table style=\"border: 1px solid black;\"><tr style=\"border: 1px solid black;text-align: left;\"><th style=\"border: 1px solid black;text-align: left;\">Debit / Credit </th><th style=\"border: 1px solid black;text-align: left;\">Chart Of Accounts</th><th style=\"border: 1px solid black;text-align: left;\">Amount</th></tr>");
                            for (TransactionDetailBean data : transactiondetail) {
                                out.println("<tr><td style=\"border: 1px solid black;text-align: left;\">" + data.getDebitcredit() + "</td><td style=\"border: 1px solid black;text-align: left;\">" + data.getCoaname() + "</td><td style=\"border: 1px solid black;text-align: left;\">" + data.getAmount() + "</td></tr>");
                                log("transaction detail : " + data.getCoaname() + " " + data.getAmount());
                            }
                            out.println("</table></body></html>");
                        } catch (SQLException e) {
                            e.printStackTrace();
                            out.println("Failed in getting type of payment data" + e);
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("transaction")) {
                    int transactionid = Integer.parseInt(request.getParameter("transactionid"));
                    log("transactionid: " + transactionid);
                    BankBean bankname = null;
                    ChartOfAccountsBean debitcoaname = null;
                    ChartOfAccountsBean creditcoaname = null;
                    TypeOfPaymentBean topname = null;
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        ArrayList<TransactionDetailBean> transactiondetail = null;
                        transaction = database.getTransactionDetailsById(transactionid);
                        transactiondetail = database.getDebitCreditDetailsById(transactionid);
                        ArrayList<ChartOfAccountsBean> coa = null;
                        ArrayList<BankBean> bank = null;
                        ArrayList<TypeOfPaymentBean> top = null;
                        top = database.getTOP();
                        coa = database.getCOA();
                        bank = database.getBank();
                        request.setAttribute("transactionid", transactionid);
                        request.setAttribute("coa", coa);
                        request.setAttribute("bank", bank);
                        request.setAttribute("top", top);
                        request.setAttribute("transaction", transaction);
                        request.setAttribute("transactiondetail", transactiondetail);
                        uri = "/WEB-INF/jsp/updatetransactiondetails.jsp";
                    } catch (SQLException e) {
                        e.printStackTrace();
                        out.println("Failed in getting transaction details" + e);
                    }
                    RequestDispatcher rd = request.getRequestDispatcher(uri);
                    rd.forward(request, response);
                } else if (request.getParameter("page").equals("verifytransaction")) {
                    int transactionid = Integer.parseInt(request.getParameter("transactionid"));
                    log("transactionid: " + transactionid);
                    BankBean bankname = null;
                    ChartOfAccountsBean debitcoaname = null;
                    ChartOfAccountsBean creditcoaname = null;
                    TypeOfPaymentBean topname = null;
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        ArrayList<TransactionDetailBean> transactiondetail = null;
                        transaction = database.getTransactionDetailsById(transactionid);
                        transactiondetail = database.getDebitCreditDetailsById(transactionid);
                        ArrayList<ChartOfAccountsBean> coa = null;
                        ArrayList<BankBean> bank = null;
                        ArrayList<TypeOfPaymentBean> top = null;
                        top = database.getTOP();
                        coa = database.getCOA();
                        bank = database.getBank();
                        request.setAttribute("transactionid", transactionid);
                        request.setAttribute("coa", coa);
                        request.setAttribute("bank", bank);
                        request.setAttribute("top", top);
                        request.setAttribute("transaction", transaction);
                        request.setAttribute("transactiondetail", transactiondetail);
                        uri = "/WEB-INF/jsp/updateverifytransactiondetails.jsp";
                    } catch (SQLException e) {
                        e.printStackTrace();
                        out.println("Failed in getting transaction details" + e);
                    }
                    RequestDispatcher rd = request.getRequestDispatcher(uri);
                    rd.forward(request, response);
                }
            } else if ((session.getAttribute("userid") != null) && (userpermission.equals("admin")) || (userpermission.equals("accountant"))) {
                if (request.getParameter("page").equals("transactiondetail")) {
                    ArrayList<TransactionDetailBean> transactiondetail = null;
                    try {
                        int transactionid = Integer.parseInt(request.getParameter("transactionid"));
                        log("id " + transactionid);
                        try {
                            transactiondetail = database.getDebitCreditDetailsById(transactionid);
                            out.println("<!DOCTYPE html><html><body>");
                            out.println("Transaction Details :");
                            out.println("<table><tr><th>Debit / Credit </th><th>Chart Of Accounts</th><th>Amount</th></tr>");
                            for (TransactionDetailBean data : transactiondetail) {
                                out.println("<tr><td>" + data.getDebitcredit() + "</td><td>" + data.getCoaname() + "</td><td>" + data.getAmount() + "</td></tr>");
                                log("transaction detail : " + data.getCoaname() + " " + data.getAmount());
                            }
                            out.println("</table></body></html>");
                        } catch (SQLException e) {
                            e.printStackTrace();
                            out.println("Failed in getting type of transaction detail : " + e);
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("report")) {
                    ReportBean data = new ReportBean();
                    try {
                        int reportid = Integer.parseInt(request.getParameter("reportid"));
                        try {
                            exists = database.checkIfExists("ReportTable", "reportname", request.getParameter("reportname"));
                            existsforupdate = database.checkIfExistsForUpdate("ReportTable", "reportname", request.getParameter("reportname"), "reportid", reportid);
                        } catch (SQLException e) {
                            log("Exception : " + e);
                            e.printStackTrace();
                            out.println(e.getMessage());
                        }
                        if ((exists && existsforupdate) || (exists == false)) {
                            try {
                                data.setReportid(reportid);
                                data.setReportname(request.getParameter("reportname"));
                                database.updateReportName(data);
                                out.println("Updated Report Name Successfully! ");
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println("Failed in updating type of payment data" + e);
                            }
                        } else if (exists) {
                            out.println("Error  : " + request.getParameter("topname") + " existed in the database!");
                        } else {
                            out.println("something went wrong in updating data");
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                } else if (request.getParameter("page").equals("transaction")) {
                    int transactionid = Integer.parseInt(request.getParameter("transactionid"));
                    log("transactionid: " + transactionid);
                    BankBean bankname = null;
                    ChartOfAccountsBean debitcoaname = null;
                    ChartOfAccountsBean creditcoaname = null;
                    TypeOfPaymentBean topname = null;
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        ArrayList<TransactionDetailBean> transactiondetail = null;
                        transaction = database.getTransactionDetailsById(transactionid);
                        transactiondetail = database.getDebitCreditDetailsById(transactionid);
                        ArrayList<ChartOfAccountsBean> coa = null;
                        ArrayList<BankBean> bank = null;
                        ArrayList<TypeOfPaymentBean> top = null;
                        top = database.getTOP();
                        coa = database.getCOA();
                        bank = database.getBank();
                        request.setAttribute("transactionid", transactionid);
                        request.setAttribute("coa", coa);
                        request.setAttribute("bank", bank);
                        request.setAttribute("top", top);
                        request.setAttribute("transaction", transaction);
                        request.setAttribute("transactiondetail", transactiondetail);
                        uri = "/WEB-INF/jsp/updatetransactiondetails.jsp";
                    } catch (SQLException e) {
                        e.printStackTrace();
                        out.println("Failed in getting transaction details" + e);
                    }
                    RequestDispatcher rd = request.getRequestDispatcher(uri);
                    rd.forward(request, response);
                }
            } else {
                request.setAttribute("Emessage", "You're not allowed to perform that action");
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
