package Servlets;
import Beans.AccountBean;
import Beans.BankBean;
import Beans.ChartOfAccountsBean;
import Beans.PictureBean;
import Beans.ReportBean;
import Beans.TransactionBean;
import Beans.TransactionDetailBean;
import Beans.TypeOfPaymentBean;
import IO.AccountingIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class AddServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        try (PrintWriter out = response.getWriter()) {
            AccountingIO pass = new AccountingIO();
            HttpSession session = request.getSession();
            int accountid = (Integer) session.getAttribute("accountid");
            String uri = "";
            String msgexists = " already exists in the database.";
            String msgsuccess = " is added in the database";
            String userpermission = (String) session.getAttribute("permission");
            //Checking of permission wont let you insert data in the database
            if ((session.getAttribute("userid") != null) && userpermission.equals("admin")) {
                if (request.getParameter("adduser") != null) {
                    String checkstatus = "active";
                    String password = request.getParameter("password");
                    String confirm_password = request.getParameter("confirm_password");
                    boolean strength = pass.checkPasswordStrength(password);
                    boolean checkstrength;
                    checkstrength = strength != false;
                    boolean hassamepassword;
                    if (password.equals(confirm_password)) {
                        hassamepassword = true;
                    } else {
                        hassamepassword = false;
                    }
                    String employeeid = request.getParameter("employeeid");
                    String firstname = request.getParameter("firstname");
                    String lastname = request.getParameter("lastname");
                    String permission = request.getParameter("permission");
                    String encodedPassword = "";
                    boolean exists = false;
                    try {
                        //employee id is unique
                        exists = pass.checkIfExists("AccountTable", "employeeid", employeeid);
                        encodedPassword = pass.hashPassword(password);
                    } catch (SQLException e) {
                        log("Exception : " + e);
                        System.out.println(e);
                    }
                    if (exists) {
                        //working
                        request.setAttribute("Emessage", "The user " + msgexists);
                    } else if (checkstrength == false) {
                        //working
                        request.setAttribute("Emessage", "Password strength is weak. Enter an alphanumeric password (at least one numerical character).");
                    } else if (hassamepassword == false) {
                        //working
                        request.setAttribute("Emessage", "Passwords do not match");
                    } else if (checkstrength == true && hassamepassword == true) {
                        AccountBean user = new AccountBean();
                        user.setEmployeeid(employeeid);
                        user.setPassword(encodedPassword);
                        user.setFirstname(firstname);
                        user.setLastname(lastname);
                        user.setPermission(permission);
                        user.setStatus(checkstatus);
                        request.setAttribute("Smessage", firstname + " " + lastname + "'s profile" + msgsuccess);
                        try {
                            pass.addUser(user);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new user");
                        }
                        AccountBean lastid = null;
                        try {
                            lastid = pass.getLastAccount();
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in getting last account id");
                        }
                        PictureBean picture = new PictureBean();
                        picture.setAccountid(lastid.getAccountid());
                        picture.setPicturename("picture-1.png");
                        try {
                            pass.uploadPicture(picture);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new user");
                        }
                    }
                    uri = "/WEB-INF/jsp/adduser.jsp";
                } else if (request.getParameter("addbank") != null) {
                    String bankname = request.getParameter("bank");
                    boolean exists = false;
                    BankBean bank = new BankBean();
                    bank.setBankname(bankname);
                    try {
                        //bank name should be unique
                        exists = pass.checkIfExists("BankTable", "bankname", bankname);
                    } catch (SQLException e) {
                        log("Exception : " + e);
                        System.out.println(e);
                    }
                    if (exists) {
                        //working
                        request.setAttribute("Emessage", bankname + msgexists);
                    } else {
                        try {
                            //working
                            pass.addBank(bank);
                            request.setAttribute("Smessage", bankname + msgsuccess);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new bank");
                        }
                    }
                    uri = "/WEB-INF/jsp/addbank.jsp";
                } else if (request.getParameter("addtop") != null) {
                    String topname = request.getParameter("top");
                    TypeOfPaymentBean top = new TypeOfPaymentBean();
                    top.setTopname(topname);
                    boolean exists = false;
                    try {
                        //type of payment should be unique
                        exists = pass.checkIfExists("TypeOfPaymentTable", "topname", topname);
                    } catch (SQLException e) {
                        log("Exception : " + e);
                        e.printStackTrace();
                        System.out.println(e);
                    }
                    if (exists) {
                        //working
                        request.setAttribute("Emessage", topname + msgexists);
                    } else {
                        try {
                            //working
                            pass.addTOP(top);
                            request.setAttribute("Smessage", topname + msgsuccess);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new type of payment.");
                        }
                    }
                    uri = "/WEB-INF/jsp/addtypeofpayment.jsp";
                } else if (request.getParameter("addcoa") != null) {
                    String coaname = request.getParameter("coa");
                    ChartOfAccountsBean coa = new ChartOfAccountsBean();
                    coa.setCoaname(coaname);
                    coa.setDebitcredit(request.getParameter("debitcredit"));
                    coa.setIncomeexpense(request.getParameter("incomeexpense"));
                    coa.setFs(request.getParameter("fs"));
                    coa.setItrfs(request.getParameter("itr/fs"));
                    coa.setClassification(request.getParameter("classification"));
                    coa.setDefinition(request.getParameter("definition"));
                    boolean exists = false;
                    try {
                        //Chart of accounts should be unique
                        exists = pass.checkIfExists("ChartOfAccountsTable", "coaname", coaname);
                    } catch (SQLException e) {
                        log("Exception : " + e);
                        System.out.println(e);
                    }
                    if (exists) {
                        //working
                        request.setAttribute("Emessage", coaname + msgexists);
                    } else {
                        try {
                            //working
                            pass.addCOA(coa);
                            request.setAttribute("Smessage", coaname + msgsuccess);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new chart of accounts.");
                        }
                    }
                    uri = "/WEB-INF/jsp/addchartofaccounts.jsp";
                } else if (request.getParameter("verifyincomestatement") != null) {
                    ReportBean data = new ReportBean();
                    try {
                        int isid = Integer.parseInt(request.getParameter("isid"));
                        data.setReportid(isid);
                        try {
                            data.setVerifierid(accountid);
                            data.setStatus("verified");
                            pass.verifyISReport(data);
                            uri = "ReportsServlet?updateincomestatement=x";
                            request.setAttribute("Smessage", "Verified Income Statement");
                        } catch (Exception ex) {
                            request.setAttribute("Emessage", "Error In Verifying Report : " + ex.getMessage());
                            uri = "/WEB-INF/jsp/incomestatement.jsp";
                            log("Exception : " + ex.getMessage());
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing Report ID");
                    }
                } else if (request.getParameter("verifybalancesheet") != null) {
                    ReportBean data = new ReportBean();
                    try {
                        int reportid = Integer.parseInt(request.getParameter("reportid"));
                        data.setReportid(reportid);
                        log("reportid " + reportid);
                        try {
                            data.setVerifierid(accountid);
                            log("accountid " + accountid);
                            pass.verifyBalanceSheet(data);
                            request.setAttribute("Smessage", "Verified Balance Sheet");
                            uri = "ReportsServlet?balancesheet=x";
                        } catch (Exception ex) {
                            request.setAttribute("Emessage", "Error In Verifying Report : " + ex.getMessage());
                            log("Exception : " + ex.getMessage());
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing Report ID");
                    }
                } else if (request.getParameter("addtransaction") != null) {
                    //After adding a new transaction, it cannot get preloaded data again. HELP
                    TransactionBean transaction = new TransactionBean();
                    ArrayList<Integer> coa = new ArrayList<Integer>();
                    ArrayList<Double> amount = new ArrayList<Double>();
                    transaction.setAccountid(accountid);
                    try {
                        java.sql.Date date = java.sql.Date.valueOf(request.getParameter("date"));
                        transaction.setDate(date);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Adding Transaction : " + ex.getMessage());
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        if (request.getParameter("debitdescription") != null) {
                            int debitdescription = Integer.parseInt(request.getParameter("debitdescription"));
                            coa.add(debitdescription);
                        }
                        if (request.getParameter("creditdescription") != null) {
                            int creditdescription = Integer.parseInt(request.getParameter("creditdescription"));
                            coa.add(creditdescription);
                        }
                        if (request.getParameter("debitdescription_0") != null) {
                            int debitdescription_0 = Integer.parseInt(request.getParameter("debitdescription_0"));
                            coa.add(debitdescription_0);
                        }
                        if (request.getParameter("debitdescription_1") != null) {
                            int debitdescription_1 = Integer.parseInt(request.getParameter("debitdescription_1"));
                            coa.add(debitdescription_1);
                        }
                        if (request.getParameter("debitdescription_2") != null) {
                            int debitdescription_2 = Integer.parseInt(request.getParameter("debitdescription_2"));
                            coa.add(debitdescription_2);
                        }
                        if (request.getParameter("debitdescription_3") != null) {
                            int debitdescription_3 = Integer.parseInt(request.getParameter("debitdescription_3"));
                            coa.add(debitdescription_3);
                        }
                        if (request.getParameter("debitdescription_4") != null) {
                            int debitdescription_4 = Integer.parseInt(request.getParameter("debitdescription_4"));
                            coa.add(debitdescription_4);
                        }
                        if (request.getParameter("debitdescription_5") != null) {
                            int debitdescription_5 = Integer.parseInt(request.getParameter("debitdescription_5"));
                            coa.add(debitdescription_5);
                        }
                        if (request.getParameter("debitdescription_6") != null) {
                            int debitdescription_6 = Integer.parseInt(request.getParameter("debitdescription_6"));
                            coa.add(debitdescription_6);
                        }
                        if (request.getParameter("debitdescription_7") != null) {
                            int debitdescription_7 = Integer.parseInt(request.getParameter("debitdescription_7"));
                            coa.add(debitdescription_7);
                        }
                        if (request.getParameter("creditdescription_0") != null) {
                            int creditdescription_0 = Integer.parseInt(request.getParameter("creditdescription_0"));
                            coa.add(creditdescription_0);
                        }
                        if (request.getParameter("creditdescription_1") != null) {
                            int creditdescription_1 = Integer.parseInt(request.getParameter("creditdescription_1"));
                            coa.add(creditdescription_1);
                        }
                        if (request.getParameter("creditdescription_2") != null) {
                            int creditdescription_2 = Integer.parseInt(request.getParameter("creditdescription_2"));
                            coa.add(creditdescription_2);
                        }
                        if (request.getParameter("creditdescription_3") != null) {
                            int creditdescription_3 = Integer.parseInt(request.getParameter("creditdescription_3"));
                            coa.add(creditdescription_3);
                        }
                        if (request.getParameter("creditdescription_4") != null) {
                            int creditdescription_4 = Integer.parseInt(request.getParameter("creditdescription_4"));
                            coa.add(creditdescription_4);
                        }
                        log("coa : " + coa);
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                    try {
                        if (request.getParameter("debitamount") != null) {
                            double debitamount = Double.parseDouble(request.getParameter("debitamount"));
                            amount.add(debitamount);
                        }
                        if (request.getParameter("creditamount") != null) {
                            double creditamount = Double.parseDouble(request.getParameter("creditamount"));
                            amount.add(creditamount);
                        }
                        if (request.getParameter("debitamount_0") != null) {
                            double debitamount_0 = Double.parseDouble(request.getParameter("debitamount_0"));
                            amount.add(debitamount_0);
                        }
                        if (request.getParameter("debitamount_1") != null) {
                            double debitamount_1 = Double.parseDouble(request.getParameter("debitamount_1"));
                            amount.add(debitamount_1);
                        }
                        if (request.getParameter("debitamount_2") != null) {
                            double debitamount_2 = Double.parseDouble(request.getParameter("debitamount_2"));
                            amount.add(debitamount_2);
                        }
                        if (request.getParameter("debitamount_3") != null) {
                            double debitamount_3 = Double.parseDouble(request.getParameter("debitamount_3"));
                            amount.add(debitamount_3);
                        }
                        if (request.getParameter("debitamount_4") != null) {
                            double debitamount_4 = Double.parseDouble(request.getParameter("debitamount_4"));
                            amount.add(debitamount_4);
                        }
                        if (request.getParameter("debitamount_5") != null) {
                            double debitamount_5 = Double.parseDouble(request.getParameter("debitamount_5"));
                            amount.add(debitamount_5);
                        }
                        if (request.getParameter("debitamount_6") != null) {
                            double debitamount_6 = Double.parseDouble(request.getParameter("debitamount_6"));
                            amount.add(debitamount_6);
                        }
                        if (request.getParameter("debitamount_7") != null) {
                            double debitamount_7 = Double.parseDouble(request.getParameter("debitamount_7"));
                            amount.add(debitamount_7);
                        }
                        if (request.getParameter("creditamount_0") != null) {
                            double creditamount_0 = Double.parseDouble(request.getParameter("creditamount_0"));
                            amount.add(creditamount_0);
                        }
                        if (request.getParameter("creditamount_1") != null) {
                            double creditamount_1 = Double.parseDouble(request.getParameter("creditamount_1"));
                            amount.add(creditamount_1);
                        }
                        if (request.getParameter("creditamount_2") != null) {
                            double creditamount_2 = Double.parseDouble(request.getParameter("creditamount_2"));
                            amount.add(creditamount_2);
                        }
                        if (request.getParameter("creditamount_3") != null) {
                            double creditamount_3 = Double.parseDouble(request.getParameter("creditamount_3"));
                            amount.add(creditamount_3);
                        }
                        if (request.getParameter("creditamount_4") != null) {
                            double creditamount_4 = Double.parseDouble(request.getParameter("creditamount_4"));
                            amount.add(creditamount_4);
                        }
                        log("amount : " + amount);
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                    try {
                        transaction.setDebitcoa(Integer.parseInt(request.getParameter("debitdescription")));;
                        transaction.setDebitbank(Integer.parseInt(request.getParameter("debitbank")));
                        transaction.setDebittop(Integer.parseInt(request.getParameter("debittypeofpayment")));
                        transaction.setDebittopdetail(request.getParameter("debitdetailtop"));
                        Double debitamount = Double.parseDouble(request.getParameter("totaldebitamount"));
                        transaction.setDebitamount(debitamount);
                        transaction.setCreditcoa(Integer.parseInt(request.getParameter("creditdescription")));
                        transaction.setStatus("pending");
                        Double creditamount = Double.parseDouble(request.getParameter("totalcreditamount"));
                        transaction.setCreditamount(creditamount);
                        String detail = request.getParameter("detail");
                        transaction.setTransactiondetail(detail);
                        try {
                            pass.addTransaction(transaction);
                            request.setAttribute("Smessage", detail + msgsuccess);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new transaction : " + ex.getMessage());
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing ID");
                    }
                    TransactionBean lastid = null;
                    try {
                        lastid = pass.getLastTransaction();
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error in getting lastest transaction id : " + ex.getMessage());
                    }
                    TransactionDetailBean trandetail = new TransactionDetailBean();
                    trandetail.setTransactionid(lastid.getTransactionid());
                    for (int i = 0; i < coa.size(); i++) {
                        for (int j = 0; j < amount.size(); j++) {
                            trandetail.setCoaid(coa.get(i));
                            trandetail.setAmount(amount.get(i));
                        }
                        try {
                            pass.addTransactionDetails(trandetail);
                        } catch (SQLException ex) {
                            log("Exception in inserting transaction details : " + ex.getMessage());
                        }
                    }
                    uri = "/AdminServlet?addtransaction=Add";
                } else if (request.getParameter("addincomestatement") != null) {
                    TransactionBean data = new TransactionBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("1 " + startdate);
                        log("2 " + enddate);
                        request.setAttribute("sqlstartdate", startdate);
                        request.setAttribute("sqlenddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date : " + ex.getMessage());
                        uri = "/WEB-INF/jsp/incomestatement.jsp";
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        transaction = pass.getTransactionByDateRangeIS(data);
                        request.setAttribute("transaction", transaction);
                        uri = "/WEB-INF/jsp/displayincomestatement.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Generating Income Statement");
                    }
                } else if (request.getParameter("addbalancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = new ReportBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("1 " + startdate);
                        log("2 " + enddate);
                        request.setAttribute("startdate", startdate);
                        request.setAttribute("enddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        uri = "/WEB-INF/jsp/balancesheet.jsp";
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> currentassets = null;
                        ArrayList<TransactionBean> noncurrentassets = null;
                        ArrayList<TransactionBean> currentliabilities = null;
                        ArrayList<TransactionBean> noncurrentliabilities = null;
                        ArrayList<TransactionBean> capitalstock = null;
                        ArrayList<TransactionBean> retainedearnings = null;
                        currentassets = pass.getTransactionByDateRangeBS(data, "currentassets");
                        noncurrentassets = pass.getTransactionByDateRangeBS(data, "noncurrentassets");
                        currentliabilities = pass.getTransactionByDateRangeBS(data, "currentliabilities");
                        noncurrentliabilities = pass.getTransactionByDateRangeBS(data, "noncurrentliabilities");
                        capitalstock = pass.getTransactionByDateRangeBS(data, "capitalstock");
                        retainedearnings = pass.getTransactionByDateRangeBS(data, "retainedearnings");
                        request.setAttribute("assets", currentassets);
                        request.setAttribute("noncurrentassets", noncurrentassets);
                        request.setAttribute("currentliabilities", currentliabilities);
                        request.setAttribute("noncurrentliabilities", noncurrentliabilities);
                        request.setAttribute("capitalstock", capitalstock);
                        request.setAttribute("retainedearnings", retainedearnings);
                        uri = "/WEB-INF/jsp/displaybalancesheet.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Getting Transactions");
                    }
                } else if (request.getParameter("saveincomestatement") != null) {
                    ReportBean data = new ReportBean();
                    try {
                        data.setMakerid(accountid);
                        data.setReportname(request.getParameter("reportname"));
                        data.setReporttype("incomestatement");
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("startdate :" + startdate);
                        log("enddate :" + enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Inserting Income Statement Inputs : " + ex);
                        uri = "ReportsServlet?addincomestatement=x";
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        pass.addIncomeStatement(data);
                        request.setAttribute("Smessage", "Saved Income Statement");
                        uri = "ReportsServlet?addincomestatement=x";
                    } catch (SQLException ex) {
                        request.setAttribute("Emessage", "Error In Saving Income Statement : " + ex);
                        uri = "ReportsServlet?addincomestatement=x";
                        log("Exception : " + ex.getMessage());
                    }
                } else if (request.getParameter("savebalancesheet") != null) {
                    ReportBean report = new ReportBean();
                    report.setMakerid(accountid);
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        report.setStartdate(startdate);
                        report.setEnddate(enddate);
                        log("startdate : " + startdate);
                        log("enddate : " + enddate);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        report.setReportname(request.getParameter("reportname"));
                        report.setReporttype("balancesheet");
                        log("date : " + report.getStartdate());
                        log("date : " + report.getEnddate());
                        pass.addBalanceSheet(report);
                        request.setAttribute("Smessage", "Successfully Created");
                        uri = "ReportsServlet?balancesheet=x";
                    } catch (Exception ex) {
                        log("Exception : " + ex);
                        uri = "ReportsServlet?balancesheet=x";
                        request.setAttribute("Emessage", "Error" + ex.getMessage());
                    }
                } else if (request.getParameter("viewincomestatement") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = null;
                    try {
                        int isid = Integer.parseInt(request.getParameter("isid"));
                        try {
                            report = pass.getISById(isid);
                            data.setStartdate(report.getStartdate());
                            data.setEnddate(report.getEnddate());
                            request.setAttribute("isreport", report);
                        } catch (Exception ex) {
                            request.setAttribute("Emessage", "Error In Getting Date");
                            uri = "/WEB-INF/jsp/incomestatement.jsp";
                            log("Exception : " + ex.getMessage());
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing Report ID");
                    }
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        AccountBean account = null;
                        transaction = pass.getTransactionByDateRangeIS(data);
                        account = pass.getAccountById(report.getVerifierid());
                        request.setAttribute("transaction", transaction);
                        request.setAttribute("verifierfirstname", account.getFirstname());
                        request.setAttribute("verifierlastname", account.getLastname());
                        uri = "/WEB-INF/jsp/displayincomestatementreport.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex.getMessage());
                        request.setAttribute("Emessage", "Error In Generating Income Statement");
                    }
                } else if (request.getParameter("viewbalancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = new ReportBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("1 " + startdate);
                        log("2 " + enddate);
                        request.setAttribute("startdate", startdate);
                        request.setAttribute("enddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        uri = "/WEB-INF/jsp/balancesheet.jsp";
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> currentassets = null;
                        ArrayList<TransactionBean> noncurrentassets = null;
                        ArrayList<TransactionBean> currentliabilities = null;
                        ArrayList<TransactionBean> noncurrentliabilities = null;
                        ArrayList<TransactionBean> capitalstock = null;
                        ArrayList<TransactionBean> retainedearnings = null;
                        currentassets = pass.getTransactionByDateRangeBS(data, "currentassets");
                        noncurrentassets = pass.getTransactionByDateRangeBS(data, "noncurrentassets");
                        currentliabilities = pass.getTransactionByDateRangeBS(data, "currentliabilities");
                        noncurrentliabilities = pass.getTransactionByDateRangeBS(data, "noncurrentliabilities");
                        capitalstock = pass.getTransactionByDateRangeBS(data, "capitalstock");
                        retainedearnings = pass.getTransactionByDateRangeBS(data, "retainedearnings");
                        request.setAttribute("assets", currentassets);
                        request.setAttribute("noncurrentassets", noncurrentassets);
                        request.setAttribute("currentliabilities", currentliabilities);
                        request.setAttribute("noncurrentliabilities", noncurrentliabilities);
                        request.setAttribute("capitalstock", capitalstock);
                        request.setAttribute("retainedearnings", retainedearnings);
                        uri = "/WEB-INF/jsp/displaybalancesheetreport.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Getting Transactions");
                    }
                } else {
                    uri = "WEB-INF/jsp/error.jsp";
                }
            } else if ((session.getAttribute("userid") != null) && (userpermission.equals("accountant"))) {
                if (request.getParameter("addtransaction") != null) {
                    //After adding a new transaction, it cannot get preloaded data again. HELP
                    TransactionBean transaction = new TransactionBean();
                    ArrayList<Integer> coa = new ArrayList<Integer>();
                    ArrayList<Double> amount = new ArrayList<Double>();
                    transaction.setAccountid(accountid);
                    try {
                        java.sql.Date date = java.sql.Date.valueOf(request.getParameter("date"));
                        transaction.setDate(date);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Adding Transaction : " + ex.getMessage());
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        if (request.getParameter("debitdescription") != null) {
                            int debitdescription = Integer.parseInt(request.getParameter("debitdescription"));
                            coa.add(debitdescription);
                        }
                        if (request.getParameter("creditdescription") != null) {
                            int creditdescription = Integer.parseInt(request.getParameter("creditdescription"));
                            coa.add(creditdescription);
                        }
                        if (request.getParameter("debitdescription_0") != null) {
                            int debitdescription_0 = Integer.parseInt(request.getParameter("debitdescription_0"));
                            coa.add(debitdescription_0);
                        }
                        if (request.getParameter("debitdescription_1") != null) {
                            int debitdescription_1 = Integer.parseInt(request.getParameter("debitdescription_1"));
                            coa.add(debitdescription_1);
                        }
                        if (request.getParameter("debitdescription_2") != null) {
                            int debitdescription_2 = Integer.parseInt(request.getParameter("debitdescription_2"));
                            coa.add(debitdescription_2);
                        }
                        if (request.getParameter("debitdescription_3") != null) {
                            int debitdescription_3 = Integer.parseInt(request.getParameter("debitdescription_3"));
                            coa.add(debitdescription_3);
                        }
                        if (request.getParameter("debitdescription_4") != null) {
                            int debitdescription_4 = Integer.parseInt(request.getParameter("debitdescription_4"));
                            coa.add(debitdescription_4);
                        }
                        if (request.getParameter("debitdescription_5") != null) {
                            int debitdescription_5 = Integer.parseInt(request.getParameter("debitdescription_5"));
                            coa.add(debitdescription_5);
                        }
                        if (request.getParameter("debitdescription_6") != null) {
                            int debitdescription_6 = Integer.parseInt(request.getParameter("debitdescription_6"));
                            coa.add(debitdescription_6);
                        }
                        if (request.getParameter("debitdescription_7") != null) {
                            int debitdescription_7 = Integer.parseInt(request.getParameter("debitdescription_7"));
                            coa.add(debitdescription_7);
                        }
                        if (request.getParameter("creditdescription_0") != null) {
                            int creditdescription_0 = Integer.parseInt(request.getParameter("creditdescription_0"));
                            coa.add(creditdescription_0);
                        }
                        if (request.getParameter("creditdescription_1") != null) {
                            int creditdescription_1 = Integer.parseInt(request.getParameter("creditdescription_1"));
                            coa.add(creditdescription_1);
                        }
                        if (request.getParameter("creditdescription_2") != null) {
                            int creditdescription_2 = Integer.parseInt(request.getParameter("creditdescription_2"));
                            coa.add(creditdescription_2);
                        }
                        if (request.getParameter("creditdescription_3") != null) {
                            int creditdescription_3 = Integer.parseInt(request.getParameter("creditdescription_3"));
                            coa.add(creditdescription_3);
                        }
                        if (request.getParameter("creditdescription_4") != null) {
                            int creditdescription_4 = Integer.parseInt(request.getParameter("creditdescription_4"));
                            coa.add(creditdescription_4);
                        }
                        log("coa : " + coa);
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                    try {
                        if (request.getParameter("debitamount") != null) {
                            double debitamount = Double.parseDouble(request.getParameter("debitamount"));
                            amount.add(debitamount);
                        }
                        if (request.getParameter("creditamount") != null) {
                            double creditamount = Double.parseDouble(request.getParameter("creditamount"));
                            amount.add(creditamount);
                        }
                        if (request.getParameter("debitamount_0") != null) {
                            double debitamount_0 = Double.parseDouble(request.getParameter("debitamount_0"));
                            amount.add(debitamount_0);
                        }
                        if (request.getParameter("debitamount_1") != null) {
                            double debitamount_1 = Double.parseDouble(request.getParameter("debitamount_1"));
                            amount.add(debitamount_1);
                        }
                        if (request.getParameter("debitamount_2") != null) {
                            double debitamount_2 = Double.parseDouble(request.getParameter("debitamount_2"));
                            amount.add(debitamount_2);
                        }
                        if (request.getParameter("debitamount_3") != null) {
                            double debitamount_3 = Double.parseDouble(request.getParameter("debitamount_3"));
                            amount.add(debitamount_3);
                        }
                        if (request.getParameter("debitamount_4") != null) {
                            double debitamount_4 = Double.parseDouble(request.getParameter("debitamount_4"));
                            amount.add(debitamount_4);
                        }
                        if (request.getParameter("debitamount_5") != null) {
                            double debitamount_5 = Double.parseDouble(request.getParameter("debitamount_5"));
                            amount.add(debitamount_5);
                        }
                        if (request.getParameter("debitamount_6") != null) {
                            double debitamount_6 = Double.parseDouble(request.getParameter("debitamount_6"));
                            amount.add(debitamount_6);
                        }
                        if (request.getParameter("debitamount_7") != null) {
                            double debitamount_7 = Double.parseDouble(request.getParameter("debitamount_7"));
                            amount.add(debitamount_7);
                        }
                        if (request.getParameter("creditamount_0") != null) {
                            double creditamount_0 = Double.parseDouble(request.getParameter("creditamount_0"));
                            amount.add(creditamount_0);
                        }
                        if (request.getParameter("creditamount_1") != null) {
                            double creditamount_1 = Double.parseDouble(request.getParameter("creditamount_1"));
                            amount.add(creditamount_1);
                        }
                        if (request.getParameter("creditamount_2") != null) {
                            double creditamount_2 = Double.parseDouble(request.getParameter("creditamount_2"));
                            amount.add(creditamount_2);
                        }
                        if (request.getParameter("creditamount_3") != null) {
                            double creditamount_3 = Double.parseDouble(request.getParameter("creditamount_3"));
                            amount.add(creditamount_3);
                        }
                        if (request.getParameter("creditamount_4") != null) {
                            double creditamount_4 = Double.parseDouble(request.getParameter("creditamount_4"));
                            amount.add(creditamount_4);
                        }
                        log("amount : " + amount);
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                    try {
                        transaction.setDebitcoa(Integer.parseInt(request.getParameter("debitdescription")));;
                        transaction.setDebitbank(Integer.parseInt(request.getParameter("debitbank")));
                        transaction.setDebittop(Integer.parseInt(request.getParameter("debittypeofpayment")));
                        transaction.setDebittopdetail(request.getParameter("debitdetailtop"));
                        Double debitamount = Double.parseDouble(request.getParameter("totaldebitamount"));
                        transaction.setDebitamount(debitamount);
                        transaction.setCreditcoa(Integer.parseInt(request.getParameter("creditdescription")));
                        transaction.setStatus("pending");
                        Double creditamount = Double.parseDouble(request.getParameter("totalcreditamount"));
                        transaction.setCreditamount(creditamount);
                        String detail = request.getParameter("detail");
                        transaction.setTransactiondetail(detail);
                        try {
                            pass.addTransaction(transaction);
                            request.setAttribute("Smessage", detail + msgsuccess);
                        } catch (SQLException ex) {
                            log("Exception : " + ex);
                            request.setAttribute("Emessage", "Error in adding new transaction : " + ex.getMessage());
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing ID");
                    }
                    TransactionBean lastid = null;
                    try {
                        lastid = pass.getLastTransaction();
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error in getting lastest transaction id : " + ex.getMessage());
                    }
                    TransactionDetailBean trandetail = new TransactionDetailBean();
                    trandetail.setTransactionid(lastid.getTransactionid());
                    for (int i = 0; i < coa.size(); i++) {
                        for (int j = 0; j < amount.size(); j++) {
                            trandetail.setCoaid(coa.get(i));
                            trandetail.setAmount(amount.get(i));
                        }
                        try {
                            pass.addTransactionDetails(trandetail);
                        } catch (SQLException ex) {
                            log("Exception in inserting transaction details : " + ex.getMessage());
                        }
                    }
                    uri = "/AdminServlet?addtransaction=Add";
                } else if (request.getParameter("addincomestatement") != null) {
                    TransactionBean data = new TransactionBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("1 " + startdate);
                        log("2 " + enddate);
                        request.setAttribute("sqlstartdate", startdate);
                        request.setAttribute("sqlenddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date : " + ex.getMessage());
                        uri = "/WEB-INF/jsp/incomestatement.jsp";
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        transaction = pass.getTransactionByDateRangeIS(data);
                        request.setAttribute("transaction", transaction);
                        uri = "/WEB-INF/jsp/displayincomestatement.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Generating Income Statement");
                    }
                } else if (request.getParameter("addbalancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = new ReportBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        log("1 " + startdate);
                        log("2 " + enddate);
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        request.setAttribute("startdate", startdate);
                        request.setAttribute("enddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        uri = "/WEB-INF/jsp/balancesheet.jsp";
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> currentassets = null;
                        ArrayList<TransactionBean> noncurrentassets = null;
                        ArrayList<TransactionBean> currentliabilities = null;
                        ArrayList<TransactionBean> noncurrentliabilities = null;
                        ArrayList<TransactionBean> capitalstock = null;
                        ArrayList<TransactionBean> retainedearnings = null;
                        currentassets = pass.getTransactionByDateRangeBS(data, "currentassets");
                        noncurrentassets = pass.getTransactionByDateRangeBS(data, "noncurrentassets");
                        currentliabilities = pass.getTransactionByDateRangeBS(data, "currentliabilities");
                        noncurrentliabilities = pass.getTransactionByDateRangeBS(data, "noncurrentliabilities");
                        capitalstock = pass.getTransactionByDateRangeBS(data, "capitalstock");
                        retainedearnings = pass.getTransactionByDateRangeBS(data, "retainedearnings");
                        request.setAttribute("assets", currentassets);
                        request.setAttribute("noncurrentassets", noncurrentassets);
                        request.setAttribute("currentliabilities", currentliabilities);
                        request.setAttribute("noncurrentliabilities", noncurrentliabilities);
                        request.setAttribute("capitalstock", capitalstock);
                        request.setAttribute("retainedearnings", retainedearnings);
                        uri = "/WEB-INF/jsp/displaybalancesheet.jsp";
                    } catch (SQLException ex) {
                        uri = "WEB-INF/jsp/balancesheet.jsp";
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Getting Transactions");
                    }
                } else if (request.getParameter("saveincomestatement") != null) {
                    ReportBean data = new ReportBean();
                    try {
                        data.setMakerid(accountid);
                        data.setReportname(request.getParameter("reportname"));
                        data.setReporttype("incomestatement");
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("startdate :" + startdate);
                        log("enddate :" + enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Inserting Income Statement Inputs : " + ex);
                        uri = "ReportsServlet?addincomestatement=x";
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        pass.addIncomeStatement(data);
                        request.setAttribute("Smessage", "Saved Income Statement");
                        uri = "ReportsServlet?addincomestatement=x";
                    } catch (SQLException ex) {
                        request.setAttribute("Emessage", "Error In Saving Income Statement : " + ex);
                        uri = "ReportsServlet?addincomestatement=x";
                        log("Exception : " + ex.getMessage());
                    }
                } else if (request.getParameter("savebalancesheet") != null) {
                    ReportBean report = new ReportBean();
                    report.setMakerid(accountid);
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        report.setStartdate(startdate);
                        report.setEnddate(enddate);
                        log("startdate : " + startdate);
                        log("enddate : " + enddate);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        report.setReportname(request.getParameter("reportname"));
                        report.setReporttype("balancesheet");
                        log("date : " + report.getStartdate());
                        log("date : " + report.getEnddate());
                        pass.addBalanceSheet(report);
                        request.setAttribute("Smessage", "Successfully Created");
                        uri = "ReportsServlet?balancesheet=x";
                    } catch (Exception ex) {
                        log("Exception : " + ex);
                        uri = "ReportsServlet?balancesheet=x";
                        request.setAttribute("Emessage", "Error" + ex.getMessage());
                    }
                } else if (request.getParameter("viewincomestatement") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = null;
                    try {
                        int isid = Integer.parseInt(request.getParameter("isid"));
                        try {
                            report = pass.getISById(isid);
                            data.setStartdate(report.getStartdate());
                            data.setEnddate(report.getEnddate());
                            request.setAttribute("isreport", report);
                        } catch (Exception ex) {
                            request.setAttribute("Emessage", "Error In Getting Date");
                            uri = "/WEB-INF/jsp/incomestatement.jsp";
                            log("Exception : " + ex.getMessage());
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing ID");
                    }
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        AccountBean account = null;
                        transaction = pass.getTransactionByDateRangeIS(data);
                        account = pass.getAccountById(report.getVerifierid());
                        request.setAttribute("transaction", transaction);
                        request.setAttribute("verifierfirstname", account.getFirstname());
                        request.setAttribute("verifierlastname", account.getLastname());
                        uri = "/WEB-INF/jsp/displayincomestatementreport.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex.getMessage());
                        request.setAttribute("Emessage", "Error In Generating Income Statement");
                    }
                } else if (request.getParameter("viewbalancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = new ReportBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("1 " + startdate);
                        log("2 " + enddate);
                        request.setAttribute("startdate", startdate);
                        request.setAttribute("enddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        uri = "/WEB-INF/jsp/balancesheet.jsp";
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> currentassets = null;
                        ArrayList<TransactionBean> noncurrentassets = null;
                        ArrayList<TransactionBean> currentliabilities = null;
                        ArrayList<TransactionBean> noncurrentliabilities = null;
                        ArrayList<TransactionBean> capitalstock = null;
                        ArrayList<TransactionBean> retainedearnings = null;
                        currentassets = pass.getTransactionByDateRangeBS(data, "currentassets");
                        noncurrentassets = pass.getTransactionByDateRangeBS(data, "noncurrentassets");
                        currentliabilities = pass.getTransactionByDateRangeBS(data, "currentliabilities");
                        noncurrentliabilities = pass.getTransactionByDateRangeBS(data, "noncurrentliabilities");
                        capitalstock = pass.getTransactionByDateRangeBS(data, "capitalstock");
                        retainedearnings = pass.getTransactionByDateRangeBS(data, "retainedearnings");
                        request.setAttribute("assets", currentassets);
                        request.setAttribute("noncurrentassets", noncurrentassets);
                        request.setAttribute("currentliabilities", currentliabilities);
                        request.setAttribute("noncurrentliabilities", noncurrentliabilities);
                        request.setAttribute("capitalstock", capitalstock);
                        request.setAttribute("retainedearnings", retainedearnings);
                        uri = "/WEB-INF/jsp/displaybalancesheetreport.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Getting Transactions");
                    }
                } else {
                    uri = "WEB-INF/jsp/error.jsp";
                }
            } else if ((session.getAttribute("userid") != null) && (userpermission.equals("employee"))) {
                if (request.getParameter("viewincomestatement") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = null;
                    try {
                        int isid = Integer.parseInt(request.getParameter("isid"));
                        try {
                            report = pass.getISById(isid);
                            data.setStartdate(report.getStartdate());
                            data.setEnddate(report.getEnddate());
                            request.setAttribute("isreport", report);
                        } catch (Exception ex) {
                            request.setAttribute("Emessage", "Error In Getting Date");
                            uri = "/WEB-INF/jsp/incomestatement.jsp";
                            log("Exception : " + ex);
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        log("Parsing Report ID Error : " + nfe.getMessage());
                        request.setAttribute("Emessage", "Error In Parsing ID");
                    }
                    try {
                        ArrayList<TransactionBean> transaction = null;
                        transaction = pass.getTransactionByDateRangeIS(data);
                        request.setAttribute("transaction", transaction);
                        uri = "/WEB-INF/jsp/displayincomestatementreport.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Generating Income Statement");
                    }
                } else if (request.getParameter("viewbalancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    ReportBean report = new ReportBean();
                    try {
                        java.sql.Date startdate = java.sql.Date.valueOf(request.getParameter("startdate"));
                        java.sql.Date enddate = java.sql.Date.valueOf(request.getParameter("enddate"));
                        data.setStartdate(startdate);
                        data.setEnddate(enddate);
                        log("1 " + startdate);
                        log("2 " + enddate);
                        request.setAttribute("startdate", startdate);
                        request.setAttribute("enddate", enddate);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        uri = "/WEB-INF/jsp/balancesheet.jsp";
                        ex.printStackTrace();
                        log("Exception : " + ex.getMessage());
                    }
                    try {
                        ArrayList<TransactionBean> currentassets = null;
                        ArrayList<TransactionBean> noncurrentassets = null;
                        ArrayList<TransactionBean> currentliabilities = null;
                        ArrayList<TransactionBean> noncurrentliabilities = null;
                        ArrayList<TransactionBean> capitalstock = null;
                        ArrayList<TransactionBean> retainedearnings = null;
                        currentassets = pass.getTransactionByDateRangeBS(data, "currentassets");
                        noncurrentassets = pass.getTransactionByDateRangeBS(data, "noncurrentassets");
                        currentliabilities = pass.getTransactionByDateRangeBS(data, "currentliabilities");
                        noncurrentliabilities = pass.getTransactionByDateRangeBS(data, "noncurrentliabilities");
                        capitalstock = pass.getTransactionByDateRangeBS(data, "capitalstock");
                        retainedearnings = pass.getTransactionByDateRangeBS(data, "retainedearnings");
                        request.setAttribute("assets", currentassets);
                        request.setAttribute("noncurrentassets", noncurrentassets);
                        request.setAttribute("currentliabilities", currentliabilities);
                        request.setAttribute("noncurrentliabilities", noncurrentliabilities);
                        request.setAttribute("capitalstock", capitalstock);
                        request.setAttribute("retainedearnings", retainedearnings);
                        uri = "/WEB-INF/jsp/displaybalancesheetreport.jsp";
                    } catch (SQLException ex) {
                        log("Exception : " + ex);
                        request.setAttribute("Emessage", "Error In Getting Transactions");
                    }
                } else {
                    uri = "WEB-INF/jsp/error.jsp";
                }
            } else {
                uri = "WEB-INF/jsp/error.jsp";
            }
            if (!uri.isEmpty()) {
                RequestDispatcher rd = request.getRequestDispatcher(uri);
                rd.forward(request, response);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
