package Servlets;
import Beans.AccountBean;
import Beans.ReportBean;
import Beans.TransactionBean;
import IO.AccountingIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class ReportsServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        try (PrintWriter out = response.getWriter()) {
            AccountingIO pass = new AccountingIO();
            HttpSession session = request.getSession();
            String uri = "";
            if (session.getAttribute("userid") != null) {
                if (request.getParameter("addincomestatement") != null) {
                    try {
                        ArrayList<ReportBean> ISreport = null;
                        ArrayList<ReportBean> report = null;
                        ArrayList<AccountBean> account = null;
                        ISreport = pass.getIncomeStatement();
                        account = pass.getAccount();
                        report = pass.getAllReports();
                        
                        request.setAttribute("ISreport", ISreport);
                        request.setAttribute("account", account);
                        session.setAttribute("report", report);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        out.println(ex);
                    }
                    uri = "/WEB-INF/jsp/incomestatement.jsp";
                } else if (request.getParameter("updateincomestatement") != null) {
                    try {
                        ArrayList<ReportBean> ISreport = null;
                        ArrayList<ReportBean> report = null;
                        ArrayList<AccountBean> account = null;
                        
                        ISreport = pass.getIncomeStatement();
                        account = pass.getAccount();
                        report = pass.getAllReports();
                        
                        request.setAttribute("ISreport", ISreport);
                        request.setAttribute("account", account);
                        session.setAttribute("report", report);
                    } catch (Exception ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        out.println(ex);
                    }
                    uri = "/WEB-INF/jsp/updateincomestatement.jsp";
                } else if (request.getParameter("balancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    try {
                        ArrayList<ReportBean> report = null;
                        ArrayList<ReportBean> Creport = null;
                        ArrayList<ReportBean> allreport = null;
                        
                        allreport = pass.getAllReports();
                        report = pass.getBalanceSheetReport();
                        Creport = pass.getCanceledBalanceSheetReport();
                        
                        request.setAttribute("report", report);
                        request.setAttribute("Creport", Creport);
                        session.setAttribute("report", allreport);
                    } catch (SQLException ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        ex.printStackTrace();
                    }
                    uri = "/WEB-INF/jsp/balancesheet.jsp";
                } else if (request.getParameter("updatebalancesheet") != null) {
                    TransactionBean data = new TransactionBean();
                    try {
                        ArrayList<ReportBean> report = null;
                        ArrayList<ReportBean> Creport = null;
                        ArrayList<ReportBean> allreport = null;
                        
                        report = pass.getVerifiedBalanceSheetReport();
                        Creport = pass.getCanceledBalanceSheetReport();
                        allreport = pass.getAllReports();
                        
                        request.setAttribute("report", report);
                        request.setAttribute("Creport", Creport);
                        session.setAttribute("report", allreport);
                    } catch (SQLException ex) {
                        request.setAttribute("Emessage", "Error In Getting Date");
                        ex.printStackTrace();
                    }
                    uri = "/WEB-INF/jsp/updatebalancesheet.jsp";
                } else {
                    uri = "/WEB-INF/jsp/error.jsp";
                }
            } else {
                uri = "index.jsp";
            }
            if (!uri.isEmpty()) {
                RequestDispatcher rd = request.getRequestDispatcher(uri);
                rd.forward(request, response);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
