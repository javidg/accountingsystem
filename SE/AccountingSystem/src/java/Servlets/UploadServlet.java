package Servlets;
import Beans.PictureBean;
import IO.AccountingIO;
import static com.sun.javafx.util.Utils.split;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.io.FileUtils;
@MultipartConfig
public class UploadServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8;image/*");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String uri = "";
            AccountingIO database = new AccountingIO();
            PictureBean lastPictureId = null;
            try {
                lastPictureId = database.getLastPicture();
            } catch (SQLException ex) {
                log("Retrieve last picture id error: " + ex.getStackTrace());
            }
            int newPictureId = lastPictureId.getPictureid() + 1;
            out.println(newPictureId);
            String filePath = "";
            ServletContext ctxt = request.getServletContext();
            String directory = ctxt.getInitParameter("file-upload-path");
            Part file = request.getPart("image");
            if (file.getSize() > 100000) {
                uri = "AdminServlet?xxx=x";
                session.setAttribute("Errormsg", "Picture is too large");
            } else {
                InputStream content = file.getInputStream();
                String fileName = file.getSubmittedFileName();
                String[] fnameSplit = split(fileName, ".");
                if (!fnameSplit[1].equals("jpg") && !fnameSplit[1].equals("jpeg") && !fnameSplit[1].equals("png")) {
                    uri = "AdminServlet?xxx=x";
                    session.setAttribute("Errormsg", "Invalid type of file");
                    //out.println("Please file type invalid");
                } else if (fnameSplit[1].equals("jpg") || fnameSplit[1].equals("jpeg") || fnameSplit[1].equals("png")) {
                    try {
                        String newFileName = fileName.replace(fnameSplit[0], "picture-" + newPictureId);
                        File destination = new File(directory, newFileName);
                        filePath = newFileName;
                        FileUtils.copyInputStreamToFile(content, destination);
                        //session.setAttribute("Successmsg", "Picture Uploaded");
                        //out.println("File uploaded!");
                    } catch (Exception e) {
                        session.setAttribute("Errormsg", "Error in uploading image");
                        uri = "AdminServlet?xxx=x";
                        e.printStackTrace();
                        //out.println("File name already exists");
                    }
                    Integer accountid = (Integer) session.getAttribute("accountid");
                    PictureBean data = new PictureBean();
                    data.setAccountid(accountid);
                    data.setPicturename(filePath);
                    try {
                        database.uploadPicture(data);
                        session.setAttribute("Successmsg", "Picture Inseted");
                        uri = "AdminServlet?xxx=x";
                    } catch (SQLException ex) {
                        log("Insert picture error: " + ex.getStackTrace());
                        session.setAttribute("Errormsg", "Error in inserting picture");
                        uri = "AdminServlet?xxx=x";
                    }
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(uri);
            rd.forward(request, response);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
