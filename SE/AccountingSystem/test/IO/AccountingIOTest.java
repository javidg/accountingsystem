package IO;

import Beans.BankBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AccountingIOTest {

    AccountingIO instance = new AccountingIO();

    Connection conn = null;
    String driver;
    String pass;
    Statement stmt = null;
    String url;
    String user;

    public AccountingIOTest() {
        ResourceBundle sql = ResourceBundle.getBundle("IO.SQL");
        this.driver = sql.getString("driver");
        this.url = sql.getString("url");
        this.user = sql.getString("user");
        this.pass = sql.getString("password");
    }

    @Before
    public void setUp() throws SQLException {
    }

    @After
    public void tearDown() throws SQLException {

    }

    //WORKING
    @Test
    public void testHashPassword() throws Exception {
        System.out.println("HashPassword initialization");
        String pin = "pass1234";
        String expResult = "vZTc2ib8y05o1qMfm1qsC1ca4mbYImIOkB736+OhHU8=";
        String result = instance.hashPassword(pin);

        assertEquals(expResult, result);

        System.out.println(expResult + "," + result);
    }

    @Test
    public void testDeleteBank() throws Exception { 
        System.out.println("DeleteBank Initialization");
        ArrayList<BankBean> bank = new ArrayList<BankBean>();
        Boolean expResult = true;
        String bankname = "testbanknameDELETE";
        BankBean data = new BankBean();
        data.setBankname(bankname);
        addBank(data); 

        instance.deleteBank(data); //what im testing     
        bank = getBank(); 

        boolean result = false;
        if (checkIfExists("BankTable", "bankname", "testbanknameDELETE") == false) {
            result = true;
        }

        assertEquals(expResult, result);
        System.out.println(expResult + "," + result);
    
    }
    
    @Test
    public void testAddBank() throws Exception { 
        System.out.println("AddBank initialization");
    
         ArrayList<BankBean> bank = new ArrayList<BankBean>();

        String bankname = "testbanknameADD";
        boolean expResult = true;
        BankBean data = new BankBean();
        data.setBankname(bankname);
        instance.addBank(data); 
        
        boolean result = false;
        if ((checkIfExists("BankTable", "bankname", "testbanknameADD")) == true) { 
            result = true;
        }
        assertEquals(expResult, result);

        deleteBank(data); 
        System.out.println(expResult + "," + result);
    
    }

    @Test
    public void testGetBank() throws Exception { 
        System.out.println ("getBank initialization");
        ArrayList<BankBean> bank = new ArrayList<BankBean>();

        String bankname = "testbanknameGET";
        BankBean data = new BankBean();
        data.setBankname(bankname);
        addBank(data);
        boolean expResult = true;
        bank = instance.getBank();    //what im testing
        boolean result = false;
        if ((checkIfExists("BankTable", "bankname", "testbanknameGET")) == true) { 
            result = true;
        }
        assertEquals(expResult, result);
        deleteBank(data); 
        System.out.println(expResult + "," + result);
    
    }

    @Test
    public void testUpdateBank() throws Exception {
        ArrayList<BankBean> bank = new ArrayList<BankBean>();
        Boolean expResult = true;
        String banknameold = "testbanknameOLD";
        String banknamenew = "testbanknameNEW";

        BankBean data = new BankBean();
        data.setBankid(100);
        data.setBankname(banknameold);
        addBank(data);

        BankBean dataupdate = new BankBean();
        dataupdate.setBankid(100);
        dataupdate.setBankname(banknamenew);
        addBank(dataupdate);

        instance.updateBank(dataupdate);    //what im testing
        boolean result = false;
        
        if ((checkIfExists("BankTable", "bankname", "testbanknameNEW")) == true) { 
            result = true;
        } else {
            result = false;
        }
        assertEquals(expResult, result);

        deleteBank(data); 
        deleteBank(dataupdate);
    }

    //Methods to be used
    public void deleteBank(BankBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;

            String preparedSQL = "delete from BankTable where bankname = ?";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getBankname());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void addBank(BankBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            PreparedStatement ps = null;

            String preparedSQL = "insert into BankTable(bankname) values(?)";
            ps = conn.prepareStatement(preparedSQL);
            ps.setString(1, data.getBankname());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void updateBank(BankBean data) throws SQLException {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            Statement upd = conn.createStatement();
            String sqlUpdate = "UPDATE BankTable SET bankname = ? WHERE bankid = ?";
            PreparedStatement ps = conn.prepareStatement(sqlUpdate);
            ps.setString(1, data.getBankname());
            ps.setInt(2, data.getBankid());
            ps.executeUpdate();
            System.out.println(sqlUpdate);
            conn.commit();
        } catch (SQLException ex) {
            if (conn != null) {
                conn.rollback();
            }
            throw ex; //rethrow so caller can handle the error
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean checkIfExists(String tablename, String columnname, String columnvalue) throws SQLException {
        boolean exists = false;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            String sql = "select * from " + tablename + " where " + columnname + " = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, columnvalue);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                exists = true;
            }

            return exists;
        } catch (SQLException e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public ArrayList<BankBean> getBank() throws SQLException {
        ArrayList<BankBean> bank = new ArrayList<BankBean>();
        try {
            conn = DriverManager.getConnection(url, user, pass);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "select * from BankTable";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                BankBean data = new BankBean();
                data.setBankid(rs.getInt("bankid"));
                data.setBankname(rs.getString("bankname"));

                bank.add(data);
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return bank;
    }

}
