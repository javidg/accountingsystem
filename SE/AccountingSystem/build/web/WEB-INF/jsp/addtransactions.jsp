<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?updatetransaction=Update'>Need the list of transactions? Click here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title> ${companyname} AIS - Add Transactions </title>
        <jsp:include page="/include/head.jsp"/>
        <script>
            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        </script>
        <script type="text/javascript">
            function Validate() {
                var debitamount = document.getElementById("nursupertag").value;
                var creditamount = document.getElementById("nursupertag2").value;
                if (debitamount !== creditamount) {
                    swal("Error!", "The amount for the debit and credit is not balance. ", "error");
                    return false;
                }
                return true;
            }
        </script>
        <script><jsp:include page="/js/debitcredit.js"/></script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container-fluid">
            <div class="col-sm-12"><center><h3>Add Transaction</h3></center></div>
        </div>
        <form action="AddServlet" method="post">
            <div class="container-fluid">
                <div class="col-sm-12">
                    <center><h3>Date:</h3>
                        <!-- Working error handling for mistyped date !-->
                        <input type="Date" id="datepicker" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"  name="date" placeholder="YYYY-MM-DD" required></center> 
                </div>   
            </div>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <center><h3 class="panel-title"><strong>Debit</strong></h3></center>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="myTable"  class=" table order-list1">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select name = "debitdescription" id="debitdescription" class="form-control" autofocus="" required>
                                                        <option value="selected" selected disabled>Choose Chart of Accounts here</option>
                                                        <c:forEach var="row" items="${coa}">
                                                            <c:if test="${row.debitcredit == 'debit'}">
                                                                <option value="${row.coaid}">${row.coaname}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select> 
                                                </td>
                                                <td class="text-right"><input type="number" name="debitamount" min="0" id='debitamount' placeholder="Enter the Amount" class="form-control" autofocus="" required onkeyup="calculateGrandTotalDebit()"></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                        <td colspan="5" style="text-align: left;">
                                            <input type="button" class="btn btn-lg btn-block " id="addrow1" value="Add Another Debit" />
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="thick-line text-right"><strong>Total Debit</strong></td>
                                            <td class="thick-line text-right"> PhP <span id="grandtotaldebit"></span>
                                                <input type="hidden" id="nursupertag" name="totaldebitamount">  </td>
                                            <td class="thick-line"></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <center><h3 class="panel-title"><strong>Credit</strong></h3></center>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="myTable"  class=" table order-list2">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select name = "creditdescription" id="creditdescription" class="form-control" autofocus="" required>
                                                        <option selected disabled>Choose Chart of Accounts here</option>
                                                        <c:forEach var="row" items="${coa}">    
                                                            <c:if test="${row.debitcredit == 'credit'}">
                                                                <option value="${row.coaid}">${row.coaname}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select> 
                                                </td>
                                                <td class="text-right">
                                                    <input type="number" name="creditamount" min="0" id='creditamount' placeholder="Enter the Amount" class="form-control" autofocus="" required onkeyup="calculateGrandTotalCredit()">
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                        <td colspan="5" style="text-align: left;">
                                            <input type="button" class="btn btn-lg btn-block " id="addrow2" value="Add Another Credit" />
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="thick-line text-right"><strong>Total Credit</strong></td>
                                            <td class="thick-line text-right"> PhP  <span id="grandtotalcredit"></span>
                                                <input type="hidden" id="nursupertag2" name="totalcreditamount"> </td>
                                            <td class="thick-line"></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                        
            <br>
            <div class="container-fluid">
                <center>
                    <div class="col-sm-12">
                        Bank: <br>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <select name = "debitbank" class="form-control" autofocus="" required>
                                <option selected disabled>Choose here</option>
                                <c:forEach var="row" items="${bank}">    
                                    <option value="${row.bankid}">${row.bankname}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <br>    
                    <div class="col-sm-12">
                        Type of Payment: <br>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <select name = "debittypeofpayment" class="form-control" autofocus="" required>
                                <option selected disabled>Choose here</option>
                                <c:forEach var="row" items="${top}">    
                                    <option value="${row.topid}">${row.topname}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="col-sm-12">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            Details of Type of Payment:<br>
                            <input type="text" name="debitdetailtop" placeholder="Enter Detail of the Type of Payment" class="form-control" autofocus="" required>
                        </div>
                    </div>
                    <br>    
                    <div class="col-sm-12">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            Transaction Detail:<br>
                            <input type="text" name="detail" placeholder="Enter Detail of the Transaction" class="form-control" autofocus="" required>
                        </div>
                    </div>
                </center>  
                <br><br>
                <div class="container-fluid">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-2">
                        <br><br>
                        <input type="reset" class="btn btn-default">
                        <input type="submit" class="btn btn-success" name="addtransaction" id="grandtotal" onclick="return Validate()" value="Add" >
                    </div>
                </div>
        </form>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
