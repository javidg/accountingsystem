<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?addtypeofpayment=Add'>Type of Payment not in the list? Add here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update Type Of Payment</title>
        <jsp:include page="/include/head.jsp"/>
        <style>.table-responsive {height:180px;}</style>
        <script><jsp:include page="/js/pagination.js"/></script>
        <script>
            function edit_row(no) {
                document.getElementById("edit_button" + no).style.display = "none";
                document.getElementById("save_button" + no).style.display = "block";
                var topid = document.getElementById("topid_row" + no);
                var topname = document.getElementById("topname_row" + no);
                var topid_data = topid.innerHTML;
                var topname_data = topname.innerHTML;
                topid.innerHTML = "<input type='hidden' name='topid' id='topid_text" + no + "' value='" + topid_data + "'>";
                topname.innerHTML = "<input type='text' class='form-control' name='topname' id='topname_text" + no + "' value='" + topname_data + "'>";
            }
            function save_row(no) {
                var topid_val = document.getElementById("topid_text" + no).value;
                var topname_val = document.getElementById("topname_text" + no).value;
                var contextpath = "${contextpath}";
                document.getElementById("topid_row" + no).innerHTML = topid_val;
                document.getElementById("topname_row" + no).innerHTML = topname_val;
                document.getElementById("edit_button" + no).style.display = "block";
                document.getElementById("save_button" + no).style.display = "none";
                if (topname_val === "") {
                    swal("Error!", "Type of Payment name is blank", "error");
                    setTimeout(function () {
                        location.reload();
                    }, 3500);
                } else {
                    $.ajax
                            ({
                                type: 'post',
                                url: contextpath + "/GetServlet",
                                data: {
                                    page: "top",
                                    topid: topid_val,
                                    topname: topname_val,
                                },
                                success: function (response) {
                                    if (response.indexOf("Error") !== -1) {
                                        swal("Error!", response, "error");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3500);
                                    } else {
                                        swal("Updated!", response, "success");
                                    }
                                },
                                error: function (response) {
                                    swal("Error!", response, "error");
                                }
                            });
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Update Type Of Payment</h3>
            <jsp:include page="/include/search.jsp"/>
            <br><br>
            <table id="myTable" class="table table-hover">  
                <thead>
                    <tr>
                        <th> Type Of Payment </th>
                        <th> Edit </th>
                    </tr>
                </thead> 
                <c:forEach var="item" items="${top}">
                    <tbody>
                        <tr>
                            <td id="topid_row${item.topid}" style="display: none;" >${item.topid}</td>                           
                            <td id="topname_row${item.topid}">${item.topname}</td>
                            <td>   
                                <input type="button" id="edit_button${item.topid}" value="Edit" class="btn btn-default" onclick="edit_row(${item.topid})">
                                <input type="button" id="save_button${item.topid}" style="display: none;"  value="Update" class="btn btn-success" onclick="save_row(${item.topid})">
                            </td>   
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg pager" id="myPager"></ul>
            </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
