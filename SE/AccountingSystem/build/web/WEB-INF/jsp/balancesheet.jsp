<%-- 
    Document   : balancesheet
    Created on : 09 9, 17, 6:24:29 PM
    Author     : user
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Balance Sheet</title>
        <jsp:include page="/include/head.jsp"/>
        <script><jsp:include page="/js/pagination.js"/></script>
        <style>
            .affix {
                top: 20 px;
            }
        </style>
        <script>
            $(function () {
                $("#datepicker").datepicker({
                    gotoCurrent: true,
                    dateFormat: "yy-mm-dd",
                });
            });
            function myFunction() {
                var x = document.getElementById("datepicker").value;
                $(function () {
                    $("#datepicker2").datepicker({
                        gotoCurrent: true,
                        dateFormat: "yy-mm-dd",
                        minDate: x,
                        numberOfMonths: 1
                    });
                });
            }
        </script> 
        <script>
            function edit_row(no) {
                document.getElementById("edit_button" + no).style.display = "none";
                document.getElementById("save_button" + no).style.display = "block";
                var reportid = document.getElementById("reportid_row" + no);
                var reportname = document.getElementById("reportname_row" + no);
                var reportid_data = reportid.innerHTML;
                var reportname_data = reportname.innerHTML;
                reportid.innerHTML = "<input type='hidden' name='reportid' id='reportid_text" + no + "' value='" + reportid_data + "'>";
                reportname.innerHTML = "<input type='text' class='form-control' name='reportname' id='reportname_text" + no + "' value='" + reportname_data + "'>";
            }
            function save_row(no) {
                var reportid_val = document.getElementById("reportid_text" + no).value;
                var reportname_val = document.getElementById("reportname_text" + no).value;
                var contextpath = "${contextpath}";
                document.getElementById("reportid_row" + no).innerHTML = reportid_val;
                document.getElementById("reportname_row" + no).innerHTML = reportname_val;
                document.getElementById("edit_button" + no).style.display = "block";
                document.getElementById("save_button" + no).style.display = "none";
                if (reportname_val === "") {
                    swal("Error!", "Report name is blank", "error");
                    setTimeout(function () {
                        location.reload();
                    }, 3500);
                } else if (reportname_val.contains("!") || reportname_val.contains("#") || reportname_val.contains("?") || reportname_val.contains("+") || reportname_val.contains("*") || reportname_val.contains("%") || reportname_val.contains("$") || reportname_val.contains(";")) {
                    swal("Error!", "Report Name contains symbols!", "error");
                    setTimeout(function () {
                        location.reload();
                    }, 3500);
                } else {
                    $.ajax
                            ({
                                type: 'post',
                                url: contextpath + "/GetServlet",
                                data: {
                                    page: "report",
                                    reportid: reportid_val,
                                    reportname: reportname_val,
                                },
                                success: function (response) {
                                    if (response.indexOf("Error") !== -1) {
                                        swal("Error!", response, "error");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3500);
                                    } else {
                                        swal("Updated!", response, "success");
                                    }
                                },
                                error: function (response) {
                                    swal("Error!", response, "error");
                                }
                            });
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <div class="row">
                <h1 style="text-align:left" >Balance Sheet</h1>
                <p>Pending Reports</p>
                <c:if test="${(permission.equals(('admin'))||(permission.equals('accountant')))}">
                    <div class="col-sm-12" align="right">
                        <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">Generate Report</button>
                    </div>
                </c:if> <br><br><br>
                <jsp:include page="/include/search.jsp"/>
                <br>
                <br>
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">Add Balance Sheet</h3>
                            </div>
                            <div class="modal-body">        
                                <form action="AddServlet" method="post">
                                    Start Date:
                                    <input type="Date" id="datepicker" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"  name="startdate" placeholder="YYYY-MM-DD" onchange="myFunction()" required>
                                    End Date:
                                    <input type="Date" id="datepicker2" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"  name="enddate" placeholder="YYYY-MM-DD" required><br><br>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="reset" class="btn btn-default" value="Reset">
                                        <input type="submit" class="btn btn-success" name="addbalancesheet" value="Submit">
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-hover">  
                        <thead>
                            <tr><th>Date Created</th>
                                <th>Report Name</th> 
                                <th>Date of Report</th>
                                <th>Prepared By</th>
                                <th>View</th>
                                    <c:if test="${permission.equals('admin')}">
                                    <th>Verify</th>
                                    </c:if>
                                    <c:if test="${(permission.equals('admin')) || (permission.equals('accountant'))}">  
                                    <th> Edit Report Name </th>    
                                    <th> Cancel </th>    
                                    </c:if>  
                            </tr>
                        </thead>  
                        <c:forEach var="item" items="${report}">
                            <tbody>
                                <tr>
                                    <td id="reportid_row${item.reportid}" style="display: none;" >${item.reportid}</td>
                                    <td>${item.datecreated} </td>  
                                    <td id="reportname_row${item.reportid}">${item.reportname}</td> 
                                    <td>${item.startdate} to ${item.enddate}</td>
                                    <td>${item.makerfirstname} ${item.makerlastname}</td>
                                    <td>
                                        <form action="AddServlet" method="post">
                                            <input type="hidden" name="reportid" value="${item.reportid}">
                                            <input type="hidden" name="startdate" value="${item.startdate}">
                                            <input type="hidden" name="enddate" value="${item.enddate}">
                                            <input type="submit" class="btn btn-success" name="viewbalancesheet" value="View">
                                        </form>
                                    </td>
                                    <c:if test="${permission.equals('admin')}">
                                        <td>
                                            <form action="AddServlet" method="post">
                                                <input type="hidden" name="reportid" value="${item.reportid}">
                                                <input type="submit" class="btn btn-success" name="verifybalancesheet" value="Verify">
                                            </form>
                                        </td>
                                    </c:if>
                                    <c:if test="${(permission.equals('admin')) || (permission.equals('accountant'))}">
                                        <td>
                                            <input type="button" id="edit_button${item.reportid}" value="Edit" class="btn btn-default" onclick="edit_row(${item.reportid})">
                                            <input type="button" id="save_button${item.reportid}" style="display: none;"  value="Update" class="btn btn-success" onclick="save_row(${item.reportid})">
                                        </td>    
                                        <td>
                                            <form action="UpdateServlet" method="post">
                                                <input type="hidden" value ="${item.reportid}" name="reportid">
                                                <button type="submit" class="btn btn-danger" value="Cancel BS Report" name="updateinfo"><span class="glyphicon glyphicon-thumbs-down"> </span></button> 
                                            </form>
                                        </td>
                                    </c:if>
                                </tr>
                            </tbody>
                        </c:forEach>
                    </table>
                    <c:if test="${(permission.equals('admin')) || (permission.equals('accountant'))}">
                        <div class="col-sm-12" align="right">
                            <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#cancel">See Canceled Reports</button> 
                        </div>
                        <div class="modal fade" id="cancel" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3 class="modal-title">Canceled Reports</h3>
                                    </div>
                                    <div class="modal-body">
                                        <table id="myTable" class="table table-hover">  
                                            <thead>
                                                <tr><th> Date Created</th>
                                                    <th> Report Name </th> 
                                                    <th> Start Date </th>
                                                    <th> End Date </th> 
                                                    <th> Status </th>
                                                        <c:if test="${(permission.equals('admin')) || (permission.equals('accountant'))}">
                                                        <th> Retrieve </th>
                                                        </c:if>
                                                </tr>
                                            </thead>
                                            <c:forEach var="citem" items="${Creport}">
                                                <c:if test="${citem.verifierid == '0'}">
                                                    <tbody>
                                                        <tr>
                                                            <td>${citem.datecreated} </td>  
                                                            <td>${citem.reportname}</td>  
                                                            <td>${citem.startdate}</td> 
                                                            <td>${citem.enddate} </td>
                                                            <td>${citem.status} </td>
                                                            <c:if test="${(permission.equals('admin')) || (permission.equals('accountant'))}">
                                                                <td>
                                                                    <form action="UpdateServlet" method="post">
                                                                        <input type="hidden" value ="${citem.reportid}" name="reportid">
                                                                        <button type="submit" class="btn btn-success" value="Retrieve Canceled BS Report" name="updateinfo"><span class="glyphicon glyphicon-thumbs-up"> </span></button> 
                                                                    </form>
                                                                </td>
                                                            </c:if>
                                                        </tr>
                                                    </tbody>
                                                </c:if>
                                            </c:forEach>
                                        </table> 
                                    </div>
                                    <div class="modal-footer">
                                        <c:if test="${permission.equals('admin')}">  
                                            <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-floppy-remove" > </span> Archive All</button>
                                        </c:if>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <div class="modal fade" id="delete" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <center><h3 class="modal-title">Warning</h3></center>
                                </div>
                                <div class="modal-body">
                                    <center><b>Would you like to delete all canceled reports?</b><br><br>
                                        <form action="UpdateServlet" method="post">
                                            <button type="submit" class="btn btn-default" value="Delete BS Report" name="updateinfo">Yes</button> 
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                        </form>
                                    </center>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <ul class="pagination pagination-lg pager" id="myPager"></ul>
                    </div>
                </div>
            </div>
            <jsp:include page="/include/footer.jsp"/>
    </body>
</html>