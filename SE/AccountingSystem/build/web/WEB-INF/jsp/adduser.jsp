<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?updateuser=Update'>Need the list of users? Click here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Add User</title>
        <jsp:include page="/include/head.jsp"/>
        <script type="text/javascript">
            function Validate() {
                var password = document.getElementById("password").value;
                var confirm_password = document.getElementById("confirm_password").value;
                
                var firstname_val = document.getElementById("firstname").value;
                var lastname_val = document.getElementById("lastname").value;
                
                if (password !== confirm_password) {
                    swal("Error!", "Passwords do not match.", "error");
                    return false;
                }
                
                if (firstname_val.contains("!")||firstname_val.contains("#")||firstname_val.contains("?")||firstname_val.contains("+")||firstname_val.contains("*")||firstname_val.contains("%")||firstname_val.contains("$")){
                    swal("Error!", "First Name contains symbol(s)!", "error");
                    return false;
                } else if (lastname_val.contains("!")||lastname_val.contains("#")||lastname_val.contains("?")||lastname_val.contains("+")||lastname_val.contains("*")||lastname_val.contains("%")||lastname_val.contains("$")){
                    swal("Error!", "Last Name contains symbol(s)!", "error");
                    return false;
                }
                
                return true;
                
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Add a User</h3>
            <form  action="AddServlet" method="post">
                Employee ID: <br>
                <input type="text"  name="employeeid" pattern="[0-9]{6}" placeholder="Enter Six Digit Employee ID" class="form-control" autofocus="" required>
                <br>
                Full Name: 
                <input type="text" id='firstname' name="firstname" placeholder="Enter First Name" class="form-control" autofocus="" required>
                <input type="text" id='lastname' name="lastname" placeholder="Enter Last Name" class="form-control" autofocus="" required><br>
                <br>
                Password:
                <input type="password" id='password' name="password" placeholder="Enter Alphanumeric Password (at least one numerical character)" class="form-control" autofocus="" required>
                <input type="password" id='confirm_password' name="confirm_password" placeholder="Enter Password Again" class="form-control" autofocus="" required>
                <br>
                Permission:
                <select name = "permission" class="form-control" autofocus="" required>
                    <option selected disabled>Choose here</option>
                    <option value="admin">Administrator</option>
                    <option value="accountant">Accountant</option>
                    <option value="employee">Employee / Stakeholder</option>
                </select> 
                <br>
                <br>
                <div align="right">
                    <input type="reset" class="btn btn-default">
                    <input type="submit" class="btn btn-success" onclick="return Validate()" value="Add" name="adduser">
                </div>    
            </form>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
