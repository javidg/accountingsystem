<%-- 
    Document   : displaybalancesheet
    Created on : 09 10, 17, 12:02:28 PM
    Author     : user
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="returnlink" value= "" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Generate Balance Sheet</title>
        <jsp:include page="/include/head.jsp"/>
        <script type="text/javascript">
            function Download(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
        <script>
            var myVar;
            function myFunction() {
                myVar = setTimeout(showPage, 2000);
            }
            function showPage() {
                document.getElementById("loader").style.display = "none";
                document.getElementById("myDiv").style.display = "block";
            }
        </script>
        <style>
            <jsp:include page="/css/loadingbutton.css"/>
        </style>
    </head>
    <body onload="myFunction()" style="margin:0;">
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div id="loader"></div>
        <div style="display:none;" id="myDiv" class="animate-bottom">
            <div class="row" >
                <div class="col-sm-10" align="right">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Save</button>
                    <button algin="right" class="btn btn-success" type="button" onclick="Download('page')" />Download/Print</button>
                </div>
            </div>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Save Balance Sheet Report</h4>
                        </div>
                        <form action="AddServlet" method="post">    
                            <div class="modal-body">    
                                <p>Date is from ${startdate} to ${enddate}</p>
                                <p>Name of Report</p>
                                <input type="hidden" name="startdate" value="${startdate}">
                                <input type="hidden" name="enddate" value="${enddate}">
                                <input type="text" name="reportname" placeholder="Enter Report Title" class="form-control" autofocus="" required>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" name="savebalancesheet" value="Add" >
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>            
            </div>   
            <c:set var="totalassets" value="${0}"/>
            <c:set var="totalliabilities" value="${0}"/>
            <c:set var="totalownerequity" value="${0}"/>
            <c:set var="totalliabilitiesownerequity" value="${0}"/>
            <c:set var="totalcurrentassets" value="${0}"/>
            <c:set var="totalnoncurrentassets" value="${0}"/>
            <c:set var="totalcurrentliabilities" value="${0}"/>
            <c:set var="totalnoncurrentliabilities" value="${0}"/>
            <c:set var="totalcapitalstock" value="${0}"/>
            <c:set var="totalretainedearnings" value="${0}"/> 
            <div style="text-align:center" id="page" class="container">
                <div class="container-fluid">
                    <div><center><h1> ${companyname} </h1>
                        <h3> Balance Sheet</h3>
                        <h3> As of ${startdate} to ${enddate}</h3>
                    </center></div>
                </div>
                <div class="container-fluid">
                    <div class="col-sm-6">
                        <h3>Assets</h3>
                        <hr></hr>
                        <div style="text-align:left" class="col-sm-6">
                            <c:forEach var="row" items="${assets}"><c:set var="totalcurrentassets" value="${totalcurrentassets+row.debitamount}"/>    
                                ${row.debitdetail} ${row.debitamount}<br>
                            </c:forEach>
                            <c:forEach var="row" items="${noncurrentassets}"><c:set var="totalnoncurrentassets" value="${totalnoncurrentassets+row.debitamount}"/>     
                                ${row.debitdetail} ${row.debitamount}<br>
                            </c:forEach>
                        </div>
                        <div style="text-align:right" class="col-sm-6">
                            <c:set var="totalassets" value="${totalcurrentassets+totalnoncurrentassets}"/>
                            <b>Total: <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalassets}"/></b>                   
                        </div>
                    </div>    
                    <div class="col-sm-6">
                        <h3>Liabilities</h3>
                        <hr></hr>
                        <div class="col-sm-6" style="text-align:left">
                            <c:forEach var="row" items="${currentliabilities}"><c:set var="totalcurrentliabilities" value="${totalcurrentliabilities+row.debitamount}"/>    
                                ${row.debitdetail} ${row.debitamount}<br>
                            </c:forEach>
                            <c:forEach var="row" items="${noncurrentliabilities}"><c:set var="totalnoncurrentliabilities" value="${totalnoncurrentliabilities+row.debitamount}"/>     
                                ${row.debitdetail} ${row.debitamount}<br>
                            </c:forEach>
                        </div>
                        <div style="text-align:right" class="col-sm-6">
                            <c:set var="totalliabilities" value="${totalcurrentliabilities+totalnoncurrentliabilities}"/>
                            <b>Total: <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalliabilities}"/></b> 
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <h3>Owner's Equity </h3>
                            <hr></hr>
                            <div class="col-sm-6" style="text-align:left">
                                <c:forEach var="row" items="${capitalstock}"><c:set var="totalcapitalstock" value="${totalcapitalstock+row.debitamount}"/>    
                                    ${row.debitdetail} ${row.debitamount}<br>
                                </c:forEach>
                                <c:forEach var="row" items="${retainedearnings}"><c:set var="totalretainedearnings" value="${totalretainedearnings+row.debitamount}"/>     
                                    ${row.debitdetail} ${row.debitamount}<br>
                                </c:forEach>
                            </div>
                            <div style="text-align:right" class="col-sm-6">
                                <c:set var="totalownerequity" value="${totalcapitalstock+totalretainedearnings}"/>
                                <b>Total: <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalownerequity}"/></b>
                            </div>
                        </div>
                    </div>
                    <hr></hr>
                    <div class="container-fluid">
                        <div class="col-sm-6">
                            <c:if test="${totalassets < 0}">
                            <h4 style="color: red;">Total Assets: - <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalassets}"/> </h4>
                            </c:if>
                            <c:if test="${totalassets >= 0}">
                            <h4 style="color: green;">Total Assets: <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalassets}"/></h4>
                            </c:if>
                        </div>
                        <div class="col-sm-6">
                            <c:set var="totalliabilitiesownerequity" value="${totalliabilities+totalownerequity}"/>
                            <c:if test="${totalliabilitiesownerequity < 0}">
                                <strong><h4 style="color: red;">Total Liabilities and Owner's Equity: - <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalliabilitiesownerequity}"/></h4></strong>
                            </c:if>
                            <c:if test="${totalliabilitiesownerequity >= 0}">
                                <strong><h4 style="color: green;">Total Liabilities and Owner's Equity: <fmt:formatNumber type = "currency" currencyCode="PHP" value = "${totalliabilitiesownerequity}"/></h4></strong>
                            </c:if>
                        </div>
                        <div class="col-sm-12">
                        <c:if test="${totalassets == totalliabilitiesownerequity}">
        -                    <div class="alert alert-success">
        -                        <strong>Balanced Report</strong>
        -                    </div>
        -               </c:if>
                        <c:if test="${totalassets != totalliabilitiesownerequity}">
        -                    <div class="alert alert-warning">
        -                        <strong>Unbalanced Report</strong>
        -                    </div>
                        </c:if>
                        </div>
                    </div>  
                </div>
            </div>            
                <jsp:include page="/include/footer.jsp"/>
            </div>
    </body>
</html>
