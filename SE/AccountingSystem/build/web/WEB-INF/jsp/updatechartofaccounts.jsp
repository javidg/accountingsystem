<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?addchartofaccounts=Add'>Chart of Accounts not in the list? Add here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update Chart of Accounts</title>
        <jsp:include page="/include/head.jsp"/>
        <style>.table-responsive {height:180px;}</style>
        <script><jsp:include page="/js/pagination.js"/></script>
        <script>
            function edit_row(no) {
                document.getElementById("edit_button" + no).style.display = "none";
                document.getElementById("save_button" + no).style.display = "block";
                var coaid = document.getElementById("coaid_row" + no);
                var coaname = document.getElementById("coaname_row" + no);
                var debitcredit = document.getElementById("debitcredit_row" + no);
                var fs = document.getElementById("fs_row" + no);
                var itrfs = document.getElementById("itrfs_row" + no);
                var incomeexpense = document.getElementById("incomeexpense_row" + no);
                var classification = document.getElementById("classification_row" + no);
                var definition = document.getElementById("definition_row" + no);
                var coaid_data = coaid.innerHTML;
                var coaname_data = coaname.innerHTML;
                var debitcredit_data = debitcredit.innerHTML;
                var fs_data = fs.innerHTML;
                var itrfs_data = itrfs.innerHTML;
                var incomeexpense_data = incomeexpense.innerHTML;
                var classification_data = classification.innerHTML;
                var definition_data = definition.innerHTML;
                coaid.innerHTML = "<input type='hidden' name='coaid' id='coaid_text" + no + "' value='" + coaid_data + "'>";
                coaname.innerHTML = "<input type='text' class='form-control' name='coaname' id='coaname_text" + no + "' value='" + coaname_data + "'>";
                debitcredit.innerHTML = "<select class='form-control' name='debitcredit' id='debitcredit_text" + no + "'>\n\
                                            <option value='" + debitcredit_data + "' selected>" + debitcredit_data + "</option>\n\
                                            <option value='debit'>debit</option>\n\
                                            <option value='credit'>credit</option>\n\
                                        <select>";
                fs.innerHTML = "<select class='form-control' name='fs' id='fs_text" + no + "'>\n\
                                    <option value='" + fs_data + "' selected>" + fs_data + "</option>\n\
                                    <option value='bs'>Balance Sheet</option>\n\
                                    <option value='is'>Income Statement</option>\n\
                                <select>";
                itrfs.innerHTML = "<select class='form-control' name='itrfs' id='itrfs_text" + no + "'>\n\
                                    <option value='" + itrfs_data + "' selected>" + itrfs_data + "</option>\n\
                                    <option value='Cash on hand and in banks'>Cash on hand and in banks</option>\n\
                                    <option value='Trade and other receivables'>Trade and other receivables</option>\n\
                                    <option value='Advances and deposits'>Advances and deposits</option>\n\
                                    <option value='Other current assets'>Other current assets</option>\n\
                                    <option value='Property and equipment - net'>Property and equipment - net</option>\n\
                                    <option value='Deferred charges - MCIT'>Deferred charges - MCIT</option>\n\
                                    <option value='Deferred tax asset'>Deferred tax asset</option>\n\
                                    <option value='Trade and other payables'>Trade and other payables</option>\n\
                                    <option value='Other current liabilities'>Other current liabilities</option>\n\
                                    <option value='Advances from related parties'>Advances from related parties</option>\n\
                                    <option value='Loans payable'>Loans payable</option>\n\
                                    <option value='Capital stock'>Capital stock</option>\n\
                                    <option value='Retained earnings'>Retained earnings</option>\n\
                                    <option value='Service fees'>Service fees</option>\n\
                                    <option value='Other income'>Other income</option>\n\
                                    <option value='Interest income'>Interest income</option>\n\
                                    <option value='Salaries and allowances'>Salaries and allowances</option>\n\
                                    <option value='SSS, EC, Philhealth and Pagibig'>SSS, EC, Philhealth and Pagibig</option>\n\
                                    <option value='Professional fees'>Professional fees</option>\n\
                                    <option value='Commissions'>Commissions</option>\n\
                                    <option value='Other cost of services'>Other cost of services</option>\n\
                                    <option value='Advertising and promotion'>Advertising and promotion</option>\n\
                                    <option value='Representation and entertainment'>Representation and entertainment</option>\n\
                                    <option value='Research and development'>Research and development</option>\n\
                                    <option value='Rental'>Rental</option>\n\
                                    <option value='Repairs and maintenance'>Repairs and maintenance</option>\n\
                                    <option value='Transportation and travel'>Transportation and travel</option>\n\
                                    <option value='Communication, light and water'>Communication, light and water</option>\n\
                                    <option value='Office supplies'>Office supplies</option>\n\
                                    <option value='Taxes and licenses'>Taxes and licenses</option>\n\
                                    <option value='Fuel and oil'>Fuel and oil</option>\n\
                                    <option value='Dues and subscriptions'>Dues and subscriptions</option>\n\
                                    <option value='Insurances'>Insurances</option>\n\
                                    <option value='Depreciation'>Depreciation</option>\n\
                                    <option value='Other contractual services'>Other contractual services</option>\n\
                                    <option value='Trainings and seminars'>Trainings and seminars</option>\n\
                                    <option value='Company sponsored program'>Company sponsored program</option>\n\
                                    <option value='Miscellaneous'>Miscellaneous</option>\n\
                                    <option value='Provision for income tax'>Provision for income tax</option>\n\
                                <select>";
                incomeexpense.innerHTML = "<select class='form-control' name='incomeexpense' id='incomeexpense_text" + no + "'>\n\
                                                <option value='" + incomeexpense_data + "' selected>" + incomeexpense_data + "</option>\n\
                                                <option value='income'>income</option>\n\
                                                <option value='expense'>expense</option>\n\
                                                </select>";
                classification.innerHTML = "<select class='form-control' name='classification' id='classification_text" + no + "'>\n\
                                                <option value='" + classification_data + "' selected>" + classification_data + "</option>\n\
                                                <option value='currentsssets'>Current Assets</option>\n\
                                                <option value='noncurrentsssets'>Noncurrent Assets</option>\n\
                                                <option value='currentliabilities'>Current Liabilities</option>\n\
                                                <option value='noncurrentliabilities'>Noncurrent Liabilities</option>\n\
                                                <option value='capitalstock'>Capital Stock</option>\n\
                                                <option value='retainedearnings'>Retained Earnings</option>\n\
                                                <option value='servicefees'>Service Fees</option>\n\
                                                <option value='costofservices'>Cost Of Services</option>\n\
                                                <option value='opex'>OPEX</option>\n\
                                                <option value='provisionforincome'>Provision for Income</option>\n\
                                                <option value='ownersequity'>Owner's Equity</option></select>";
                definition.innerHTML = "<input type='text' class='form-control' name='definition' id='definition_text" + no + "' value='" + definition_data + "'>";
            }
            function save_row(no) {
                var coaid_val = document.getElementById("coaid_text" + no).value;
                var coaname_val = document.getElementById("coaname_text" + no).value;
                var debitcredit_val = document.getElementById("debitcredit_text" + no).value;
                var incomeexpense_val = document.getElementById("incomeexpense_text" + no).value;
                var classification_val = document.getElementById("classification_text" + no).value;
                var fs_val = document.getElementById("fs_text" + no).value;
                var itrfs_val = document.getElementById("itrfs_text" + no).value;
                var definition_val = document.getElementById("definition_text" + no).value;
                var contextpath = "${contextpath}";
                document.getElementById("coaid_row" + no).innerHTML = coaid_val;
                document.getElementById("coaname_row" + no).innerHTML = coaname_val;
                document.getElementById("debitcredit_row" + no).innerHTML = debitcredit_val;
                document.getElementById("incomeexpense_row" + no).innerHTML = incomeexpense_val;
                document.getElementById("classification_row" + no).innerHTML = classification_val;
                document.getElementById("fs_row" + no).innerHTML = fs_val;
                document.getElementById("itrfs_row" + no).innerHTML = itrfs_val;
                document.getElementById("definition_row" + no).innerHTML = definition_val;
                document.getElementById("edit_button" + no).style.display = "block";
                document.getElementById("save_button" + no).style.display = "none";
                if (coaname_val === "") {
                    swal("Error!", "Chart Of Account is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (definition_val === ""){
                    swal("Error!", "Chart Of Account is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else {
                    $.ajax
                            ({
                                type: 'post',
                                url: contextpath + "/GetServlet",
                                data: {
                                    page: "coa",
                                    coaid: coaid_val,
                                    coaname: coaname_val,
                                    debitcredit: debitcredit_val,
                                    incomeexpense: incomeexpense_val,
                                    classification: classification_val,
                                    fs: fs_val,
                                    itrfs: itrfs_val,
                                    definition: definition_val,
                                },
                                success: function (response) {
                                    if(response.indexOf("Error") !== -1){
                                        swal("Error!", response, "error");
                                        setTimeout(function() {
                                            location.reload();
                                        }, 3500);
                                    }else{
                                        swal("Updated!", response, "success");
                                    }
                                },
                                error: function (response) {
                                    swal("Error!", response, "error");
                                }
                            });
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Update Chart of Accounts</h3>
            <jsp:include page="/include/search.jsp"/>
            <br><br>
            <table id="myTable" class="table table-hover">  
                <thead>
                    <tr>
                        <th> Chart of Accounts Name </th>
                        <th> Debit / Credit </th>
                        <th> FS </th>
                        <th> ITR/FS </th>
                        <th> Income / Expense </th>
                        <th> Classification</th>
                        <th> Definition</th>
                        <th> Edit </th>
                    </tr>
                </thead>  
                <c:forEach var="item" items="${coa}">
                    <tbody>
                        <tr>
                            <td id="coaid_row${item.coaid}" style="display: none;">${item.coaid}</td>
                            <td id="coaname_row${item.coaid}">${item.coaname}</td>
                            <td id="debitcredit_row${item.coaid}">${item.debitcredit}</td>
                            <td id="fs_row${item.coaid}">${item.fs}</td>
                            <td id="itrfs_row${item.coaid}">${item.itrfs}</td>
                            <td id="incomeexpense_row${item.coaid}">${item.incomeexpense}</td>
                            <td id="classification_row${item.coaid}">${item.classification}</td>
                            <td id="definition_row${item.coaid}">${item.definition}</td>
                            <td>   
                                <input type="button" id="edit_button${item.coaid}" value="Edit" class="btn btn-default" onclick="edit_row(${item.coaid})">
                                <input type="button" style="display: none;" id="save_button${item.coaid}" value="Update" class="btn btn-success" onclick="save_row(${item.coaid})">
                            </td>   
                        </tr>
                    </tbody>
                </c:forEach>
            </table>   
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg pager" id="myPager"></ul>
            </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
