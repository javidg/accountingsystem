<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ul class="nav navbar-nav">
    <c:if test="${permission.equals('admin') || permission.equals('accountant')}">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-list-alt"></span>  Transaction
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="${contextpath}/AdminServlet?addtransaction=Add" data-toggle="tooltip" data-placement="auto" title="Add new transactions in the database"><span class="glyphicon glyphicon-plus"></span> Add New Transaction </a></li>
                    <c:if test="${permission.equals('admin')}">
                    <li><a href="${contextpath}/AdminServlet?verifytransaction=Verify" data-toggle="tooltip" data-placement="auto" title="Verify new transactions in the database"><span class="glyphicon glyphicon-search"></span> Verify Pending Transactions</a></li>
                    </c:if>
                <li><a href="${contextpath}/AdminServlet?updatetransaction=Update" data-toggle="tooltip" data-placement="auto" title="View and update existing transactions in the database"><span class="glyphicon glyphicon-pencil"></span> Update Verified Transactions</a></li>
            </ul>
        </li>
    </c:if> 
    <c:if test="${permission.equals('admin') || permission.equals('accountant') ||permission.equals('employee')}">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-file"></span>  Income Statement 
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <c:if test="${(permission.equals('admin'))}">
                    <li><a href="${contextpath}/ReportsServlet?addincomestatement=Add" data-toggle="tooltip" data-placement="auto" title="Create and verify reports on the company's net profit on a certain period of time"><span class="glyphicon glyphicon-circle-arrow-right"></span> Add and Verify Income Statement Reports</a></li>
                    </c:if>
                    <c:if test="${(permission.equals('accountant'))}">
                    <li><a href="${contextpath}/ReportsServlet?addincomestatement=Add" data-toggle="tooltip" data-placement="auto" title="Create a report on the company's net profit on a certain period of time"><span class="glyphicon glyphicon-circle-arrow-right"></span> Add Income Statement Reports</a></li>
                    </c:if>     
                <li><a href="${contextpath}/ReportsServlet?updateincomestatement=Update" data-toggle="tooltip" data-placement="auto" title="Update reports on the company's net profit on a certain period of time"><span class="glyphicon glyphicon-circle-arrow-right"></span> View Verified Income Statement Reports</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-file"></span>  Balance Sheet
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <c:if test="${(permission.equals('admin'))}">
                    <li><a href="${contextpath}/ReportsServlet?balancesheet=x" data-toggle="tooltip" data-placement="auto" title="Create and verify reports on the company's assets, liabilities and shareholders equity at a specific point in time"><span class="glyphicon glyphicon-circle-arrow-right"></span> Add and Verify Pending Balance Sheet Reports </a></li>
                    </c:if>
                    <c:if test="${(permission.equals('accountant'))}">
                    <li><a href="${contextpath}/ReportsServlet?balancesheet=x" data-toggle="tooltip" data-placement="auto" title="Create a report on the company's assets, liabilities and shareholders equity at a specific point in time"><span class="glyphicon glyphicon-circle-arrow-right"></span> Add New Balance Sheet Report </a></li>
                    </c:if>
                <li><a href="${contextpath}/ReportsServlet?updatebalancesheet=x" data-toggle="tooltip" data-placement="auto" title="View verified reports on the company's assets, liabilities and shareholders equity on a certain period of time"><span class="glyphicon glyphicon-circle-arrow-right"></span> View Verified Balance Sheet Reports </a></li>
            </ul>
        </li>
    </c:if>
    <li class="divider"></li> 
        <c:if test="${permission.equals('admin')}">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-user"></span>  User
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="${contextpath}/AdminServlet?adduser=Add" data-toggle="tooltip" data-placement="auto" title="Add new users in the database to access the system"><span class="glyphicon glyphicon-plus"></span> Add New User  Profile </a></li>
                <li><a href="${contextpath}/AdminServlet?updateuser=Update" data-toggle="tooltip" data-placement="auto" title="View and modify existing users in the database"><span class="glyphicon glyphicon-pencil"></span> View and Update Existing User Profiles </a></li>
            </ul>
        </li>
        <li class="divider"></li> 
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-piggy-bank"></span> Bank
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="${contextpath}/AdminServlet?addbank=Add" data-toggle="tooltip" data-placement="auto" title="Add new bank in the database"><span class="glyphicon glyphicon-plus"></span> Add New Bank </a></li>
                <li><a href="${contextpath}/AdminServlet?updatebank=Update" data-toggle="tooltip" data-placement="auto" title="View and modify existing bank in the database"><span class="glyphicon glyphicon-pencil"></span> View and Update Existing Bank </a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-tasks"></span> Chart of Accounts
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="${contextpath}/AdminServlet?addchartofaccounts=Add" data-toggle="tooltip" data-placement="auto" title="Add new chart of accounts in the database"><span class="glyphicon glyphicon-plus"></span> Add New Chart of Accounts </a></li>
                <li><a href="${contextpath}/AdminServlet?updatechartofaccounts=Update" data-toggle="tooltip" data-placement="auto" title="View and modify existing chart of accounts in the database" ><span class="glyphicon glyphicon-pencil"></span> View and Update Existing Chart of Accounts </a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-credit-card"></span> Type Of Payment
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="${contextpath}/AdminServlet?addtypeofpayment=Add" data-toggle="tooltip" data-placement="auto" title="Add new type of payment in the database"><span class="glyphicon glyphicon-plus"></span> Add New Type of Payment </a></li>
                <li><a href="${contextpath}/AdminServlet?updatetypeofpayment=Update" data-toggle="tooltip" data-placement="auto" title="View and modify existing tye of payment in the database"><span class="glyphicon glyphicon-pencil"></span> View and Update Existing Type of Payment </a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="glyphicon glyphicon-briefcase"></span>  Company Details
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="${contextpath}/AdminServlet?updatecompany=Update" data-toggle="tooltip" data-placement="auto" title="View and modify company's details "><span class="glyphicon glyphicon-pencil"></span> View and Update Company Details </a></li>
            </ul>
        </li>
    </c:if>
</ul>
