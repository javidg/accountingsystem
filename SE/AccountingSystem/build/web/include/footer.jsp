<%@page import="java.time.LocalDateTime"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />
<%-- Source: https://stackoverflow.com/questions/4949554/getting-current-date-in-jstl-el-and-doing-arithmetic-on-it --%>
<style>
    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        border: none;
        outline: none;
        background-color: #13a02a;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 10px;
    }
    #myBtn:hover {
        background-color: #13a02a
    }
</style>
<%-- Source: https://www.w3schools.com/howto/howto_js_scroll_to_top.asp --%>
<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
<script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
        scrollFunction()
    };
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }
    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>
<footer>
    <div align = "center">  
        ${returnlink}
        <form action="AdminServlet" method="post">
            <input type="submit" value="Return to Home Page" data-toggle="tooltip" data-placement="auto" title="Navigate back to the home page" name="xxx" class="btn btn-link">
        </form>
        <h6>
            <em> Copyright ${year} | All rights reserved <br>
                ${companyname} | ${companyaddress} | ${companytelno}
            </em>
        </h6>
    </div>   
</footer>
