<script>
            $(document).ready(function () {
                $('.search').on('keyup', function () {
                    var searchTerm = $(this).val().toLowerCase();
                    $('#myTable tbody tr').each(function () {
                        var lineStr = $(this).text().toLowerCase();
                        if (lineStr.indexOf(searchTerm) === -1) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    });
                });
            });
        </script>
<style>
            
            input[type=text] {
                width: 130px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            /* When the input field gets focus, change its width to 100% */
            input[type=text]:focus {
                width: 100%;
            }
        </style>
        <input type="text" class="search form-control pull-right" placeholder="Search" />
