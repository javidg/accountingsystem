<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="contextpath" value= "${pageContext.servletConfig.servletContext.contextPath}" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title> ${companyname} Accounting Information Systems</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="assets/bootstrap.min.css">
        <link rel="stylesheet" href="css/form-elements.css">
        <link rel="stylesheet" href="css/style.css">
        <style media="screen" type="text/css">
        <%@include file="css/fonts.css" %>
        </style>
    </head>
    <body> 
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text" >
                            <h2 class="login"><strong> ${companyname}
                                Accounting Information Systems </strong></h2>
                            <div class="description">
                                <p> Collect. Store. Process.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Login to the system</h3>
                                    <p>Enter your Employee ID Number and Password:</p>
                                    <c:if test="${error != null}">
                                        ${error}
                                    </c:if>
                                </div>    
                                <div class="form-top-right">
                                    <i class="fa fa-lock"></i>
                                </div>
                            </div>
                            <div class="form-bottom">
                                <form action="LoginServlet" method="post" class="login-form">
                                    <div class="form-group">
                                        <label class="sr-only" for="idnum">ID Number</label>
                                        <input type="text" name="idnum" pattern="[0-9]{6}" autofocus="" placeholder="Enter ID Number here" required class="form-username form-control" id="form-username">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="password">Password</label>
                                        <input type="password" name="password" autofocus="" required placeholder="Enter your password here" class="form-password form-control" id="form-password" >
                                    </div>
                                    <button type="submit" name="Login" value="Login" class="btn btn-success">Login!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.backstretch.min.js"></script>
        <script src="js/scripts.js"></script>
</html>
