$(document).ready(function () {
    calculateGrandTotalCredit();
    calculateGrandTotalDebit();
   
    var latestamount = $('#amount1').val();
    $("#value1").val(latestamount);
    $("#amount1").keyup(function(){
        var latestamount = $('#amount1').val();
        $('#value1').val(latestamount);
    });
    
    var latestamount = $('#amount2').val();
    $("#value2").val(latestamount);
    $("#amount2").keyup(function(){
        var latestamount = $('#amount2').val();
        $('#value2').val(latestamount);
    });
    
    var latestamount = $('#amount3').val();
    $("#value3").val(latestamount);
    $("#amount3").keyup(function(){
        var latestamount = $('#amount3').val();
        $('#value3').val(latestamount);
    });
    
    var latestamount = $('#amount4').val();
    $("#value4").val(latestamount);
    $("#amount4").keyup(function(){
        var latestamount = $('#amount4').val();
        $('#value4').val(latestamount);
    });
    
    var latestamount = $('#amount5').val();
    $("#value5").val(latestamount);
    $("#amount5").keyup(function(){
        var latestamount = $('#amount5').val();
        $('#value5').val(latestamount);
    });
    
    var latestamount = $('#amount6').val();
    $("#value6").val(latestamount);
    $("#amount6").keyup(function(){
        var latestamount = $('#amount6').val();
        $('#value6').val(latestamount);
    });
    
    var latestamount = $('#amount7').val();
    $("#value7").val(latestamount);
    $("#amount7").keyup(function(){
        var latestamount = $('#amount7').val();
        $('#value7').val(latestamount);
    });
    
    var latestamount = $('#amount8').val();
    $("#value8").val(latestamount);
    $("#amount8").keyup(function(){
        var latestamount = $('#amount8').val();
        $('#value8').val(latestamount);
    });
    
    var latestamount = $('#amount9').val();
    $("#value9").val(latestamount);
    $("#amount9").keyup(function(){
        var latestamount = $('#amount9').val();
        $('#value9').val(latestamount);
    });
    
    $("#grandtotal").on("change", function () {
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();   
    });
    
    var timer = null;
    $('input[id^="debitamount"]').keyup(function(){
           clearTimeout(timer); 
           timer = setTimeout(calculateGrandTotalDebit(), 1000);
    });
    
    var timer2 = null;
    $('input[id^="creditamount"]').keyup(function(){
           clearTimeout(timer2); 
           timer2 = setTimeout(calculateGrandTotalCredit(), 1000);
    });
});
$(document).ready(function () {
    calculateGrandTotalCredit();
    calculateGrandTotalDebit();
    var counter = 0;
    
    $("#grandtotal").on("change", function () {
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
       
    });
    var timer = null;
    $('input[id^="debitamount"]').keyup(function(){
           clearTimeout(timer); 
           timer = setTimeout(calculateGrandTotalDebit(), 1000);
    });
    
    var timer2 = null;
    $('input[id^="creditamount"]').keyup(function(){
           clearTimeout(timer2); 
           timer2 = setTimeout(calculateGrandTotalCredit(), 1000);
    });
});
/*
function getLastestValue(target, destination){
    var latestamount = document.getElementById(target).value;
    $(destination).val(latestamount);
}
    var latestamount = $('#debitamount1').val();
    $("#debitvalue1").val(latestamount);
    $("#debitamount1").keyup(function(){
        var latestamount = $('#debitamount1').val();
        $('#debitvalue1').val(latestamount);
    });
*/
function calculateGrandTotalDebit() {
    var grandTotalDebit = 0;
    
    $("table.order-list1").find('input[name^="debitamount"]').each(function () {
        grandTotalDebit += +$(this).val();
    });
    $("#grandtotaldebit").text(grandTotalDebit.toFixed(2));
    $("#nursupertag").val(grandTotalDebit.toFixed(2));
}
function calculateGrandTotalCredit() {
    var grandTotalCredit = 0;
    $("table.order-list2").find('input[name^="creditamount"]').each(function () {
        grandTotalCredit += +$(this).val();
    });
    $("#grandtotalcredit").text(grandTotalCredit.toFixed(2));
    $("#nursupertag2").val(grandTotalCredit.toFixed(2));
}
