$(document).ready(function () {
    calculateGrandTotalCredit();
    calculateGrandTotalDebit();
    var counter = 0;
    
    $("#addrow1").on("click", function () {
       
        var newRow = $("<tr>");
        var cols = "";
        
        cols += '<td ><select name="debitdescription_'+ counter +'" id="debitdescription_'+ counter + '" class="form-control" autofocus=""></select></td>';
        cols += '<td class="text-right"><input name="debitamount_'+ counter +'" placeholder="Enter the Amount" min="0" id="debitamount_'+counter+'" onkeyup="calculateGrandTotalDebit()"class="form-control" type="number"' + counter + '"/></td>';
        cols += '<td><button class="ibtnDel btn btn-md btn-danger "><span class="glyphicon glyphicon-remove"></span></button></td>';
        newRow.append(cols);
        $("table.order-list1").append(newRow);
        if(counter === 0){
            $('#debitdescription option').clone().appendTo('#debitdescription_0');
        }
        
        if(counter === 1){
            $('#debitdescription option').clone().appendTo('#debitdescription_1');
        }
        
        if(counter === 2){
            $('#debitdescription option').clone().appendTo('#debitdescription_2');
        }
        
        if(counter === 3){
            $('#debitdescription option').clone().appendTo('#debitdescription_3');
        }
        
        if(counter === 4){
            $('#debitdescription option').clone().appendTo('#debitdescription_4');
        }
        
        if(counter === 5){
            $('#debitdescription option').clone().appendTo('#debitdescription_5');
        }
        
        if(counter === 6){
            $('#debitdescription option').clone().appendTo('#debitdescription_6');
        }
         
        if(counter === 7){
            $('#debitdescription option').clone().appendTo('#debitdescription_7');
        }
        
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
        counter++;
    });
    
    $("table.order-list1").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1;
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
    });
    
    $("#grandtotal").on("change", function () {
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
       
    });
    
    var timer = null;
    $('input[id^="debitamount"]').keyup(function(){
           clearTimeout(timer); 
           timer = setTimeout(calculateGrandTotalDebit(), 1000);
    });
    
    var timer2 = null;
    $('input[id^="creditamount"]').keyup(function(){
           clearTimeout(timer2); 
           timer2 = setTimeout(calculateGrandTotalCredit(), 1000);
    });
});
$(document).ready(function () {
    calculateGrandTotalCredit();
    calculateGrandTotalDebit();
    var counter = 0;
    
    $("#addrow2").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";
                                            
        cols += '<td><select class="form-control" autofocus="" name="creditdescription_'+ counter +'" id="creditdescription_' + counter +'"></select></td>';
        cols += '<td class="text-right"><input name="creditamount_'+ counter +'" min="0" id="creditamount_'+ counter +'" placeholder="Enter the Amount" onkeyup="calculateGrandTotalCredit()" class="form-control" type="number"' + counter + '"/></td>';
        cols += '<td><button class="ibtnDel btn btn-md btn-danger "><span class="glyphicon glyphicon-remove"></span></button></td>';
        newRow.append(cols);
        $("table.order-list2").append(newRow);
        $('#creditdescription option').clone().appendTo('#creditdescription_0');
        if(counter === 0){
            $('#creditdescription option').clone().appendTo('#creditdescription_0');
        }
        if(counter === 1){
            $('#creditdescription option').clone().appendTo('#creditdescription_1');
        }
        if(counter === 2){
            $('#creditdescription option').clone().appendTo('#creditdescription_2');
        }
        if(counter === 3){
            $('#creditdescription option').clone().appendTo('#creditdescription_3');
        }
        if(counter === 4){
            $('#creditdescription option').clone().appendTo('#creditdescription_4');
        }
        if(counter === 5){
            $('#creditdescription option').clone().appendTo('#creditdescription_5');
        }
     
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
        counter++;
    }); 
    
    $("table.order-list2").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1;
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
    });
    
     $("#grandtotal").on("change", function () {
        calculateGrandTotalCredit();
        calculateGrandTotalDebit();
       
    });
    var timer = null;
    $('input[id^="debitamount"]').keyup(function(){
           clearTimeout(timer); 
           timer = setTimeout(calculateGrandTotalDebit(), 1000);
    });
    
    var timer2 = null;
    $('input[id^="creditamount"]').keyup(function(){
           clearTimeout(timer2); 
           timer2 = setTimeout(calculateGrandTotalCredit(), 1000);
    });
});

function getLastestValue(target, destination){
    var latestamount = $(target).val();
    $(destination).val(latestamount);
}

function calculateGrandTotalDebit() {
    var grandTotalDebit = 0;
    
    $("table.order-list1").find('input[name^="debitamount"]').each(function () {
        grandTotalDebit += +$(this).val();
    });
    $("#grandtotaldebit").text(grandTotalDebit.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
    $("#nursupertag").val(grandTotalDebit.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
}
function calculateGrandTotalCredit() {
    var grandTotalCredit = 0;
    $("table.order-list2").find('input[name^="creditamount"]').each(function () {
        grandTotalCredit += +$(this).val();
    });
    $("#grandtotalcredit").text(grandTotalCredit.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
    $("#nursupertag2").val(grandTotalCredit.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
}
