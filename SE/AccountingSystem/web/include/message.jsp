<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="js/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<c:if test="${!empty Emessage}">
    <script>
        swal("Error!", "${Emessage}", "warning");
    </script>
</c:if>
<c:if test="${!empty Smessage}">
    <script>
        swal("Success!", "${Smessage}", "success");
    </script>
</c:if>
<c:set var="message" value= "" scope="request" />
