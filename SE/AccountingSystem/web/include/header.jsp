<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="css/bootstrap.min.css" rel="stylesheet">
<style>
    * {
        font-family: sans-serif;
        font-size: 14px;
    }
    .navbar-inverse {
        background-color:#13a02a;
        border-color: #E7E7E7;
    }
    .navbar-inverse .navbar-nav > li > a {
        color: whitesmoke;
    }
</style>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-left">
            <%-- Goes back  to home page --%>
            <li><a href="${contextpath}/AdminServlet?xxx=x" data-toggle="tooltip" data-placement="auto" title="Go back to home page" ><span class="glyphicon glyphicon-tree-deciduous"></span>  ${companyname}  </a></li>
                <jsp:include page="/include/navigation.jsp"/>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <%-- Log-out, Redirect to servlet to remove session attributes --%>
            <li><a href="${contextpath}/LogoutServlet"  data-toggle="tooltip" data-placement="auto" title="Logout from the system"><span class="glyphicon glyphicon-user"></span> ${userfname}'s account <span class="glyphicon glyphicon-log-out"></span> Logout </a></li>
        </ul>
    </div>
</nav>
