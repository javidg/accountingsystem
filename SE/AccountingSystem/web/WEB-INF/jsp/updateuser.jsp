<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?adduser=Add'>User not in the list? Add here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update User</title>
        <jsp:include page="/include/head.jsp"/>
        <style>.table-responsive {height:180px;}</style>
        <script><jsp:include page="/js/pagination.js"/></script>
        <script>
            function edit_row(no) {
                document.getElementById("edit_button" + no).style.display = "none";
                document.getElementById("pass1").style.display = "block";
                document.getElementById("pass2").style.display = "block";
                document.getElementById("save_button" + no).style.display = "block";
                document.getElementById("password1_row" + no).style.display = "block";
                document.getElementById("password2_row" + no).style.display = "block";
                var accountid = document.getElementById("accountid_row" + no);
                var employeeid = document.getElementById("employeeid_row" + no);
                var firstname = document.getElementById("firstname_row" + no);
                var lastname = document.getElementById("lastname_row" + no);
                var password1 = document.getElementById("password1_row" + no);
                var password2 = document.getElementById("password2_row" + no);
                var permission = document.getElementById("permission_row" + no);
                var status = document.getElementById("status_row" + no);
                var accountid_data = accountid.innerHTML;
                var employeeid_data = employeeid.innerHTML;
                var firstname_data = firstname.innerHTML;
                var lastname_data = lastname.innerHTML;
                var password1_data = password1.innerHTML;
                var password2_data = password2.innerHTML;
                var permission_data = permission.innerHTML;
                var status_data = status.innerHTML;
                accountid.innerHTML = "<input type='hidden' name='accountid' id='accountid_text" + no + "' value='" + accountid_data + "'>";
                employeeid.innerHTML = "<input type='text' pattern='[0-9]{6}' class='form-control' name='employeeid' id='employeeid_text" + no + "' value='" + employeeid_data + "'>";
                firstname.innerHTML = "<input type='text' class='form-control' name='firstname' id='firstname_text" + no + "' value='" + firstname_data + "'>";
                lastname.innerHTML = "<input type='text' class='form-control' name='lastname' id='lastname_text" + no + "' value='" + lastname_data + "'>";
                password1.innerHTML = "<input type='password' class='form-control' name='password1' id='password1_text" + no + "' value='" + password1_data + "'>";
                password2.innerHTML = "<input type='password' class='form-control' name='password2' id='password2_text" + no + "' value='" + password2_data + "'>";
                permission.innerHTML = "<select name='permission' class='form-control' id='permission_text" + no + "'><option value='" + permission_data + "' selected>" + permission_data + "</option><option value='admin'>admin</option><option value='accountant'>accountant</option><option value='employee'>employee</option></select>";
                status.innerHTML = "<select name='status' class='form-control' id='status_text" + no + "'><option value='" + status_data + "' selected>" + status_data + "</option><option value='active'>active</option><option value='inactive'>inactive</option></select>";
            }
            function save_row(no) {
                var accountid_val = document.getElementById("accountid_text" + no).value;
                var employeeid_val = document.getElementById("employeeid_text" + no).value;
                var firstname_val = document.getElementById("firstname_text" + no).value;
                var lastname_val = document.getElementById("lastname_text" + no).value;
                var password1_val = document.getElementById("password1_text" + no).value;
                var password2_val = document.getElementById("password2_text" + no).value;
                var permission_val = document.getElementById("permission_text" + no).value;
                var status_val = document.getElementById("status_text" + no).value;
                var contextpath = "${contextpath}";
                document.getElementById("accountid_row" + no).innerHTML = accountid_val;
                document.getElementById("employeeid_row" + no).innerHTML = employeeid_val;
                document.getElementById("firstname_row" + no).innerHTML = firstname_val;
                document.getElementById("lastname_row" + no).innerHTML = lastname_val;
                document.getElementById("password1_row" + no).innerHTML = password1_val;
                document.getElementById("password2_row" + no).innerHTML = password2_val;
                document.getElementById("permission_row" + no).innerHTML = permission_val;
                document.getElementById("status_row" + no).innerHTML = status_val;
                document.getElementById("edit_button" + no).style.display = "block";
                document.getElementById("save_button" + no).style.display = "none";
                document.getElementById("pass1").style.display = "none";
                document.getElementById("pass2").style.display = "none";
                document.getElementById("password1_row" + no).style.display = "none";
                document.getElementById("password2_row" + no).style.display = "none";
                if (employeeid_val === "") {
                    swal("Error!", "Employee ID is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (employeeid_val.length !== 6) {
                    swal("Error!", "Invalid Employee ID. Please Enter Six Digit Employee ID", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (firstname_val === "") {
                    swal("Error!", "First Name is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (firstname_val.contains("!")||firstname_val.contains("#")||firstname_val.contains("?")||firstname_val.contains("+")||firstname_val.contains("*")||firstname_val.contains("%")||firstname_val.contains("$")) {
                    swal("Error!", "First Name contains symbols!", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (lastname_val === "") {
                    swal("Error!", "Last Name is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (lastname_val.contains("!")||lastname_val.contains("#")||lastname_val.contains("?")||lastname_val.contains("+")||lastname_val.contains("*")||lastname_val.contains("%")||lastname_val.contains("$")) {
                    swal("Error!", "First Name contains symbols!", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (password1_val !== password2_val) {
                    swal("Error!", "Passwords do not match", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else {
                    $.ajax
                            ({
                                type: 'post',
                                url: contextpath + "/GetServlet",
                                data: {
                                    page: "account",
                                    accountid: accountid_val,
                                    employeeid: employeeid_val,
                                    firstname: firstname_val,
                                    lastname: lastname_val,
                                    password: password1_val,
                                    permission: permission_val,
                                    status: status_val,
                                },
                                success: function (response) {
                                    
                                    if(response.indexOf("Error") !== -1){
                                        swal("Error!", response, "error");
                                        setTimeout(function() {
                                            location.reload();
                                        }, 3500);
                                    }else {
                                        swal("Updated!", response, "success");
                                    }
                                },
                                error: function (response) {
                                    swal("Error!", response, "error");
                                }
                            });
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/> 
        <jsp:include page="/include/message.jsp"/>
        
        <div class="container">
            <h3>Update User</h3>
            <jsp:include page="/include/search.jsp"/>
            <br><br>
            <table id="myTable" class="table table-hover">  
                <thead>
                    <tr>
                        <th> Employee ID </th>
                        <th> First Name </th>
                        <th> Last Name </th>
                        <th style="display: none;" id="pass1"> Password </th>
                        <th style="display: none;" id="pass2"> Confirm Password </th>
                        <th> Permission </th>
                        <th> Active Status </th> 
                        <th> Edit </th>
                    </tr>
                </thead>  
                <c:forEach var="item" items="${account}">
                    <tbody>
                        <tr>
                            <c:if test="${item.accountid != 0}">
                            <td id="accountid_row${item.accountid}" style="display: none;">${item.accountid}</td>
                            <td id="employeeid_row${item.accountid}">${item.employeeid}</td>
                            <td id="firstname_row${item.accountid}">${item.firstname}</td>
                            <td id="lastname_row${item.accountid}">${item.lastname}</td>
                            <td id="password1_row${item.accountid}" style="display: none;"></td>
                            <td id="password2_row${item.accountid}" style="display: none;"></td>
                            <td id="permission_row${item.accountid}">${item.permission}</td>
                            <td id="status_row${item.accountid}">${item.status}</td>
                            <td>   
                                <input type="button" id="edit_button${item.accountid}" value="Edit" class="btn btn-default" onclick="edit_row(${item.accountid})">
                                <input type="button" style="display: none;" id="save_button${item.accountid}" value="Update" class="btn btn-success" onclick="save_row(${item.accountid})">
                            </td>   
                            </c:if>
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg pager" id="myPager"></ul>
            </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
