<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?updatechartofaccounts=Update'>Need the list of Chart of Accounts? Click here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title> ${companyname} AIS - Add Chart Of Accounts </title>
        <jsp:include page="/include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Add Chart Of Accounts</h3>
            <form action="AddServlet" method="post">
                Chart of Accounts Name:
                <input type="text" name="coa" placeholder="Enter Chart of Accounts Name" class="form-control" autofocus="" required><br>
                Debit Or Credit:
                <select name = "debitcredit" class="form-control" autofocus="" required>
                    <option selected disabled>Choose here</option>
                    <option value="debit">Debit</option>
                    <option value="credit">Credit</option>
                </select> 
                <br>
                Financial Statement:
                <select name = "fs" class="form-control" autofocus="" required>
                    <option selected disabled>Choose here</option>
                    <option value="bs">Balance Sheet</option>
                    <option value="is">Income Statement</option>
                </select> 
                <br>
                Income Tax Return / Financial Statement
                <select name = "itr/fs" class="form-control" autofocus="" required>
                    <option selected disabled>Choose here</option>
                    <option value="Cash on hand and in banks">Cash on hand and in banks</option>
                    <option value="Trade and other receivables">Trade and other receivables</option>
                    <option value="Advances and deposits">Advances and deposits</option>
                    <option value="Other current assets">Other current assets</option>
                    <option value="Property and equipment - net">Property and equipment - net</option>
                    <option value="Deferred charges - MCIT">Deferred charges - MCIT</option>
                    <option value="Deferred tax asset">Deferred tax asset</option>
                    <option value="Other current liabilities">Trade and other payables</option>
                    <option value="incomestatement">Other current liabilities</option>
                    <option value="Advances from related parties">Advances from related parties</option>
                    <option value="Loans payable">Loans payable</option>
                    <option value="Capital stock">Capital stock</option>
                    <option value="Retained earnings">Retained earnings</option>
                    <option value="Service fees">Service fees</option>
                    <option value="Other income">Other income</option>
                    <option value="Interest income">Interest income</option>
                    <option value="Salaries and allowances">Salaries and allowances</option>
                    <option value="SSS, EC, Philhealth and Pagibig">SSS, EC, Philhealth and Pagibig</option>
                    <option value="Professional fees">Professional fees</option>
                    <option value="Commissions">Commissions</option>
                    <option value="Other cost of services">Other cost of services</option>
                    <option value="Advertising and promotion">Advertising and promotion</option>
                    <option value="Representation and entertainment">Representation and entertainment</option>
                    <option value="Research and development">Research and development</option>
                    <option value="Rental">Rental</option>
                    <option value="Repairs and maintenance">Repairs and maintenance</option>
                    <option value="Transportation and travel">Transportation and travel</option>
                    <option value="Communication, light and water">Communication, light and water</option>
                    <option value="Office supplies">Office supplies</option>
                    <option value="Taxes and licenses">Taxes and licenses</option>
                    <option value="Fuel and oil">Fuel and oil</option>
                    <option value="Dues and subscriptions">Dues and subscriptions</option>
                    <option value="Insurances">Insurances</option>
                    <option value="Depreciation">Depreciation</option>
                    <option value="Other contractual services">Other contractual services</option>
                    <option value="Trainings and seminars">Trainings and seminars</option>
                    <option value="Company sponsored program">Company sponsored program</option>
                    <option value="Miscellaneous">Miscellaneous</option>
                    <option value="Provision for income tax">Provision for income tax</option>
                </select> 
                <br>
                
                Income Or Expense :
                <select name = "incomeexpense" class="form-control" autofocus="" required>
                    <option selected disabled>Choose here</option>
                    <option value="income">Income</option>
                    <option value="expense">Expense</option>
                </select> 
                <br>
                Classification :
                <select name = "classification" class="form-control" autofocus="" required>
                    <option selected disabled>Choose here</option>
                    <option value="currentsssets">Current Assets</option>
                    <option value="noncurrentsssets">Noncurrent Assets</option>
                    <option value="currentliabilities">Current Liabilities</option>
                    <option value="noncurrentliabilities">Noncurrent Liabilities</option>
                    <option value="capitalstock">Capital Stock</option>
                    <option value="retainedearnings">Retained Earnings</option>
                    <option value="servicefees">Service Fees</option>
                    <option value="costofservices">Cost Of Services</option>
                    <option value="opex">OPEX</option>
                    <option value="provisionforincome">Provision for Income</option>
                    <option value="ownersequity">Owner's Equity</option>
                </select> 
                <br>
                Definition : 
                <input type="text" name="definition" placeholder="Enter Definition" class="form-control" autofocus="" required>
                <br><br>
                
                <div align="right">
                    <input type="reset" class="btn btn-default">
                    <input type="submit" class="btn btn-success" name="addcoa" value="Add" >
                </div>
            </form>  
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
