<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "" scope="session" />
<c:set var="Emessage" value="" scope="session" />
<c:set var="Smessage" value="" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} Accounting System Home Page</title>
        <jsp:include page="/include/head.jsp"/>
        <style>
            <jsp:include page="/css/ui-template.css"/>
            .custom-scroll {
                overflow-y:scroll;
            }
        </style>    
        <script type="text/javascript">
            function Validate() {
                var password = document.getElementById("password").value;
                var confirm_password = document.getElementById("confirm_password").value;
                if (password !== confirm_password) {
                    swal("Error!", "Passwords do not match.", "error");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <c:set var="message" value= "" scope="session" />
        <jsp:include page="/include/header.jsp"/>
        <div class="col-md-2 sidebar"> 
            <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#myModal">
                <span class="glyphicon glyphicon-wrench"></span> 
            </button> 
            <div class="row">
                <div class="img-circle" align="center">
                    <input type="image" src="/AccountingSystem/profilepicture/${picturename}" alt="Submit" width="200" height="200" />
                </div>
                <br>
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Change Profile Picture</h4>
                            </div>
                            <div class="modal-body" align="center">
                                <input type="image" src="${contextpath}/profilepicture/${picturename}" alt="Submit" width="200" height="200"/>
                                <form action="UploadServlet" method="post" enctype="multipart/form-data">
                                    <input type="file" name="image" accept="image/*" required><br>
                                    <button type="submit" class="btn btn-default" value="Upload" name="upload">Upload</button>
                                </form> <br><br>
                                <form action="UpdateServlet" method="post">
                                    <input type="hidden" name="accountid" value="${accountid}">
                                    Employee ID: <br>
                                    <input type="text"  disabled value="${userid}" class="form-control" autofocus="" required>
                                    <input type="hidden"  name="employeeid" value="${userid}" class="form-control" autofocus="" required>
                                    <br>
                                    Full Name: 
                                    <input type="text" name="firstname" placeholder="Enter First Name" value="${userfname}" class="form-control" autofocus="" required>
                                    <input type="text" name="lastname" placeholder="Enter Last Name" value="${userlname}" class="form-control" autofocus="" required><br>
                                    <br>
                                    Password:
                                    <input type="password" id='password' name="password" placeholder="Enter Alphanumeric Password (at least one numerical character)" class="form-control" autofocus="" >
                                    <input type="password" id='confirm_password' name="confirm_password" placeholder="Enter Password Again" class="form-control" autofocus="" >
                                    <br>
                                    <button type="submit" class="btn btn-default" value="Update Profile" name="updateinfo">Update Profile</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${permission.equals('admin')}">
                    <div class="side-menu">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="side-menu-container">
                                <jsp:include page="/include/navigation.jsp"/>
                            </div>
                        </nav>
                    </div>
                </c:if>
            </div>  		
        </div>
        <c:if test="${permission.equals('admin')}">
            <div class="col-md-8 content">
            </c:if>
            <c:if test="${permission.equals('accountant') ||permission.equals('employee') }">
                <div class="col-md-10 content">
                </c:if>         
                <div class="panel panel-default">
                    <div class="panel-body" align="center">
                        <div style="font-size: 32px">${userfname} ${userlname} </div>
                        <h5> ${permission}</h5>
                    </div>
                </div>
                <!-- Dashboard -->
                <c:if test="${permission.equals('admin')}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">
                                    <h1><span class="glyphicon glyphicon-book"></span> ${transactionctr} </h1>
                                    <h4> <a href='${contextpath}/AdminServlet?updatetransaction=Update'>transactions</a></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">
                                    <h1><span class="glyphicon glyphicon-tasks"></span> ${reportctr} </h1>
                                    <h4> <a href="#"> generated reports </a> </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">
                                    <h1><span class="glyphicon glyphicon-user"></span> ${accountctr} </h1>
                                    <h4> <a href='${contextpath}/AdminServlet?updateuser=Update'> registered users </a> </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">
                                    <h1><span class="glyphicon glyphicon-piggy-bank"></span> ${bankctr} </h1>
                                    <h4> <a href='${contextpath}/AdminServlet?addbank=Add'> banks </a> </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">
                                    <h1><span class="glyphicon glyphicon-ok-circle"></span> ${coactr} </h1>
                                    <h4> <a href='${contextpath}/AdminServlet?updatechartofaccounts=Update'> chart of accounts </a> </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-default">
                                <div class="panel-heading" align="center">
                                    <h1><span class="glyphicon glyphicon-credit-card"></span> ${topctr} </h1>
                                    <h4> <a href='${contextpath}/AdminServlet?updatetypeofpayment=Update'> type of payments </a> </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 ">  <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            <h4> <span class="glyphicon glyphicon-bookmark"></span>  Notifications </h4>
                        </div>
                        <div class="panel-body custom-scroll" style="max-height: 700px;min-height: 600px;">
                            <c:forEach var="item" items="${transaction}">
                                <c:choose>
                                    <c:when test="${item.status == 'pending'}">
                                        <div class="alert alert-success alert-dismissable" >
                                            <p class="close" data-dismiss="alert" aria-label="close" data-toggle="tooltip" data-placement="auto" title="Dismiss notification">×</p>
                                            <strong>Pending Transaction</strong>
                                            <p>${item.date} ${item.transactiondetail}</p>
                                        </div>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:forEach var="item" items="${report}">
                                <c:choose>
                                    <c:when test="${item.status == 'pending'}">
                                        <c:if test="${item.reporttype == 'balancesheet'}">
                                        <div class="alert alert-success alert-dismissable" >
                                            <p class="close" data-dismiss="alert" aria-label="close">×</p>
                                            <strong>Pending Balance Sheet Report</strong>
                                            <p>${item.datecreated} ${item.reportname}</p>
                                        </div>
                                        </c:if>
                                        <c:if test="${item.reporttype == 'incomestatement'}">
                                        <div class="alert alert-success alert-dismissable" >
                                            <p class="close" data-dismiss="alert" aria-label="close">×</p>
                                            <strong>Pending Income Statement Report</strong>
                                            <p>${item.datecreated} ${item.reportname}</p>
                                        </div>
                                        </c:if>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:if>   
            <c:if test="${permission.equals('employee')}">
                <img src="/AccountingSystem/profilepicture/employee.jpg" width="1358" height="490" alt="employee help infographics"/>
            </c:if>      
            <c:if test="${permission.equals('accountant')}">
                <img src="/AccountingSystem/profilepicture/accountant.jpg" width="1361" height="472" alt="employee help infographics"/>
            </c:if>   
            <br>
        </div>
    </body>
</html>
