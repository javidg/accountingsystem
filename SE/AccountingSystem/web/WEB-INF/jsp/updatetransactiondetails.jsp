<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?addtransaction=Add'>Transaction not in the list? Add here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update Transaction</title>
        <jsp:include page="/include/head.jsp"/>
        <script>
            function Validate() {
                var debitamount = document.getElementById("nursupertag").value;
                var creditamount = document.getElementById("nursupertag2").value;
                
                if (debitamount !== creditamount) {
                    swal("Error!", "The amount for the debit and credit is not balance. ", "error");
                    return false;
                } else if (debitamount < 0 || creditamount < 0) {
                    swal("Error!", "The amount should be a positive integer.", "error");
                    return false;
                }
                return true;
            }
        </script>
        <script>
            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        </script>
        <script><jsp:include page="/js/update.js"/></script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container-fluid">
            <div class="col-sm-12"><center><h3>Update Transaction</h3></center></div>
        </div>
         <form action="UpdateServlet" method="post">
        <c:forEach var="item" items="${transaction}">     
            <div class="container-fluid">
                <div class="col-sm-12">
                    <center><h3>Date:</h3>
                        <!-- Working error handling for mistyped date !-->
                        <input type="Date" id="datepicker" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"  name="date" value="${item.date}" placeholder="${item.date}" required></center> 
                </div>   
            </div>
            <br>
            <div class="row">
                    <div class="col-sm-6">
                        <div class="container-fluid">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                               <center><h3 class="panel-title"><strong>Debit</strong></h3></center>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="myTable"  class=" table order-list1">
                                        <tbody>
                                            <c:forEach var="item2" items="${transactiondetail}" begin="0" varStatus="status">
                                            <tr> 
                                                <c:if test="${item2.debitcredit == 'debit'}">
                                                <td >
                                                    <input type="hidden" name="detailid${status.count}" value="${item2.id}">
                                                    <select name = "coa${status.count}" class="form-control" autofocus="">
                                                        <option value="${item2.coaid}">${item2.coaname}</option>
                                                        <c:forEach var="row" items="${coa}">    
                                                            <c:if test="${item2.debitcredit == 'debit'}">
                                                                <option value="${row.coaid}">${row.coaname}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </td>
                                                
                                                <td class="text-right">
                                                    <input type="hidden" name="amount${status.count}" id="value${status.count}">
                                                    <input type="number" name="debitamount" min="0" id="amount${status.count}" value="${item2.amount}" class="form-control" autofocus="" required onkeyup="calculateGrandTotalDebit()">
                                                </td>
                                                </c:if>
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                        <tfoot>
                                            
                                            <tr>
                                                <td class="thick-line text-right"><strong>Total Debit</strong></td>
                                                
                                                <td class="thick-line text-right"> PhP <span id="grandtotaldebit"></span>
                                                    <input type="hidden" id="nursupertag" name="totaldebitamount">  
                                                </td>
                                                <td class="thick-line"></td>
                                            </tr>
                                            
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-sm-6">
                        <div class="container-fluid">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <center><h3 class="panel-title"><strong>Credit</strong></h3></center>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="myTable"  class=" table order-list2">
                                        <tbody>
                                            <c:forEach var="item2" items="${transactiondetail}" begin="0" varStatus="status">
                                            <tr>    
                                            
                                                <c:if test="${item2.debitcredit == 'credit'}">
                                                <td>
                                                    <input type="hidden" name="detailid${status.count}" value="${item2.id}">
                                                    <select name = "coa${status.count}" class="form-control" autofocus="">
                                                        <option value="${item2.coaid}">${item2.coaname}</option>
                                                        <c:forEach var="row" items="${coa}">    
                                                            <c:if test="${item2.debitcredit == 'credit'}">
                                                                <option value="${row.coaid}">${row.coaname}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                </select></td>
                                                
                                                <td class="text-right">
                                                    <input type="hidden" name="amount${status.count}" id="value${status.count}">
                                                    <input type="number" name="creditamount" min="0" id="amount${status.count}" value="${item2.amount}" class="form-control" autofocus="" required onkeyup="calculateGrandTotalCredit()">
                                                </td>
                                                </c:if>
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                        <tfoot>
                                            </tr>
                                            <tr>
                                                <td class="thick-line text-right"><strong>Total Credit</strong></td>
                                                <td class="thick-line text-right"> PhP  <span id="grandtotalcredit"></span>
                                                    <input type="hidden" id="nursupertag2" name="totalcreditamount"> 
                                                </td>
                                                <td class="thick-line"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                        
            <br>
            <div class="container-fluid">
                <center>
                    <div class="col-sm-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                    Bank: <br>
                    <select name = "debitbank" class="form-control" autofocus="" required>
                        
                        <c:forEach var="row" items="${bank}">    
                            <c:if test="${item.debitbank != row.bankid}">
                                <option value="${row.bankid}">${row.bankname}</option> 
                            </c:if>
                            <c:if test="${item.debitbank == row.bankid}">
                                <option value="${row.bankid}" selected>${row.bankname}</option> 
                            </c:if>
                        </c:forEach>
                    </select>
                    </div>
                    </div>  
                <br>
                    <div class="col-sm-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                    Type of Payment:
                    <select name = "debittypeofpayment" class="form-control" autofocus="" required>
                        <c:forEach var="row" items="${top}">   
                            <c:if test="${item.debittop != row.topid}">
                                <option value="${row.topid}">${row.topname}</option>
                            </c:if>
                            <c:if test="${item.debittop == row.topid}">
                                <option value="${row.topid}" selected>${row.topname}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    </div>
                    </div>
                <br>
                    <div class="col-sm-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                    Details of Type of Payment:<br>
                    <input type="text" name="debitdetailtop" value="${item.debittopdetail}" class="form-control" autofocus="" required>
                    </div>
                    </div>
                <br>    
                    <div class="col-sm-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        Transaction Detail: <br>
                    <input type="text" name="detail" value="${item.transactiondetail}" class="form-control" autofocus="" required><br><br>
                    </div>
                    </div>
                </center>
            
            <div class="container-fluid">
                <br><br>
                <div class="col-sm-8"></div>
                <div class="col-sm-2">
                    <input type="reset" class="btn btn-default">
                    <input type="hidden" value="${transactionid}" name="transactionid">
                    <input type="hidden" value="${accountid}" name="accountid">
                    <input type="submit" class="btn btn-success" name="updateinfo" onclick="return Validate()"  value="Update Transaction" >
                </div>
            </div>
            </c:forEach>            
        </form>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
