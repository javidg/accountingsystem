<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?updatetypeofpayment=Update'>Need the list of type of payments? Click here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title> ${companyname} AIS - Add Type Of Payment </title>
        <jsp:include page="/include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Type Of Payment</h3>
            <form action="AddServlet" method="post">
                Type Of Payment:
                <input type="text" name="top" placeholder="Enter Type of Payment" class="form-control" autofocus="" required><br>
                <div align="right">
                    <input type="reset" class="btn btn-default">
                    <input type="submit" class="btn btn-success" name="addtop" value="Add" >
                </div>
            </form>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
