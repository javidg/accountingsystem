<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?addtransaction=Add'>Transaction not in the list? Add here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update Transaction</title>
        <jsp:include page="/include/head.jsp"/>
        <style>.table-responsive {height:180px;}</style>
        <script><jsp:include page="/js/pagination.js"/></script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Update a Verified Transaction</h3>
            <jsp:include page="/include/search.jsp"/>
            <br><br>
            <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">Canceled Transactions</h3>
                            </div>
                            <div class="modal-body">
                            <table id="table" class="table table-hover">  
                                <thead>
                                    <tr><th> Date </th>
                                        <th> Transaction Detail </th> 
                                        <th> Debit Accounts</th>
                                        <th> Credit Accounts</th>
                                        <th> Status </th>
                                        <th> Retrieve </th>
                                    </tr>
                                </thead>
                                <c:forEach var="citem" items="${ctransaction}">
                                    <tbody>
                                        <tr>
                                            <td>${citem.date} </td>  
                                            <td>${citem.transactiondetail} </td>
                                            <td>${citem.debitcoaname}</td>
                                            <td>${citem.creditcoaname}</td>
                                            <td>${citem.status} </td>
                                            <td>
                                                <form action="UpdateServlet" method="post">
                                                    <input type="hidden" value ="${citem.transactionid}" name="transactionid">
                                                    <button type="submit" class="btn btn-success" value="Retrieve Verified Transaction" name="updateinfo"><span class="glyphicon glyphicon-thumbs-up"> </span></button> 
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </c:forEach>
                            </table> 
                            </div>
                            <div class="modal-footer">
                                <c:if test="${permission.equals('admin')}">  
                                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-floppy-remove" > </span> Archive All</button>
                                    </c:if>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <table id="myTable" class="table table-hover">  
                <thead>
                    <tr><th> Date </th>
                        <th> Transaction Detail </th> 
                        <th> Debit Accounts</th>
                        <th> Credit Accounts</th>
                        <th> Status </th>
                        <th> Verifier </th>
                        <th> Edit </th>
                        <th> Cancel </th>
                    </tr>
                </thead>  
                <c:forEach var="item" items="${transaction}">
                    <tbody>
                        <tr>
                            <td>${item.date} </td>  
                            <td>${item.transactiondetail} </td>
                            <td>${item.debitcoaname}</td>
                            <td>${item.creditcoaname}</td>
                            <td>${item.status}</td>
                            <td>${item.verifierfirstname} ${item.verifierlastname}</td>
                            <td>   
                                <form action="GetServlet" method="post">
                                    <input type="hidden" value="transaction" name="page">
                                    <input type="hidden" value="${accountid}" name="accountid">
                                    <input type="hidden" value="${item.transactionid}" name="transactionid">
                                    <button type="submit" class="btn btn-default">Edit</button>
                                </form>
                            </td> 
                            <td>
                                <form action="UpdateServlet" method="post">
                                    <input type="hidden" value ="${item.transactionid}" name="transactionid">
                                    <button type="submit" class="btn btn-default" value="Cancel Verified Transaction" name="updateinfo"><span class="glyphicon glyphicon-thumbs-down"> </span></button> 
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="col-sm-12" align="right">
                <button type="submit" class="btn btn-default" data-toggle="modal" data-target="#myModal">See Canceled Transactions</button> 
            </div>
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg pager" id="myPager"></ul>
            </div>
            <div class="modal fade" id="delete" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <center><h3 class="modal-title">Warning</h3></center>
                            </div>
                            <div class="modal-body">
                                <center><b>Would you like to delete all canceled transactions?</b><br><br>
                                        <form action="UpdateServlet" method="post">
                                            <button type="submit" class="btn btn-default" value="Delete T Report" name="updateinfo">Yes</button> 
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                        </form>
                                </center>    
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
