<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="returnlink" value= "" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} - AIS - VIEW INCOME STATEMENT</title>
        <jsp:include page="/include/head.jsp"/>
        <script type="text/javascript">
            function myFunction(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
        <script>
            var myVar;
            function myFunction() {
                myVar = setTimeout(showPage, 2000);
            }
            function showPage() {
                document.getElementById("loader").style.display = "none";
                document.getElementById("myDiv").style.display = "block";
            }
        </script>
        <script>
            function format2(n) {
                return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
            }
        </script>
        <style>
            <jsp:include page="/css/loadingbutton.css"/>
        </style>
    </head>
    <body onload="myFunction()" style="margin:0;">
        <jsp:include page="/include/header.jsp"/>
        <div id="loader"></div>
        <div style="display:none;" id="myDiv" class="animate-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-11" align="right">
                        <button type="button" class="btn btn-info btn" data-toggle="modal" data-target="#myModal">Save Report</button>
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" align="center">Save Income Statement Report</h4>
                                    </div>
                                    <form action="AddServlet" method="post">
                                        <div class="modal-body"> <center>
                                                <input type="hidden" name="startdate" value="${sqlstartdate}">
                                                <input type="hidden" name="enddate" value="${sqlenddate}">
                                                <p>Date is from ${sqlstartdate} to ${sqlenddate}</p>
                                                <p>Name of Report</p>
                                                <input type="text" name="reportname" placeholder="Enter Report Name" class="form-control" autofocus="" required>
                                            </center></div>        
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-success" name="saveincomestatement" value="Save Report" >
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1" align="right">
                        <button algin="right" class="btn btn-success" type="button" onclick="myFunction('demo')" />Print/Download</button>
                    </div>
                </div>
            </div>                              
            <div id="demo" class="container">
                <div class="row">                               
                    <center>
                        <h4>Income Statement for ${companyname}</h4>
                        <h5>for the period ending ${sqlenddate}</h5> 
                    </center>
                    <c:set var="total" value="${0}"/>
                    <c:set var="totalincome" value="${0}"/>
                    <c:set var="totalcostofservices" value="${0}"/>
                    <c:set var="totaloperatingexpenses" value="${0}"/>
                    <c:set var="totalotherincome" value="${0}"/>
                    <div class="container">
                        <table id="myTable" class="table table-hover">  
                            <thead>
                                <tr>
                                    <th> Gross Receipts </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <c:forEach var="item" items="${transaction}">
                                <tbody><tr>
                                        <c:if test="${item.classification == 'servicefees'}" ><c:set var="totalincome" value="${totalincome+item.debitamount}"/>
                                            <td>${item.debitdetail}</td>
                                            <td>${item.debitamount}</td>
                                            <td></td>
                                        </c:if>
                                    </c:forEach>
                                </tr></tbody>
                            <thead>
                                <tr>
                                    <th>  </th>
                                    <th> Total Income </th>
                                    <th>PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totalincome}" /></th>
                                </tr>
                            </thead> 
                            <thead>
                                <tr>
                                    <th> Less : Cost Of Services </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <c:forEach var="item" items="${transaction}">
                                <tbody><tr>
                                        <c:if test="${item.classification == 'costofservices'}"><c:set var="totalcostofservices" value="${totalcostofservices+item.debitamount}"/>
                                            <td>${item.debitdetail}</td>
                                            <td>(${item.debitamount})</td>
                                            <td></td>
                                        </c:if>
                                    </c:forEach>
                                </tr></tbody>
                            <thead>
                                <tr>
                                    <th>  </th>
                                    <th> Total Cost Of Services </th>
                                    <th style="color: red"> - PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totalcostofservices}" /></th>
                                </tr>
                            </thead> 
                            <thead>
                                <tr>
                                    <th> Gross Profit / (Loss) </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <thead>
                                <tr>
                                    <th> Less : Operating Expenses </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <c:forEach var="item" items="${transaction}">
                                <tbody><tr>
                                        <c:if test="${item.classification == 'opex'}"><c:set var="totaloperatingexpenses" value="${totaloperatingexpenses+item.debitamount}"/>
                                            <td>${item.debitdetail}</td>
                                            <td>(${item.debitamount})</td>
                                            <td></td>
                                        </c:if>
                                    </c:forEach>
                                </tr></tbody>
                            <thead>
                                <tr>
                                    <th>  </th>
                                    <th> Total Operating Expenses </th>
                                    <th style="color: red"> - PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totaloperatingexpenses}" /></th>
                                </tr>
                            </thead> 
                            <thead>
                                <tr>
                                    <th> Net Operating Income / (Loss) </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <thead>
                                <tr>
                                    <th> Add : Other Income </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <c:forEach var="item" items="${transaction}">
                                <tbody><tr>
                                        <c:if test="${item.classification == 'provisionforincome'}"><c:set var="totalotherincome" value="${totalotherincome+item.debitamount}"/>
                                            <td>${item.debitdetail}</td>
                                            <td>(${item.debitamount})</td>
                                            <td></td>
                                        </c:if>
                                    </c:forEach>
                                </tr></tbody>
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th> Total Other Income </th>
                                    <th> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totalotherincome}" /></th>
                                </tr>
                            </thead>
                            <c:set var="total" value="${totalincome-totalcostofservices-totaloperatingexpenses+totalotherincome}"/>
                            <thead>
                                <tr>
                                    <th> Net Income / (Loss) For This Period </th>
                                    <th> </th>
                                        <c:if test="${total < 0}">
                                        <th style="color: red;"> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${total}" /></th>
                                        </c:if>
                                        <c:if test="${total >= 0}">
                                        <th style="color: green;"> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${total}" /></th>
                                        </c:if>
                                </tr>
                            </thead> 
                        </table>
                    </div></div></div>
                    <jsp:include page="/include/footer.jsp"/>
        </div>
    </body>
</html>
