<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "" scope="session" />
<c:set var="message" value= "Ooooops! Something happened" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Error</title>
        <jsp:include page="/include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <div class="container" align="center">
            <div class="alert alert-danger">
                <c:if test="${message!=null}">
                    <strong>${message}</strong>
                </c:if>
                <c:if test="${message==null}">
                    <strong>Error!</strong> Sorry! An error occurred
                </c:if>
            </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
