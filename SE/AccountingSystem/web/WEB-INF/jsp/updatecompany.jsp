
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update Company Details</title>
        <jsp:include page="/include/head.jsp"/>
        <script>
            function edit_row(no) {
                document.getElementById("edit_button" + no).style.display = "none";
                document.getElementById("save_button" + no).style.display = "block";
                var id = document.getElementById("id_row" + no);
                var companyname = document.getElementById("companyname_row" + no);
                var companyaddress = document.getElementById("companyaddress_row" + no);
                var companytelno = document.getElementById("companytelno_row" + no);
                var id_data = id.innerHTML;
                var companyname_data = companyname.innerHTML;
                var companyaddress_data = companyaddress.innerHTML;
                var companytelno_data = companytelno.innerHTML;
                id.innerHTML = "<input type='hidden' name='id' id='id_text" + no + "' value='" + id_data + "'>";
                companyname.innerHTML = "<input type='text' class='form-control' required name='companyname placeholder='Enter Company Name Here' id='companyname_text" + no + "' value='" + companyname_data + "'>";
                companyaddress.innerHTML = "<input type='text' class='form-control' placeholder='Enter Company Address Here' required name='companyaddress' id='companyaddress_text" + no + "' value='" + companyaddress_data + "'>";
                companytelno.innerHTML = "<input type='number' pattern='[0-9]{7}' placeholder='Enter Seven Digit Telephone Number Here' class='form-control' required name='companytelno' id='companytelno_text" + no + "' value='" + companytelno_data + "'>";
            }
            function save_row(no) {
                var id_val = document.getElementById("id_text" + no).value;
                var companyname_val = document.getElementById("companyname_text" + no).value;
                var companyaddress_val = document.getElementById("companyaddress_text" + no).value;
                var companytelno_val = document.getElementById("companytelno_text" + no).value;
                var contextpath = "${contextpath}";
                document.getElementById("id_row" + no).innerHTML = id_val;
                document.getElementById("companyname_row" + no).innerHTML = companyname_val;
                document.getElementById("companyaddress_row" + no).innerHTML = companyaddress_val;
                document.getElementById("companytelno_row" + no).innerHTML = companytelno_val;
                document.getElementById("edit_button" + no).style.display = "block";
                document.getElementById("save_button" + no).style.display = "none";
                if (companyname_val === ("")) {
                    swal("Error!", "Company name is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (companyaddress_val === ("")) {
                    swal("Error!", "Company address is blank", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (companytelno_val === ("")) {
                    swal("Error!", "Company telephone number is blank or invalid input", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else if (companytelno_val.length !== 7) {
                    swal("Error!", "Invalid telephone number. Please enter seven digit telephone number", "error");
                    setTimeout(function() {
                        location.reload();
                    }, 3500);
                } else {
                    $.ajax
                            ({
                                type: 'post',
                                url: contextpath + "/GetServlet",
                                data: {
                                    page: "company",
                                    companyid: id_val,
                                    companyname: companyname_val,
                                    companyaddress: companyaddress_val,
                                    companytelno: companytelno_val
                                },
                                success: function (response) {
                                    swal("Updated!", response, "success");
                                },
                                error: function (response) {
                                    swal("Error!", response, "error");
                                }
                            });
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <table id="myTable" class="table table-hover">  
                <thead>
                    <tr>
                        <th> Company Name </th>
                        <th> Company Address </th>
                        <th> Company Telephone Number </th>
                        <th> Edit </th>
                    </tr>
                </thead> 
                <c:forEach var="item" items="${companylist}">
                    <tbody>
                        <tr>
                            <td id="id_row${item.id}" style="display: none;">${item.id}</td>
                            <td id="companyname_row${item.id}">${item.companyname}</td>
                            <td id="companyaddress_row${item.id}">${item.companyaddress}</td>
                            <td id="companytelno_row${item.id}">${item.companytelno}</td>
                            <td>   
                                <input type="button" class="btn btn-default" id="edit_button${item.id}" value="Edit" onclick="edit_row(${item.id})">
                                <input type="button" style="display: none;" id="save_button${item.id}" value="Update" class="btn btn-success" onclick="save_row(${item.id})">
                            </td>   
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="panel-heading"> 
                Note: Changes will take place after logging out of the system
            </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
