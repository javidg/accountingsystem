
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?addbank=Add'>Bank not in the list? Add here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} AIS - Update Bank</title>
        <jsp:include page="/include/head.jsp"/>
        <style>.table-responsive {height:180px;}</style>
        <script><jsp:include page="/js/pagination.js"/></script>
        <script>
            function edit_row(no) {
                document.getElementById("edit_button" + no).style.display = "none";
                document.getElementById("save_button" + no).style.display = "block";
                var bankid = document.getElementById("bankid_row" + no);
                var bankname = document.getElementById("bankname_row" + no);
                var bankid_data = bankid.innerHTML;
                var bankname_data = bankname.innerHTML;
                bankid.innerHTML = "<input type='hidden'  name='bankid' id='bankid_text" + no + "' value='" + bankid_data + "'>";
                bankname.innerHTML = "<input type='text' required class='form-control' name='bankname' id='bankname_text" + no + "' value='" + bankname_data + "'>";
            }
            function save_row(no) {
                var bankid_val = document.getElementById("bankid_text" + no).value;
                var bankname_val = document.getElementById("bankname_text" + no).value;
                var contextpath = "${contextpath}";
                document.getElementById("bankid_row" + no).innerHTML = bankid_val;
                document.getElementById("bankname_row" + no).innerHTML = bankname_val;
                document.getElementById("edit_button" + no).style.display = "block";
                document.getElementById("save_button" + no).style.display = "none";
                if (bankname_val === ("")) {
                    swal("Error!", "Bank name is blank", "error");
                    setTimeout(function () {
                        location.reload();
                    }, 3500);
                } else {
                    $.ajax
                            ({
                                type: 'post',
                                url: contextpath + "/GetServlet",
                                data: {
                                    page: "bank",
                                    bankid: bankid_val,
                                    bankname: bankname_val
                                },
                                success: function (response) {
                                    if (response.indexOf("Error") !== -1) {
                                        swal("Error!", response, "error");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3500);
                                    } else {
                                        swal("Updated!", response, "success");
                                    }
                                },
                                error: function (response) {
                                    swal("Error!", response, "error");
                                }
                            });
                }
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Update Bank </h3>
            <jsp:include page="/include/search.jsp"/>
            <br><br>
            <table id="myTable" class="table table-hover">  
                <thead>
                    <tr>
                        <th> Bank Name </th>
                        <th> Edit </th>
                    </tr>
                </thead> 
                <c:forEach var="item" items="${bank}">
                    <tbody>
                        <tr>
                            <td id="bankid_row${item.bankid}" style="display: none;">${item.bankid}</td>
                            <td id="bankname_row${item.bankid}">${item.bankname}</td>
                            <td>   
                                <input type="button" class="btn btn-default" id="edit_button${item.bankid}" value="Edit" onclick="edit_row(${item.bankid})">
                                <input type="button" style="display: none;" id="save_button${item.bankid}" value="Update" class="btn btn-success" onclick="save_row(${item.bankid})">
                            </td>   
                        </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg pager" id="myPager"></ul>
            </div>
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
