<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="returnlink" value= "<a href='${contextpath}/AdminServlet?updatebank=Update'>Need the list of banks? Click here</a><br>" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title> ${companyname} AIS - Add Bank </title>
        <jsp:include page="/include/head.jsp"/>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <jsp:include page="/include/message.jsp"/>
        <div class="container">
            <h3>Add Bank</h3>
            <form action="AddServlet" method="post">
                Bank Name: 
                <input type="text" name="bank" placeholder="Enter Bank Name" class="form-control" autofocus="" required><br><br>
                <div align="right">
                    <input type="reset" class="btn btn-default">
                    <input type="submit" class="btn btn-success" name="addbank" value="Add" >
                </div>
            </form>  
        </div>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>