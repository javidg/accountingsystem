<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="returnlink" value= "" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <title>${companyname} - AIS - Verified Income Statement Report</title>
        <jsp:include page="/include/head.jsp"/>
        <style>
            <jsp:include page="/css/loadingbutton.css"/>
        </style>
        <script type="text/javascript">
            function myFunction(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
    </head>
    <body>
        <jsp:include page="/include/header.jsp"/>
        <div class="container">
            <div class="row">
                <div class="col-sm-11" id="no" align="right">
                    <c:if test="${permission.equals('admin')}">
                    <form action="AddServlet" method="post">
                        <input type="hidden" name="isid" value="${isreport.reportid}">
                        <c:choose>
                            <c:when test="${isreport.status.equals('verified')}">
                                <input type="submit" class="btn btn-success" name="verifyincomestatement" value="Verify" disabled="">
                            </c:when>
                            <c:when test="${isreport.status.equals('pending')}">
                                <input type="submit" class="btn btn-success" name="verifyincomestatement" value="Verify">
                            </c:when>
                        </c:choose>
                    </form>
                    </c:if>
                </div>
                <div class="col-sm-1" align="right">
                    <button algin="right" class="btn btn-success" type="button" onclick="myFunction('demo')" />Print/Download</button>
                </div>
            </div>
        </div>
        <div id="demo" class="container">
            <div class="row">
            <center>
                <h4>Income Statement for ${companyname}</h4>
                <h5>for the period ending ${isreport.enddate}</h5> 
            </center>
            <c:set var="total" value="${0}"/>
            <c:set var="totalincome" value="${0}"/>
            <c:set var="totalcostofservices" value="${0}"/>
            <c:set var="totaloperatingexpenses" value="${0}"/>
            <c:set var="totalotherincome" value="${0}"/>
            <div class="container">
                <table id="myTable" class="table table-hover">  
                    <thead>
                        <tr>
                            <th> Gross Receipts </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead> 
                    <c:forEach var="item" items="${transaction}">
                        <tbody><tr>
                                <c:if test="${item.classification.equals('servicefees')}" ><c:set var="totalincome" value="${totalincome+item.debitamount}"/>
                                    <td>${item.debitdetail}</td>
                                    <td>${item.debitamount}</td>
                                    <td></td>
                                </c:if>
                            </c:forEach>
                        </tr></tbody>
                    <thead>
                        <tr>
                            <th>  </th>
                            <th> Total Income </th>
                            <th> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totalincome}" /> </th>
                        </tr>
                    </thead> 
                    <thead>
                        <tr>
                            <th> Less : Cost Of Services </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead> 
                    <c:forEach var="item" items="${transaction}">
                        <tbody><tr>
                                <c:if test="${item.classification.equals('costofservices')}"><c:set var="totalcostofservices" value="${totalcostofservices+item.debitamount}"/>
                                    <td>${item.debitdetail}</td>
                                    <td>(${item.debitamount})</td>
                                    <td></td>
                                </c:if>
                            </c:forEach>
                        </tr></tbody>
                    <thead>
                        <tr>
                            <th>  </th>
                            <th> Total Cost Of Services </th>
                            <th style="color: red"> - PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totalcostofservices}" /></th>
                        </tr>
                    </thead> 
                    
                    <thead>
                        <tr>
                            <th> Gross Profit / (Loss) </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead> 
                    <thead>
                        <tr>
                            <th> Less : Operating Expenses </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead> 
                    
                    <c:forEach var="item" items="${transaction}">
                        <tbody><tr>
                                <c:if test="${item.classification.equals('opex')}"><c:set var="totaloperatingexpenses" value="${totaloperatingexpenses+item.debitamount}"/>
                                    <td>${item.debitdetail}</td>
                                    <td>(${item.debitamount})</td>
                                    <td></td>
                                </c:if>
                            </c:forEach>
                        </tr></tbody>
                    <thead>
                        <tr>
                            <th>  </th>
                            <th> Total Operating Expenses </th>
                            <th style="color: red"> - PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totaloperatingexpenses}" /></th>
                        </tr>
                    </thead> 
                    
                    <thead>
                        <tr>
                            <th> Net Operating Income / (Loss) </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead> 
                    <thead>
                        <tr>
                            <th> Add : Other Income </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead> 
                    
                    <c:forEach var="item" items="${transaction}">
                        <tbody><tr>
                                <c:if test="${item.classification.equals('provisionforincome')}"><c:set var="totalotherincome" value="${totalotherincome+item.debitamount}"/>
                                    <td>${item.debitdetail}</td>
                                    <td>(${item.debitamount})</td>
                                    <td></td>
                                </c:if>
                            </c:forEach>
                        </tr></tbody>
                    <thead>
                        <tr>
                            <th> </th>
                            <th> Total Other Income </th>
                            <th> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${totalotherincome}" /> </th>
                        </tr>
                    </thead>
                    
                    <c:set var="total" value="${totalincome-totalcostofservices-totaloperatingexpenses+totalotherincome}"/>
                    <thead>
                        <tr>
                            <th> Net Income / (Loss) For This Period </th>
                            <th> </th>
                            <c:if test="${total < 0}">
                            <th style="color: red;"> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${total}" /></th>
                            </c:if>
                            <c:if test="${total >= 0}">
                            <th style="color: green;"> PHP <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${total}" /></th>
                            </c:if>
                        </tr>
                    </thead> 
                </table>
                <br><br><br><br><br>
            </div>
            <div class="row">
                <div class="col-sm-5" align="center">
                    <strong>Prepared By</strong>
                </div>
                <div class="col-sm-5" align="center">
                    <strong>Verified By</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5" align="center">
                    ${isreport.makerfirstname} ${isreport.makerlastname}
                </div>
                <div class="col-sm-5" align="center">
                    ${verifierfirstname} ${verifierlastname}
                </div>
            </div>
        </div>
        </div><br><br><br>
        <jsp:include page="/include/footer.jsp"/>
    </body>
</html>
