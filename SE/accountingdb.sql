-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: accountingdb
-- ------------------------------------------------------
-- Server version	5.5.47-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AccountTable`
--

DROP TABLE IF EXISTS `AccountTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccountTable` (
  `accountid` int(11) NOT NULL AUTO_INCREMENT,
  `employeeid` varchar(25) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`accountid`),
  UNIQUE KEY `employeeid` (`employeeid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccountTable`
--

LOCK TABLES `AccountTable` WRITE;
/*!40000 ALTER TABLE `AccountTable` DISABLE KEYS */;
INSERT INTO `AccountTable` VALUES (0,'000000',' ',' ','CxTVAaWURCoBxoWVQbyz6BZNGD0yk3uFGDVEL2nVyU4=','admin','active'),(1,'123456','John','Doe','XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=','admin','active'),(2,'140332','Javi Angela','de Gorostiza','vZTc2ib8y05o1qMfm1qsC1ca4mbYImIOkB736+OhHU8=','admin','active'),(3,'142856','Xin Yi','Pang','vZTc2ib8y05o1qMfm1qsC1ca4mbYImIOkB736+OhHU8=','accountant','active'),(4,'140713','Nur Aidil Bin','Nordin','vZTc2ib8y05o1qMfm1qsC1ca4mbYImIOkB736+OhHU8=','employee','active');
/*!40000 ALTER TABLE `AccountTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BankTable`
--

DROP TABLE IF EXISTS `BankTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BankTable` (
  `bankid` int(11) NOT NULL AUTO_INCREMENT,
  `bankname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bankid`),
  UNIQUE KEY `bankname` (`bankname`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BankTable`
--

LOCK TABLES `BankTable` WRITE;
/*!40000 ALTER TABLE `BankTable` DISABLE KEYS */;
INSERT INTO `BankTable` VALUES (2,'BDO'),(1,'UCPB');
/*!40000 ALTER TABLE `BankTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChartOfAccountsTable`
--

DROP TABLE IF EXISTS `ChartOfAccountsTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChartOfAccountsTable` (
  `coaid` int(11) NOT NULL AUTO_INCREMENT,
  `coaname` varchar(255) DEFAULT NULL,
  `debitcredit` varchar(255) DEFAULT NULL,
  `fs` varchar(255) DEFAULT NULL,
  `itrfs` varchar(255) DEFAULT NULL,
  `incomeexpense` varchar(255) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `definition` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`coaid`),
  UNIQUE KEY `coaname` (`coaname`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChartOfAccountsTable`
--

LOCK TABLES `ChartOfAccountsTable` WRITE;
/*!40000 ALTER TABLE `ChartOfAccountsTable` DISABLE KEYS */;
INSERT INTO `ChartOfAccountsTable` VALUES (1,'Cash in bank','debit','bs','Cash on hand and in banks','income','currentassets','Cash in bank'),(2,'Account Receiveable','debit','bs','Payment that will be received by the company from clients','income','currentassets','Account Receiveable'),(3,'Petty cash fund','debit','bs','Cash on hand and in banks','expense','currentassets','Petty cash fund'),(4,'Trade receivables','debit','bs','Trade and other receivables','income','currentassets','charged sales / sales on account'),(5,'Other receivables','debit','bs','Trade and other receivables','income','currentassets','other than trade receiables'),(6,'Advances to related parties','debit','bs','Advances and deposits','income','currentassets','advances of stockholder / owner, related companies'),(7,'Advances to officers and employees','debit','bs','Advances and deposits','expense','currentassets','cash advances of employees subject for liquidation, clearance, salary deduction'),(8,'Advances to suppliers','debit','bs','Advances and deposits','expense','currentassets','deposit on rentals and other deposits to contractors, something is cannot be used in the near future but is refunable'),(16,'Deposit','debit','bs','Advances and deposits','income','currentsssets','deposit on rentals and other deposits to contractors, something is cannot be used in the near future but is refunable'),(17,'Prepayments','debit','bs','Other current assets','income','currentsssets','anything unused at the end of the reporting period'),(18,'Input tax','debit','bs','Other current assets','expense','currentsssets','input taxes claimed from local purchases and vat on importation'),(19,'Creditable income tax','debit','bs','Other current assets','income','currentsssets','Unused creditable income tax'),(20,'Motor vehicles','debit','bs','Property and equipment - net','expense','noncurrentsssets','purchase of motor vehicle / company fleet'),(21,'Office equipments','debit','bs','Property and equipment - net','expense','noncurrentsssets','anything that uses electricity such as computer equipments, airconditioning units etc'),(22,'Furniture and fixtures','debit','bs','Property and equipment - net','expense','noncurrentsssets','office tables and chairs, cabinets and related items'),(23,'Leasehold improvements','debit','bs','Property and equipment - net','expense','noncurrentsssets','anything material renovation of lease property'),(24,'Building and building improvements','debit','bs','Property and equipment - net','expense','noncurrentsssets','acquired building or buidling construction'),(25,'Land','debit','bs','Property and equipment - net','expense','noncurrentsssets','acquired real property'),(26,'Computer software','debit','bs','Property and equipment - net','income','noncurrentsssets','website or software designs'),(27,'Construction-in-progress','debit','bs','Property and equipment - net','expense','noncurrentsssets','personal or real property acquired that is still in progress or on-going construction'),(28,'Deferred charges - MCIT','debit','bs','Property and equipment - net','income','noncurrentsssets','MCIT'),(29,'Deferred tax asset','debit','bs','Deferred tax asset','income','noncurrentsssets','deferred tax asset from NOLCO and other related furture tax benefit'),(30,'Trade payables','credit','bs','Trade and other payables','expense','currentliabilities','Dues to suppliers'),(31,'SSS premium payable','credit','bs','Trade and other payables','expense','currentliabilities','Statutories payable'),(32,'Philhealth premium payable','credit','bs','Trade and other payables','expense','currentliabilities','Statutories payable'),(33,'Pagibig premium payable','credit','bs','Trade and other payables','expense','currentliabilities','Statutories payable'),(34,'SSS loans payable','credit','bs','Trade and other payables','expense','currentliabilities','SSS loans payable'),(35,'Pagibig loans payable','credit','bs','Trade and other payables','expense','currentliabilities','Pagibig loans payable'),(36,'Commissions payable','credit','bs','Trade and other payables','expense','currentliabilities','Commissions payable'),(37,'Withholding tax payable - 1601C','credit','bs','Other current liabilities','expense','currentliabilities','N/A'),(38,'Withholding tax payable - 1601E 1%','credit','bs','Other current liabilities','expense','currentliabilities','withholding tax on materials and supplies'),(39,'Withholding tax payable - 1601E 2%','credit','bs','Other current liabilities','expense','currentliabilities','withholding tax on services'),(40,'Withholding tax payable - 1601E 5%','credit','bs','Other current liabilities','expense','currentliabilities','withholding tax on rentals'),(41,'Withholding tax payable - 1601E 10%','credit','bs','Other current liabilities','expense','currentliabilities','withholding tax on professionals <= 720,000 / year'),(42,'Withholding tax payable - 1601E 15%','credit','bs','Other current liabilities','expense','currentliabilities','withholding tax on professionals >  720,000 / year'),(43,'Income tax payable','credit','bs','Other current liabilities','expense','currentliabilities','outstanding income tax due at the the end of reporting period'),(44,'Output tax payable','credit','bs','Other current liabilities','expense','currentliabilities','outstanding output vat at the the end of reporting period'),(45,'Accrued expense','credit','bs','Other current liabilities','expense','currentliabilities','Other current liabilities'),(46,'Advances from related parties','credit','bs','Advances from related parties','expense','noncurrentliabilities','advances from stockholders / owner, or related companies'),(47,'Loans payable','credit','bs','Loans payable','expense','noncurrentliabilities','cash received from banks or other related financing institutions'),(48,'Capital stock','credit','bs','Capital stock','expense','capitalstock','paid up capital'),(49,'Retained earnings','credit','bs','Retained earnings','expense','retainedearnings','accumulated profit or loss'),(50,'Service fees','credit','bs','Service fees','income','servicefees','Recruitment service fees'),(51,'Other income','credit','bs','Other income','income','servicefees','income incidental to the main source of revenue'),(52,'Interest income','credit','bs','Interest income','income','servicefees','interest income on bank deposit'),(53,'Salaries and wages','debit','bs','Salaries and allowances','expense','costofservices','basic, overtime, undertime, ecola'),(54,'13th month pay','debit','bs','Salaries and allowances','expense','costofservices','13th month pay'),(55,'SSS and EC employer','debit','bs','SSS, EC, Philhealth and Pagibig','expense','costofservices','SSS and EC employer'),(56,'Philhealth employer','debit','bs','SSS, EC, Philhealth and Pagibig','expense','costofservices','Philhealth employer'),(57,'Pagibig employer','debit','bs','SSS, EC, Philhealth and Pagibig','expense','costofservices','Pagibig employer'),(58,'Vacation leave','debit','bs','Salaries and allowances','expense','costofservices','cash conversion of vacation leave'),(59,'De minimis benefits','debit','bs','Salaries and allowances','expense','costofservices','rice, laundry, christmas gifts, medical, insurance'),(60,'Salaries and wages - nontaxable','debit','bs','Salaries and allowances','expense','costofservices','Officers\' salaries'),(61,'Professional fees','debit','bs','Professional fees','expense','costofservices','directly related to recruitment'),(62,'Commissions','debit','bs','Commissions','expense','costofservices','commissions to service fees'),(63,'Other cost of services','debit','bs','Other cost of services','expense','costofservices','other than the cost'),(64,'Advertising and promotion','debit','bs','Advertising and promotion','expense','opex','for job vacancies, for product ads'),(65,'Marketing charges','debit','bs','Advertising and promotion','expense','opex','charges of depot like credit card, invoice charges and other accomodation charges like rebates, '),(66,'Sponsorship and contribution','debit','bs','Advertising and promotion','expense','opex','sponsorship'),(67,'Representation and entertainment','debit','bs','Representation and entertainment','expense','opex','gifts, meals to existing clients or potential clients, this limits only to meals and gifts'),(68,'Research and development','debit','bs','Research and development','expense','opex','Research and development'),(69,'Rental office','debit','is','Rental','expense','opex','rental of office space'),(70,'Rental parking','debit','is','Rental','expense','opex','rental of parking space'),(71,'Repairs and maintenance (Labor or labor and materials)','debit','is','Repairs and maintenance','expense','opex','repairs of fixed assets if comibination of labor and materials'),(72,'Repairs and maintenance (Materials / supplies)','debit','is','Repairs and maintenance','expense','opex','repairs of fixed assets if purely materials and supplies'),(73,'Air fare and ground fare','debit','is','Transportation and travel','expense','opex','air and ground fare and deliveries'),(74,'Tolling fees','debit','is','Transportation and travel','expense','opex','toll charges'),(75,'Courier and freight charges','debit','is','Transportation and travel','expense','opex','Courier and freight charges'),(76,'Parking fees','debit','is','Transportation and travel','expense','opex','Parking fees'),(77,'Communications','debit','is','Communication, light and water','expense','opex','landline, postpaid line charges, internet'),(78,'Leased line fee','debit','is','Communication, light and water','expense','opex','landline, postpaid line charges, internet'),(79,'power charges','debit','is','Communication, light and water','expense','opex','payable to meralco'),(80,'water charges','debit','is','Communication, light and water','expense','opex','payable to maynilad and manila waters'),(81,'Office supplies','debit','is','Office supplies','expense','opex','printing charges, photocopying charges, office supplies'),(82,'Pantry supplies','debit','is','Office supplies','expense','opex','pantry supplies such as coffee, sugar, tissue, drinking water or any related purchases'),(83,'Photocopies','debit','is','Office supplies','expense','opex','photocopies etc'),(84,'Taxes and licenses - barangay clearance','debit','is','Taxes and licenses','expense','opex','barangay clearance'),(85,'Taxes and licenses - business permit','debit','is','Taxes and licenses','expense','opex','business permit / mayor\'s permit'),(86,'Taxes and licenses - real property','debit','is','Taxes and licenses','expense','opex','real property tax'),(87,'Taxes and licenses - 0605','debit','is','Taxes and licenses','expense','opex','annual registration fee'),(88,'Taxes and licenses - CTC','debit','is','Taxes and licenses','expense','opex','community tax certificate'),(89,'Taxes and licenses - Others','debit','is','Taxes and licenses','expense','opex','other taxes payable to government agencies such as LTO car registration etc'),(90,'Fuel and oil','debit','is','Fuel and oil','expense','costofservices','gasoline of motor vehicle'),(91,'Association dues','debit','is','Dues and subscriptions','expense','opex','monthly assoc. dues'),(92,'Job street','debit','is','Dues and subscriptions','expense','opex','Job street'),(93,'Linked-in','debit','is','Dues and subscriptions','expense','opex','Linked-in'),(94,'Newspapers and magazines','debit','is','Dues and subscriptions','expense','opex','Newspapers and magazines'),(95,'Insurances','debit','is','Insurances','expense','opex','insurance of vehicle, fire insurance, life insurance of key personnel'),(96,'Depreciation','debit','is','Depreciation','expense','opex','provision for wear and tear of fixed assets'),(97,'Janitorial and messnegerial services','debit','bs','Cash on hand and in banks','income','currentsssets','Janitorial and messnegerial services charge'),(98,'Other contractual services','debit','is','Other contractual services','expense','opex','payments to services to hired agencies'),(99,'Trainings and seminars','debit','is','Trainings and seminars','expense','opex','employee development such as seminars, conventions'),(100,'Company sponsored program - christmas party','debit','is','Company sponsored program','expense','opex','christmas party'),(101,'Company sponsored program - midyear party','debit','is','Company sponsored program','expense','opex','mid-year party'),(102,'Company sponsored program - outing','debit','bs','Cash on hand and in banks','income','currentsssets','Outing'),(103,'Company sponsored program - planning','debit','is','Company sponsored program','expense','opex','planning'),(104,'Company sponsored program - others','debit','is','Company sponsored program','expense','opex','others and personal of YM'),(105,'Miscellaneous - bank charge','debit','is','Miscellaneous','expense','opex','other expenses'),(106,'Miscellaneous - penalty','debit','is','Miscellaneous','expense','opex','other expenses'),(107,'Miscellaneous  - others','debit','is','Miscellaneous','expense','opex','other charges not in the chart of accounts'),(108,'Provision for income tax - current','debit','is','Provision for income tax','expense','provisionforincome','income tax due '),(109,'Provision for income tax - deferred','debit','is','Provision for income tax','expense','provisionforincome','income tax due');
/*!40000 ALTER TABLE `ChartOfAccountsTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CompanyTable`
--

DROP TABLE IF EXISTS `CompanyTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CompanyTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(255) DEFAULT NULL,
  `companyaddress` varchar(255) DEFAULT NULL,
  `companytelno` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companyname` (`companyname`),
  UNIQUE KEY `companyaddress` (`companyaddress`),
  UNIQUE KEY `companytelno` (`companytelno`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CompanyTable`
--

LOCK TABLES `CompanyTable` WRITE;
/*!40000 ALTER TABLE `CompanyTable` DISABLE KEYS */;
INSERT INTO `CompanyTable` VALUES (1,'Viventis','Ortigas Center, Pasig City',9876542);
/*!40000 ALTER TABLE `CompanyTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PictureTable`
--

DROP TABLE IF EXISTS `PictureTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PictureTable` (
  `pictureid` int(11) NOT NULL AUTO_INCREMENT,
  `accountid` int(11) NOT NULL,
  `picturename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pictureid`),
  KEY `accountid` (`accountid`),
  CONSTRAINT `PictureTable_ibfk_1` FOREIGN KEY (`accountid`) REFERENCES `AccountTable` (`accountid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PictureTable`
--

LOCK TABLES `PictureTable` WRITE;
/*!40000 ALTER TABLE `PictureTable` DISABLE KEYS */;
INSERT INTO `PictureTable` VALUES (1,1,'picture-1.png'),(2,2,'picture-1.png'),(3,3,'picture-1.png'),(4,4,'picture-1.png'),(5,5,'picture-1.png'),(6,6,'picture-1.png'),(7,7,'picture-1.png'),(8,2,'picture-1.png'),(9,3,'picture-1.png'),(10,4,'picture-1.png');
/*!40000 ALTER TABLE `PictureTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReportTable`
--

DROP TABLE IF EXISTS `ReportTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReportTable` (
  `reportid` int(11) NOT NULL AUTO_INCREMENT,
  `reportname` varchar(255) DEFAULT NULL,
  `reporttype` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `datecreated` date DEFAULT NULL,
  `verifierid` int(11) DEFAULT NULL,
  `makerid` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ReportTable`
--

LOCK TABLES `ReportTable` WRITE;
/*!40000 ALTER TABLE `ReportTable` DISABLE KEYS */;
INSERT INTO `ReportTable` VALUES (1,'January 2017 Income Statement','incomestatement','2017-01-01','2017-01-31','2017-11-24',0,1,'canceled'),(2,'January 2017 Income Statement','incomestatement','2017-01-01','2017-01-31','2017-11-24',1,1,'verified'),(3,'February 2017 Income Statement','incomestatement','2017-02-01','2017-02-28','2017-11-24',1,1,'verified'),(4,'Balance Sheet for March 2017','balancesheet','2017-03-01','2017-03-31','2017-11-26',1,1,'verified'),(5,'April 2017 Income Statement','incomestatement','2017-04-01','2017-04-30','2017-11-26',0,1,'pending'),(6,'April 2017','balancesheet','2017-04-01','2017-04-30','2017-11-26',2,1,'verified'),(7,'January Report','balancesheet','2017-01-01','2017-01-31','2017-11-29',2,2,'verified'),(8,'Feb 2017 Balance Sheet','balancesheet','2017-02-01','2017-02-28','2017-11-29',2,2,'verified'),(9,'Balance Sheet May 2017','balancesheet','2017-05-01','2017-05-31','2017-11-29',2,2,'verified'),(10,'June 2017 Balance Sheet','balancesheet','2017-06-01','2017-06-30','2017-11-29',2,2,'verified'),(11,'July 2017 BS','balancesheet','2017-07-01','2017-07-31','2017-11-29',2,2,'verified'),(12,'August 17 Balance Sheet','balancesheet','2017-08-01','2017-08-31','2017-11-29',0,2,'pending'),(13,'September 2017 Balance Sheet Report','balancesheet','2017-09-01','2017-09-30','2017-11-29',2,2,'verified'),(14,'October Balance Sheet','balancesheet','2017-10-01','2017-10-31','2017-11-29',0,2,'pending'),(15,'March Income Statement 2017','incomestatement','2017-03-01','2017-03-31','2017-11-29',0,2,'pending'),(16,'May 1 to 31 IS Report','incomestatement','2017-05-01','2017-05-31','2017-11-29',0,2,'pending'),(17,'June 2017 Income Statement','incomestatement','2017-06-01','2017-06-30','2017-11-29',0,2,'pending'),(18,'JUly 2017 Income Statement','incomestatement','2017-07-01','2017-07-31','2017-11-29',0,2,'pending'),(19,'August 2017 Income Statement','incomestatement','2017-08-01','2017-08-31','2017-11-29',0,2,'pending'),(20,'2017 September Income Statement','incomestatement','2017-09-01','2017-09-30','2017-11-29',0,2,'pending'),(21,'October Income Statement','incomestatement','2017-10-01','2017-10-31','2017-11-29',0,2,'pending'),(22,'Novmeber Report','balancesheet','2017-11-01','2017-11-30','2017-11-29',0,2,'canceled');
/*!40000 ALTER TABLE `ReportTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionDetailTable`
--

DROP TABLE IF EXISTS `TransactionDetailTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionDetailTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionid` int(11) DEFAULT NULL,
  `coaid` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coaid` (`coaid`),
  KEY `transactionid` (`transactionid`),
  CONSTRAINT `TransactionDetailTable_ibfk_1` FOREIGN KEY (`coaid`) REFERENCES `ChartOfAccountsTable` (`coaid`),
  CONSTRAINT `TransactionDetailTable_ibfk_2` FOREIGN KEY (`transactionid`) REFERENCES `TransactionTable` (`transactionid`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionDetailTable`
--

LOCK TABLES `TransactionDetailTable` WRITE;
/*!40000 ALTER TABLE `TransactionDetailTable` DISABLE KEYS */;
INSERT INTO `TransactionDetailTable` VALUES (1,1,22,60000),(2,1,30,60000),(3,2,80,4000),(4,2,45,4000),(5,3,1,70541),(6,3,52,70500),(7,3,51,41),(8,4,16,6541),(9,4,30,6541),(10,5,69,858000),(11,5,45,858000),(12,6,2,863690),(13,6,50,863690),(14,7,18,5690),(15,7,41,5690),(16,8,55,7651),(17,8,34,7651),(18,9,3,78500),(19,9,36,78500),(20,10,2,9600000),(21,10,48,9600000),(22,11,26,7500),(23,11,45,7500),(24,12,5,68500),(25,12,50,68500),(26,13,21,61000),(27,13,45,61000),(28,14,97,95030),(29,14,45,95030),(30,15,2,200),(31,15,30,100),(32,15,1,250),(33,15,30,350),(34,16,3,450),(35,16,31,450),(36,17,3,67876),(37,17,36,67876),(38,17,4,980),(39,17,34,900),(40,17,35,80),(41,18,5,69),(42,18,37,69),(43,19,1,5000),(44,19,32,5000),(45,20,8,10000),(46,20,36,10000),(47,21,5,12090),(48,21,32,12090);
/*!40000 ALTER TABLE `TransactionDetailTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionTable`
--

DROP TABLE IF EXISTS `TransactionTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionTable` (
  `transactionid` int(11) NOT NULL AUTO_INCREMENT,
  `accountid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `debitcoa` int(11) DEFAULT NULL,
  `debitbank` int(11) DEFAULT NULL,
  `debittop` int(11) DEFAULT NULL,
  `debitamount` double DEFAULT NULL,
  `creditcoa` int(11) DEFAULT NULL,
  `creditamount` double DEFAULT NULL,
  `transactiondetail` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `verifierid` int(11) DEFAULT NULL,
  `debittopdetail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`transactionid`),
  KEY `accountid` (`accountid`),
  KEY `debitcoa` (`debitcoa`),
  KEY `creditcoa` (`creditcoa`),
  KEY `debitbank` (`debitbank`),
  KEY `debittop` (`debittop`),
  KEY `verifierid` (`verifierid`),
  CONSTRAINT `TransactionTable_ibfk_1` FOREIGN KEY (`accountid`) REFERENCES `AccountTable` (`accountid`),
  CONSTRAINT `TransactionTable_ibfk_2` FOREIGN KEY (`debitcoa`) REFERENCES `ChartOfAccountsTable` (`coaid`),
  CONSTRAINT `TransactionTable_ibfk_3` FOREIGN KEY (`creditcoa`) REFERENCES `ChartOfAccountsTable` (`coaid`),
  CONSTRAINT `TransactionTable_ibfk_4` FOREIGN KEY (`debitbank`) REFERENCES `BankTable` (`bankid`),
  CONSTRAINT `TransactionTable_ibfk_8` FOREIGN KEY (`debittop`) REFERENCES `TypeOfPaymentTable` (`topid`),
  CONSTRAINT `verifierid` FOREIGN KEY (`verifierid`) REFERENCES `AccountTable` (`accountid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionTable`
--

LOCK TABLES `TransactionTable` WRITE;
/*!40000 ALTER TABLE `TransactionTable` DISABLE KEYS */;
INSERT INTO `TransactionTable` VALUES (1,1,'2017-01-02',22,2,3,60000,30,60000,'Bought new office chairs','verified',1,'Cheque number 765-98751'),(2,1,'2017-01-19',80,2,2,4000,45,4000,'Unpaid water bill','verified',1,'Master Card (xxx xxxx xxx)'),(3,2,'2017-01-25',1,2,3,0,52,0,'Income from Ayala Inc','verified',2,'Cheque number 865-99751'),(4,1,'2017-01-30',16,2,1,6541,30,6541,'Paid Trade Debt via Bank','verified',1,'Transaction number 8764-289'),(5,1,'2017-02-05',69,2,1,0,45,0,'Unpaid office rental','pending',1,'Cheque number 195-98461'),(6,2,'2017-02-15',2,2,2,0,50,0,'Service Fees paid in bank','verified',2,'Visa'),(7,1,'2017-02-25',18,2,1,5690,41,5690,'BIR WItholding Tax for Feb 2017','verified',1,'Petty Cash Voucher #8760'),(8,1,'2017-03-07',55,2,1,7651,34,7651,'SSS Transaction for March','canceled',1,'Transaction number 8778-287'),(9,1,'2017-03-09',3,2,3,78500,36,78500,'Commission for Agents','verified',1,'Cheque number 330-98751'),(10,1,'2017-03-17',2,2,3,9600000,48,9600000,'Invested Capital ','verified',1,'Cheque number 310-58751'),(11,1,'2017-04-11',26,2,1,7500,45,7500,'Unpaid Computer Software','verified',1,'Cash VOucher #9710'),(12,2,'2017-04-19',5,2,2,0,50,0,'Service Fees for April 2017','verified',2,'Paymaya'),(13,1,'2017-04-05',21,2,1,61000,45,61000,'Bought new Desk','verified',2,'Cash Voucher #9750'),(14,1,'2017-05-03',97,2,3,95030,45,95030,'Unpaid Janitorial Services','canceled',2,'Cheque number 431-98461'),(15,2,'2017-05-07',2,2,1,0,30,0,'Materials bought in SM','verified',2,'Cash'),(16,2,'2017-05-24',3,2,1,0,31,0,'Paid SM Expenses','verified',2,'Petty Cash'),(17,2,'2017-06-14',3,2,1,0,36,0,'June Transactions','verified',2,'Petty Cash Voicher Numbering'),(18,2,'2017-07-26',5,2,3,69,37,69,'July Trnasactions','verified',2,'Chque Number'),(19,2,'2017-08-31',1,2,1,5000,32,5000,'Paid Philhealth','verified',2,'Voucher'),(20,2,'2017-09-20',8,2,3,0,36,0,'Early Payment for Comission','verified',2,'Cheque Number'),(21,2,'2017-10-15',5,2,2,12090,32,12090,'Ocotber PhilHealth','verified',2,'Credit Card Number');
/*!40000 ALTER TABLE `TransactionTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TypeOfPaymentTable`
--

DROP TABLE IF EXISTS `TypeOfPaymentTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TypeOfPaymentTable` (
  `topid` int(11) NOT NULL AUTO_INCREMENT,
  `topname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`topid`),
  UNIQUE KEY `topname` (`topname`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TypeOfPaymentTable`
--

LOCK TABLES `TypeOfPaymentTable` WRITE;
/*!40000 ALTER TABLE `TypeOfPaymentTable` DISABLE KEYS */;
INSERT INTO `TypeOfPaymentTable` VALUES (1,'Cash'),(3,'Cheque'),(2,'Credit Card');
/*!40000 ALTER TABLE `TypeOfPaymentTable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-19 22:33:47
