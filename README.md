# Viventis Search Asia Accounting Information System's README

Viventis Search Asia Accounting Information System is a web based platform where accountants, employees and stakeholders will be given different permissions to access the system.
The system manages and tracks the company’s financial transactions. Through this system, VSA should be able to efficiently generate reports needed to meet corporate and management requirements of their accounting department